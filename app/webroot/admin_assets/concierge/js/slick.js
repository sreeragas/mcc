
$(document).ready(function() {
	
$('.single-item').slick({
	autoplay: true,
   dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  
  });
	
$('.responsive').slick({
 autoplay: true,
 dots: true,
 infinite: true,
 speed: 1000,
 slidesToShow: 3,
 slidesToScroll: 3,
 responsive: [
   {
     breakpoint: 1024,
     settings: {
       slidesToShow: 3,
       slidesToScroll: 3,
     }
   },
   
   {
     breakpoint: 992,
     settings: {
       slidesToShow: 2,
       slidesToScroll: 2,
     }
   },
   
   {
     breakpoint: 600,
     settings: {
       slidesToShow: 2,
       slidesToScroll: 2
     }
   },
   {
     breakpoint: 480,
     settings: {
       slidesToShow: 1,
       slidesToScroll: 1
     }
   }
 ]
});
});