var app = angular.module('iServicesWebApp', [ ]);

app.controller('mainController', function($scope) {
  $scope.userdata = [{userid: "u001", name: "Moroni", email:"user1@iservices.com", role: "Administrator", status : "Active"},
					{userid: "u002", name: "Tiancum", email:"user2@iservices.com", role: "Supervisor", status : "Active"},
					{userid: "u003", name: "Jacob", email:"user3@iservices.com", role: "User", status : "Inactive"},
					{userid: "u004", name: "Nephi", email:"user4@iservices.com", role: "User", status : "Active"},
					{userid: "u005", name: "Enos", email:"user5@iservices.com", role: "User", status : "Active"},
					{userid: "u006", name: "Rio", email:"user6@iservices.com", role: "User", status : "Inactive"},
					{userid: "u007", name: "Hulk", email:"user7@iservices.com", role: "User", status : "Active"},
					{userid: "u008", name: "Peter", email:"user8@iservices.com", role: "User", status : "Active"},
					{userid: "u009", name: "Roshan", email:"user9@iservices.com", role: "User", status : "Active"},
					{userid: "u0010", name: "Tom", email:"user10@iservices.com", role: "User", status : "Active"},
					{userid: "u0011", name: "Andrew", email:"user11@iservices.com", role: "User", status : "Inactive"},
					{userid: "u0012", name: "Zing", email:"user12@iservices.com", role: "User", status : "Active"},
					{userid: "u0013", name: "Poland", email:"user13@iservices.com", role: "User", status : "Active"},
					{userid: "u0014", name: "Turtle", email:"user14@iservices.com", role: "User", status : "Active"},
					{userid: "u0015", name: "French", email:"user15@iservices.com", role: "User", status : "Inactive"},
					{userid: "u0016", name: "None", email:"user16@iservices.com", role: "User", status : "Active"},
					{userid: "u0017", name: "Dido", email:"user17@iservices.com", role: "User", status : "Active"}
				];
  
});
