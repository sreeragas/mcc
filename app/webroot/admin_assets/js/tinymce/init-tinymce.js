$(document).ready(function () {
	tinymce.init({
		selector: 'textarea.tinymce',
		branding: false,
		height: 400,
		relative_urls : false,
		remove_script_host : false,
		convert_urls : true,
		plugins: [
		  'advlist autolink lists link image charmap print preview anchor',
		  'searchreplace visualblocks code fullscreen',
		  'insertdatetime media table contextmenu paste code'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
	});
});