$(document).ready(function() {
	
	$('#sidebarmenulist > ul > li:has(ul)').addClass("has-sub");
	
	$('#sidebarmenulist > ul > li.active').addClass("on");
	$('#sidebarmenulist > ul > li.active > ul').slideDown('normal');
	
	$('#sidebarmenulist > ul > li > a').click(function() {
		var checkElement = $(this).next();
		
		$('#sidebarmenulist li').removeClass('on');
		$(this).closest('li').addClass('on');	
		
		
		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
		  $(this).closest('li').removeClass('on');
		  checkElement.slideUp('normal');
		}
		
		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
		  $('#sidebarmenulist ul ul:visible').slideUp('normal');
		  checkElement.slideDown('normal');
		}
		
		if (checkElement.is('ul')) {
		  return false;
		} else {
		  return true;	
		}		
	});
	
	var pgurl = window.location.pathname;

    $("#sidebarmenulist li a").each(function () {

        if ($(this).attr("href") == pgurl || $(this).attr("href") == '') {
            $(this).addClass("menuselected");
        }
        if ($(".menuselected").parent().parent().parent().hasClass("has-sub")) {
            $(".menuselected").parent().parent().css("display", "block");
			$(".menuselected").parent().parent().parent().addClass("active on");
        }
    });
	
	
	$("#sidebarmenulist").mCustomScrollbar({
		theme:"minimal",
		scrollInertia:300
	});
	
	setTimeout(function(){
		$('#sidebarmenulist').mCustomScrollbar('scrollTo', '.has-sub.active',{
			scrollInertia:800,
		});
	}, 500);
	
	$('.modal').on('hidden.bs.modal', function(){
		$(this).find('form')[0].reset();
	});
	
	var geturl = window.location.pathname; 
	var getlaststr = geturl.substring(geturl.lastIndexOf('/') + 1);
	var getstrnoext = getlaststr.split(".")[0];
	$('html').addClass(getstrnoext);
	
	// START : allow letters and whitespaces only.
	$(".inputTextSpace").keypress(function(event){
		var inputValue = event.which;
		if ((inputValue < 65 || inputValue > 90) && (inputValue < 97 || inputValue > 123) && inputValue > 32){
			event.preventDefault();
		}
	});
	// END : allow letters and whitespaces only.
	
	// START : allow numbers only.
	$(".inputNumberOnly").keypress(function(event){
		var inputValue = event.which;
		if (inputValue > 31 && (inputValue < 48 || inputValue > 57)){
			event.preventDefault();
		}
	});
	// END : allow numbers only.
	
	// START : Allow decimal only.
	/*$(".inputDecimal").on("input", function(evt) {
		var self = $(this);
		self.val(self.val().replace(/[^0-9\.]/g, ''));
		if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
		{
		 evt.preventDefault();
		}
	});*/
	$('.inputDecimal').keypress(function(event) {
		var $this = $(this);
		if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
		   ((event.which < 48 || event.which > 57) &&
		   (event.which != 0 && event.which != 8))) {
			   event.preventDefault();
		}
	
		var text = $(this).val();
		if ((event.which == 46) && (text.indexOf('.') == -1)) {
			setTimeout(function() {
				if ($this.val().substring($this.val().indexOf('.')).length > 3) {
					$this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
				}
			}, 1);
		}
	
		if ((text.indexOf('.') != -1) &&
			(text.substring(text.indexOf('.')).length > 2) &&
			(event.which != 0 && event.which != 8) &&
			($(this)[0].selectionStart >= text.length - 2)) {
				event.preventDefault();
		}      
	});
	
	$('.inputDecimal').bind("paste", function(e) {
	var text = e.originalEvent.clipboardData.getData('Text');
	if ($.isNumeric(text)) {
		if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
			e.preventDefault();
			$(this).val(text.substring(0, text.indexOf('.') + 3));
	   }
	}
	else {
			e.preventDefault();
		 }
	});
	// END : Allow decimal only.
	
	$('.inputMultiplier').change(function() {
		if($(this).val() > 1) {
			$(this).val('');
		}       
	});    
	
	$('.inputNoEnter').keydown(function(e) {
	   e.preventDefault();
	   return false;
	});

	$(".mdl-data-table")
		.on('all.bs.table', function (e, data) {
		setTimeout(function(){
		  componentHandler.upgradeAllRegistered();
		}, 0);
		$('.mdl-checkbox__input').change(function() {
			if($(this).is(':checked')) {
				$(this).closest('tr').addClass('mdl-color--yellow-50');
			}else{
				$(this).closest('tr').removeClass('mdl-color--yellow-50');
			}
			/*var checkboxes = $(this).closest('form').find(':checkbox');
			if($(this).is('#checkbox-all')){
				if($('#checkbox-all').is(':checked')) {
					checkboxes.prop('checked', true);
					checkboxes.parent().addClass('is-checked');
					checkboxes.closest('tr').addClass('mdl-color--yellow-50');
				} else {
					checkboxes.prop('checked', false);
					checkboxes.parent().removeClass('is-checked');
					checkboxes.closest('tr').removeClass('mdl-color--yellow-50');
				}
			}*/
			var checkboxeschecked = $(this).closest('tbody').find(':checkbox:checked');
			var checkboxeslength = $(this).closest('tbody').find(':checkbox');
			var countchecked = checkboxeschecked.length;
			var countcheckbox = checkboxeslength.length;
			
			if (countcheckbox != countchecked ){
				$("#checkbox-all").prop('checked', false);
				$("#checkbox-all").parent().removeClass('is-checked');
				$("#checkbox-all").closest('tr').removeClass('mdl-color--yellow-50');
			}
			
			if(countchecked>0) {
				$('#hdrbtn').addClass('mdl-color--yellow-A400');
			} else{
				$('#hdrbtn').removeClass('mdl-color--yellow-A400');
			}
		});
		
		var checkboxes = $('#checkbox-all').closest('form').find(':checkbox');
		$("#checkbox-all").change(function(){
			if($('#checkbox-all').is(':checked')) {
				checkboxes.prop('checked', true);
				checkboxes.parent().addClass('is-checked');
				checkboxes.closest('tr').addClass('mdl-color--yellow-50');
				$('#hdrbtn').addClass('mdl-color--yellow-A400');
			} else {
				checkboxes.prop('checked', false);
				checkboxes.parent().removeClass('is-checked');
				checkboxes.closest('tr').removeClass('mdl-color--yellow-50');
				$('#hdrbtn').removeClass('mdl-color--yellow-A400');
			}
		});
    });
	
	$('.mdl-checkbox__input').change(function() {
			if($(this).is(':checked')) {
				$(this).closest('tr').addClass('mdl-color--yellow-50');
			}else{
				$(this).closest('tr').removeClass('mdl-color--yellow-50');
			}
			/*var checkboxes = $(this).closest('form').find(':checkbox');
			if($(this).is('#checkbox-all')){
				if($('#checkbox-all').is(':checked')) {
					checkboxes.prop('checked', true);
					checkboxes.parent().addClass('is-checked');
					checkboxes.closest('tr').addClass('mdl-color--yellow-50');
				} else {
					checkboxes.prop('checked', false);
					checkboxes.parent().removeClass('is-checked');
					checkboxes.closest('tr').removeClass('mdl-color--yellow-50');
				}
			}*/
			var checkboxeschecked = $(this).closest('tbody').find(':checkbox:checked');
			var checkboxeslength = $(this).closest('tbody').find(':checkbox');
			var countchecked = checkboxeschecked.length;
			var countcheckbox = checkboxeslength.length;
			
			if (countcheckbox != countchecked ){
				$("#checkbox-all").prop('checked', false);
				$("#checkbox-all").parent().removeClass('is-checked');
				$("#checkbox-all").closest('tr').removeClass('mdl-color--yellow-50');
			}
			
			if(countchecked>0) {
				$('#hdrbtn').addClass('mdl-color--yellow-A400');
			} else{
				$('#hdrbtn').removeClass('mdl-color--yellow-A400');
			}
		});
		
		var checkboxes = $('#checkbox-all').closest('form').find(':checkbox');
		$("#checkbox-all").change(function(){
			if($('#checkbox-all').is(':checked')) {
				checkboxes.prop('checked', true);
				checkboxes.parent().addClass('is-checked');
				checkboxes.closest('tr').addClass('mdl-color--yellow-50');
				$('#hdrbtn').addClass('mdl-color--yellow-A400');
			} else {
				checkboxes.prop('checked', false);
				checkboxes.parent().removeClass('is-checked');
				checkboxes.closest('tr').removeClass('mdl-color--yellow-50');
				$('#hdrbtn').removeClass('mdl-color--yellow-A400');
			}
		});

	$(".fixed-table-toolbar").find(".search").append("<input type='button' id='cleatsearchtext' value='X' class='btn btn-link' />");
	
	$('#cleatsearchtext').on('click', function () {
		$('.mdl-data-table').bootstrapTable('resetSearch', '');
		$('#cleatsearchtext').val('X');
	}); 
	
	if ($.cookie('setcookieforpage') == "yes") {
        $('.mdl-layout').addClass('menubuttonclicked');
    }
	
	$('.mdl-layout__header').find('.mdl-layout__header-row').append('<div id="menu-button"><i class="material-icons">&#xE5D2;</i></div>');
	
	$('.mdl-layout__header-row').on("click", "#menu-button", function() {
		if($('.mdl-layout').hasClass('menubuttonclicked')){
			$('.mdl-layout').removeClass('menubuttonclicked');
			$.cookie('setcookieforpage', 'no', { expires: 7, path: '/' });
		}else{
			$('.mdl-layout').addClass('menubuttonclicked');
			$.cookie('setcookieforpage', 'yes', { expires: 7, path: '/' });
		}
	});
	
});


$(window).bind("load resize", function() {
	if($(window).width() >= 768){
		$('#sidebarmenulist').css({'height':($(window).height() - 151)+'px'});
	}else{
		$('#sidebarmenulist').css({'height':($(window).height() - 151)+'px'});
	}
});

// Decect browser and platform
var nVer = navigator.appVersion; 
var nAgt = navigator.userAgent; 
var bN = navigator.appName; 
var fullV = "" + parseFloat(navigator.appVersion); 
var majorV = parseInt(navigator.appVersion, 10); 
var nameOffset, verOffset, ix; 
var OSName="unknownOS";
if ((verOffset = nAgt.indexOf("Opera")) !== -1) { bN = "Opera"; fullV = nAgt.substring(verOffset + 6); if ((verOffset = nAgt.indexOf("Version")) !== -1) { fullV = nAgt.substring(verOffset + 8) } } else { if ((verOffset = nAgt.indexOf("MSIE")) !== -1) { bN = "IE"; fullV = nAgt.substring(verOffset + 5) } else { if ((verOffset = nAgt.indexOf("Chrome")) !== -1) { bN = "Chrome"; fullV = nAgt.substring(verOffset + 7) } else { if ((verOffset = nAgt.indexOf("Safari")) !== -1) { bN = "Safari"; fullV = nAgt.substring(verOffset + 7); if ((verOffset = nAgt.indexOf("Version")) !== -1) { fullV = nAgt.substring(verOffset + 8) } } else { if ((verOffset = nAgt.indexOf("Firefox")) !== -1) { bN = "Firefox"; fullV = nAgt.substring(verOffset + 8) } else { if ((nameOffset = nAgt.lastIndexOf(" ") + 1) < (verOffset = nAgt.lastIndexOf("/"))) { bN = nAgt.substring(nameOffset, verOffset); fullV = nAgt.substring(verOffset + 1); if (bN.toLowerCase() == bN.toUpperCase()) { bN = navigator.appName } } } } } } } 
if ((ix = fullV.indexOf(";")) !== -1) { fullV = fullV.substring(0, ix) } if ((ix = fullV.indexOf(" ")) !== -1) { fullV = fullV.substring(0, ix) } majorV = parseInt("" + fullV, 10); if (isNaN(majorV)) { fullV = "" + parseFloat(navigator.appVersion); majorV = parseInt(navigator.appVersion, 10) } 
if (navigator.appVersion.indexOf("Win")!=-1) { OSName="Windows"; } if (navigator.appVersion.indexOf("Mac")!=-1) { OSName="MacOS"; } if (navigator.appVersion.indexOf("X11")!=-1) { OSName="UNIX"; } if (navigator.appVersion.indexOf("Linux")!=-1) { OSName="Linux"; }  if (navigator.appVersion.indexOf("Android")!=-1) { OSName="Android"; } if (navigator.appVersion.indexOf("iPhone")!=-1) { OSName="iPhone"; }
	document.getElementsByTagName("html")[0].className += " " +  bN + " " +  bN + majorV +  " " + OSName;