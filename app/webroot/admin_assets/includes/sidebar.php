<div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
    <header class="demo-drawer-header">
      <img src="images/logo.png" class="demo-avatar">
      <div class="demo-avatar-dropdown">
        <span><?php echo $this->Auth->user('email'); ?>hello@example.com</span>
        <div class="mdl-layout-spacer"></div>
        <a href="index.php" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon mdl-color--white mdl-color-text--red-600" title="Logout">
          <i class="material-icons" role="presentation">power_settings_new</i>
          <span class="visuallyhidden">Logout</span>
        </a>
      </div>
    </header>
    <nav class="mdl-navigation">
      <div id="sidebarmenulist">
      <ul class="sidebarmenulist">
        <li><a href="dashboard.php"><span></span>Dashboard</a></li>
        <li>
            <a href="javascript:void(0);"><span></span>iDeliver</a></a>
            <ul>
                <li><a href="products.php">Products</a></li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);"><span></span>Administrator</a></a>
            <ul>
                <li><a href="users.php">Users</a></li>
                <li><a href="roles.php">Roles</a></li>
            </ul>
        </li>
      </ul>
      </div>
    </nav>
</div>