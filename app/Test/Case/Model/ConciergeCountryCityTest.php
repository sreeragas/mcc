<?php
App::uses('ConciergeCountryCity', 'Model');

/**
 * ConciergeCountryCity Test Case
 */
class ConciergeCountryCityTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_country_city',
		'app.cs_country'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeCountryCity = ClassRegistry::init('ConciergeCountryCity');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeCountryCity);

		parent::tearDown();
	}

}
