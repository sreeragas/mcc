<?php
App::uses('ConciergePartnerServiceOfferingCountry', 'Model');

/**
 * ConciergePartnerServiceOfferingCountry Test Case
 */
class ConciergePartnerServiceOfferingCountryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partner_service_offering_country',
		'app.org',
		'app.proc_partner',
		'app.proc_service_offering',
		'app.proc_currency'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergePartnerServiceOfferingCountry = ClassRegistry::init('ConciergePartnerServiceOfferingCountry');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergePartnerServiceOfferingCountry);

		parent::tearDown();
	}

}
