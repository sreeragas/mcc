<?php
App::uses('IconciergeMembershipCategory', 'Model');

/**
 * IconciergeMembershipCategory Test Case
 */
class IconciergeMembershipCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.iconcierge_membership_category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->IconciergeMembershipCategory = ClassRegistry::init('IconciergeMembershipCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->IconciergeMembershipCategory);

		parent::tearDown();
	}

}
