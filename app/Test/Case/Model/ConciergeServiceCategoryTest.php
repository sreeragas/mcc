<?php
App::uses('ConciergeServiceCategory', 'Model');

/**
 * ConciergeServiceCategory Test Case
 */
class ConciergeServiceCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_service_category',
		'app.org'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeServiceCategory = ClassRegistry::init('ConciergeServiceCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeServiceCategory);

		parent::tearDown();
	}

}
