<?php
App::uses('ConciergeTransactionEnquirySent', 'Model');

/**
 * ConciergeTransactionEnquirySent Test Case
 */
class ConciergeTransactionEnquirySentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_transaction_enquiry_sent',
		'app.org',
		'app.tse_ts_order',
		'app.tse_pr_partner'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeTransactionEnquirySent = ClassRegistry::init('ConciergeTransactionEnquirySent');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeTransactionEnquirySent);

		parent::tearDown();
	}

}
