<?php
App::uses('ConciergeCurrencyMaster', 'Model');

/**
 * ConciergeCurrencyMaster Test Case
 */
class ConciergeCurrencyMasterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_currency_master'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeCurrencyMaster = ClassRegistry::init('ConciergeCurrencyMaster');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeCurrencyMaster);

		parent::tearDown();
	}

}
