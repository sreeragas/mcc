<?php
App::uses('ConciergeTransaction', 'Model');

/**
 * ConciergeTransaction Test Case
 */
class ConciergeTransactionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_transaction',
		'app.org',
		'app.ts_member',
		'app.ts_membership_category',
		'app.ts_mr_mem_request',
		'app.ts_service_category',
		'app.ts_service_type',
		'app.ts_service_offering',
		'app.ts_partner'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeTransaction = ClassRegistry::init('ConciergeTransaction');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeTransaction);

		parent::tearDown();
	}

}
