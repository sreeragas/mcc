<?php
App::uses('ConciergeNationality', 'Model');

/**
 * ConciergeNationality Test Case
 */
class ConciergeNationalityTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_nationality'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeNationality = ClassRegistry::init('ConciergeNationality');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeNationality);

		parent::tearDown();
	}

}
