<?php
App::uses('ConciergeMembershipRequest', 'Model');

/**
 * ConciergeMembershipRequest Test Case
 */
class ConciergeMembershipRequestTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_membership_request',
		'app.org',
		'app.mr_member',
		'app.mr_old_membership_category',
		'app.mr_toh_order'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeMembershipRequest = ClassRegistry::init('ConciergeMembershipRequest');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeMembershipRequest);

		parent::tearDown();
	}

}
