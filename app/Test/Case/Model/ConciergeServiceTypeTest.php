<?php
App::uses('ConciergeServiceType', 'Model');

/**
 * ConciergeServiceType Test Case
 */
class ConciergeServiceTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_service_type',
		'app.org',
		'app.st_service_catg'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeServiceType = ClassRegistry::init('ConciergeServiceType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeServiceType);

		parent::tearDown();
	}

}
