<?php
App::uses('ConciergePartnerServiceNationalityPref', 'Model');

/**
 * ConciergePartnerServiceNationalityPref Test Case
 */
class ConciergePartnerServiceNationalityPrefTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partner_service_nationality_pref',
		'app.org',
		'app.psnp_partner',
		'app.psnp_service_offering',
		'app.psnp_service_type',
		'app.psnp_nationality'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergePartnerServiceNationalityPref = ClassRegistry::init('ConciergePartnerServiceNationalityPref');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergePartnerServiceNationalityPref);

		parent::tearDown();
	}

}
