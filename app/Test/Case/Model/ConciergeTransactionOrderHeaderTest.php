<?php
App::uses('ConciergeTransactionOrderHeader', 'Model');

/**
 * ConciergeTransactionOrderHeader Test Case
 */
class ConciergeTransactionOrderHeaderTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_transaction_order_header',
		'app.org',
		'app.toh_agent',
		'app.toh_member'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeTransactionOrderHeader = ClassRegistry::init('ConciergeTransactionOrderHeader');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeTransactionOrderHeader);

		parent::tearDown();
	}

}
