<?php
App::uses('ConciergeAtributeList', 'Model');

/**
 * ConciergeAtributeList Test Case
 */
class ConciergeAtributeListTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_atribute_list'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeAtributeList = ClassRegistry::init('ConciergeAtributeList');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeAtributeList);

		parent::tearDown();
	}

}
