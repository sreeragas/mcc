<?php
App::uses('ConciergeCountryMaster', 'Model');

/**
 * ConciergeCountryMaster Test Case
 */
class ConciergeCountryMasterTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_country_master'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeCountryMaster = ClassRegistry::init('ConciergeCountryMaster');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeCountryMaster);

		parent::tearDown();
	}

}
