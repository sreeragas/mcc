<?php
App::uses('ConciergePartnershipRequest', 'Model');

/**
 * ConciergePartnershipRequest Test Case
 */
class ConciergePartnershipRequestTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partnership_request',
		'app.org',
		'app.pr_email',
		'app.pr_country'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergePartnershipRequest = ClassRegistry::init('ConciergePartnershipRequest');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergePartnershipRequest);

		parent::tearDown();
	}

}
