<?php
App::uses('ConciergeServingCountry', 'Model');

/**
 * ConciergeServingCountry Test Case
 */
class ConciergeServingCountryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_serving_country',
		'app.org',
		'app.sct_service_category',
		'app.sct_country',
		'app.sct_city'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeServingCountry = ClassRegistry::init('ConciergeServingCountry');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeServingCountry);

		parent::tearDown();
	}

}
