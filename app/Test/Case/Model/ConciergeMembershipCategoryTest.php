<?php
App::uses('ConciergeMembershipCategory', 'Model');

/**
 * ConciergeMembershipCategory Test Case
 */
class ConciergeMembershipCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_membership_category',
		'app.org'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeMembershipCategory = ClassRegistry::init('ConciergeMembershipCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeMembershipCategory);

		parent::tearDown();
	}

}
