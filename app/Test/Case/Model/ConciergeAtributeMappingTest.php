<?php
App::uses('ConciergeAtributeMapping', 'Model');

/**
 * ConciergeAtributeMapping Test Case
 */
class ConciergeAtributeMappingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_atribute_mapping',
		'app.atm_service_type',
		'app.atm_at_list'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeAtributeMapping = ClassRegistry::init('ConciergeAtributeMapping');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeAtributeMapping);

		parent::tearDown();
	}

}
