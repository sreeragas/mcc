<?php
App::uses('ConciergePartnerServiceOffering', 'Model');

/**
 * ConciergePartnerServiceOffering Test Case
 */
class ConciergePartnerServiceOfferingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partner_service_offering',
		'app.org',
		'app.pro_partner',
		'app.pro_service_catg',
		'app.pro_currency'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergePartnerServiceOffering = ClassRegistry::init('ConciergePartnerServiceOffering');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergePartnerServiceOffering);

		parent::tearDown();
	}

}
