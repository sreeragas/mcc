<?php
App::uses('ConciergePartnershipInfo', 'Model');

/**
 * ConciergePartnershipInfo Test Case
 */
class ConciergePartnershipInfoTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partnership_info',
		'app.pr_partner_request',
		'app.org'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergePartnershipInfo = ClassRegistry::init('ConciergePartnershipInfo');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergePartnershipInfo);

		parent::tearDown();
	}

}
