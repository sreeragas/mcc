<?php
App::uses('ConciergeServiceTypesMembershipAccess', 'Model');

/**
 * ConciergeServiceTypesMembershipAccess Test Case
 */
class ConciergeServiceTypesMembershipAccessTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_service_types_membership_access',
		'app.org',
		'app.stm_st_service_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeServiceTypesMembershipAccess = ClassRegistry::init('ConciergeServiceTypesMembershipAccess');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeServiceTypesMembershipAccess);

		parent::tearDown();
	}

}
