<?php
App::uses('ConciergeServiceTypesServingLocation', 'Model');

/**
 * ConciergeServiceTypesServingLocation Test Case
 */
class ConciergeServiceTypesServingLocationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_service_types_serving_location',
		'app.org',
		'app.stl_service_type',
		'app.stl_country',
		'app.stl_city'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeServiceTypesServingLocation = ClassRegistry::init('ConciergeServiceTypesServingLocation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeServiceTypesServingLocation);

		parent::tearDown();
	}

}
