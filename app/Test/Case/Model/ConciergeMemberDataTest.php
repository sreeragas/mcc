<?php
App::uses('ConciergeMemberData', 'Model');

/**
 * ConciergeMemberData Test Case
 */
class ConciergeMemberDataTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_member_data',
		'app.org',
		'app.md_member_login_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeMemberData = ClassRegistry::init('ConciergeMemberData');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeMemberData);

		parent::tearDown();
	}

}
