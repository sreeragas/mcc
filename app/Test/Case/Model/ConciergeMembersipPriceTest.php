<?php
App::uses('ConciergeMembersipPrice', 'Model');

/**
 * ConciergeMembersipPrice Test Case
 */
class ConciergeMembersipPriceTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_membersip_price',
		'app.org',
		'app.mp_catg',
		'app.mp_country'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ConciergeMembersipPrice = ClassRegistry::init('ConciergeMembersipPrice');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ConciergeMembersipPrice);

		parent::tearDown();
	}

}
