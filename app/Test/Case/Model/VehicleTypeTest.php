<?php
App::uses('VehicleType', 'Model');

/**
 * VehicleType Test Case
 */
class VehicleTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vehicle_type',
		'app.iwash_request',
		'app.user',
		'app.building',
		'app.building_type',
		'app.building_time_slot',
		'app.social_profile',
		'app.iwashpricinglist',
		'app.iwashpriceplan',
		'app.promotion',
		'app.transaction',
		'app.ideliver_order',
		'app.ideliver_delivery_type',
		'app.ideliver_order_delivery_day',
		'app.week_day',
		'app.iwash_requests_week_day',
		'app.ideliver_order_product',
		'app.ideliver_product',
		'app.imaintain_purchased_plan',
		'app.imaintain_plan_pricing',
		'app.imaintain_plan',
		'app.location_type',
		'app.no_of_room',
		'app.imaintain_plan_type',
		'app.plan_type',
		'app.imaintain_purchased_additional_plan',
		'app.tbl_ipestpurchasedplan',
		'app.tbl_ipestplan',
		'app.tbl_ipestplan_frequency',
		'app.tbl_icleantransaction',
		'app.tbl_ilaundryorder',
		'app.tbl_ilaundry_promocode',
		'app.tbl_ihandymantransaction',
		'app.tbl_jobrequest',
		'app.iwash_log',
		'app.admin_login',
		'app.screen_assignment',
		'app.admin_type',
		'app.selected_day',
		'app.package'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VehicleType = ClassRegistry::init('VehicleType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VehicleType);

		parent::tearDown();
	}

}
