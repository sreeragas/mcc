<?php
App::uses('ConciergePartnerServiceOfferingsController', 'Controller');

/**
 * ConciergePartnerServiceOfferingsController Test Case
 */
class ConciergePartnerServiceOfferingsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partner_service_offering'
	);

}
