<?php
App::uses('ConciergeServiceCategoriesController', 'Controller');

/**
 * ConciergeServiceCategoriesController Test Case
 */
class ConciergeServiceCategoriesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_service_category'
	);

}
