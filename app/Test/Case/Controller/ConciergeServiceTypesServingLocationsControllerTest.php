<?php
App::uses('ConciergeServiceTypesServingLocationsController', 'Controller');

/**
 * ConciergeServiceTypesServingLocationsController Test Case
 */
class ConciergeServiceTypesServingLocationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_service_types_serving_location'
	);

}
