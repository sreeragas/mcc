<?php
App::uses('ConciergeNationalitiesController', 'Controller');

/**
 * ConciergeNationalitiesController Test Case
 */
class ConciergeNationalitiesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_nationality'
	);

}
