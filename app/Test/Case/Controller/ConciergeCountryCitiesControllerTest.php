<?php
App::uses('ConciergeCountryCitiesController', 'Controller');

/**
 * ConciergeCountryCitiesController Test Case
 */
class ConciergeCountryCitiesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_country_city',
		'app.cs_country'
	);

}
