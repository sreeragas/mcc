<?php
App::uses('ConciergePartnershipInfosController', 'Controller');

/**
 * ConciergePartnershipInfosController Test Case
 */
class ConciergePartnershipInfosControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partnership_info',
		'app.concierge_partnership_request',
		'app.concierge_country_master',
		'app.concierge_currency_master'
	);

}
