<?php
App::uses('ConciergeServiceTypesController', 'Controller');

/**
 * ConciergeServiceTypesController Test Case
 */
class ConciergeServiceTypesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_service_type'
	);

}
