<?php
App::uses('ConciergePartnerServiceNationalityPrefsController', 'Controller');

/**
 * ConciergePartnerServiceNationalityPrefsController Test Case
 */
class ConciergePartnerServiceNationalityPrefsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partner_service_nationality_pref'
	);

}
