<?php
App::uses('ConciergeServiceTypesMembershipAccessesController', 'Controller');

/**
 * ConciergeServiceTypesMembershipAccessesController Test Case
 */
class ConciergeServiceTypesMembershipAccessesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_service_types_membership_access',
		'app.concierge_service_type',
		'app.concierge_service_category',
		'app.concierge_membership_category'
	);

}
