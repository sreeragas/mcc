<?php
App::uses('ConciergePartnerServiceOfferingCountriesController', 'Controller');

/**
 * ConciergePartnerServiceOfferingCountriesController Test Case
 */
class ConciergePartnerServiceOfferingCountriesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partner_service_offering_country'
	);

}
