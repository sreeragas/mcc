<?php
App::uses('ConciergeServingCountriesController', 'Controller');

/**
 * ConciergeServingCountriesController Test Case
 */
class ConciergeServingCountriesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_serving_country'
	);

}
