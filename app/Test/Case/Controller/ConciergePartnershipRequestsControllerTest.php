<?php
App::uses('ConciergePartnershipRequestsController', 'Controller');

/**
 * ConciergePartnershipRequestsController Test Case
 */
class ConciergePartnershipRequestsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_partnership_request',
		'app.concierge_country_master',
		'app.concierge_currency_master'
	);

}
