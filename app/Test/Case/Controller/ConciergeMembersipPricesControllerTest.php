<?php
App::uses('ConciergeMembersipPricesController', 'Controller');

/**
 * ConciergeMembersipPricesController Test Case
 */
class ConciergeMembersipPricesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.concierge_membersip_price',
		'app.concierge_membership_category',
		'app.concierge_country_master',
		'app.concierge_currency_master'
	);

}
