<?php
/**
 * ConciergeAtributeList Fixture
 */
class ConciergeAtributeListFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'concierge_atribute_list';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'at_list_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'at_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 85, 'collate' => 'latin1_swedish_ci', 'comment' => 'used as the db filed name', 'charset' => 'latin1'),
		'at_label' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'at_description' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'at_icon_file' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'at_list_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'at_list_id' => 1,
			'at_name' => 'Lorem ipsum dolor sit amet',
			'at_label' => 'Lorem ipsum dolor sit amet',
			'at_description' => 'Lorem ipsum dolor sit amet',
			'at_icon_file' => 'Lorem ipsum dolor sit amet'
		),
	);

}
