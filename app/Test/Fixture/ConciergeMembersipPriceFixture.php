<?php
/**
 * ConciergeMembersipPrice Fixture
 */
class ConciergeMembersipPriceFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'mp_mem_price_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'key' => 'index'),
		'mp_catg_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'mp_country_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'mp_price_year' => array('type' => 'decimal', 'null' => false, 'default' => '0.00', 'length' => '10,2', 'unsigned' => false),
		'mp_price_month' => array('type' => 'decimal', 'null' => false, 'default' => '0.00', 'length' => '10,2', 'unsigned' => false),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'mp_mem_price_id', 'unique' => 1),
			'org_id' => array('column' => array('org_id', 'mp_catg_id', 'mp_country_id'), 'unique' => 1),
			'mp_catg_id' => array('column' => 'mp_catg_id', 'unique' => 0),
			'mp_country_id' => array('column' => 'mp_country_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'mp_mem_price_id' => 1,
			'org_id' => 1,
			'mp_catg_id' => 1,
			'mp_country_id' => 1,
			'mp_price_year' => '',
			'mp_price_month' => '',
			'created_by' => 1,
			'created_on' => '2018-06-09 08:35:57',
			'updated_by' => 1,
			'updated_on' => '2018-06-09 08:35:57'
		),
	);

}
