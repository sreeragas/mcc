<?php
/**
 * ConciergePartnerServiceOfferingCountry Fixture
 */
class ConciergePartnerServiceOfferingCountryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'proc_offering_country_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'comment' => 'for company identification'),
		'proc_service_type' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk->service_type.st_service_type_id'),
		'proc_partner_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'FK -> partner_info.partner_id'),
		'proc_service_offering_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk->partner_service_offering.service_offering_id'),
		'proc_city' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk->iconcierge_country_city.	cs_city_id'),
		'proc_currency_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'proc_price' => array('type' => 'decimal', 'null' => false, 'default' => '0.00', 'length' => '10,2', 'unsigned' => false),
		'proc_working_days' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 25, 'collate' => 'latin1_swedish_ci', 'comment' => 'save data seperated with comma. 1-Mon,2-Tue,3-Wed,4-Tur,5-Fri,6-Sat,7-Sun', 'charset' => 'latin1'),
		'proc_working_time_start' => array('type' => 'time', 'null' => true, 'default' => null),
		'proc_working_time_end' => array('type' => 'time', 'null' => true, 'default' => null),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'proc_offering_country_id', 'unique' => 1),
			'proc_service_type' => array('column' => 'proc_service_type', 'unique' => 0),
			'pro_service_offering_id' => array('column' => 'proc_service_offering_id', 'unique' => 0),
			'proc_city' => array('column' => 'proc_city', 'unique' => 0),
			'proc_currency_id' => array('column' => 'proc_currency_id', 'unique' => 0),
			'proc_partner_id' => array('column' => 'proc_partner_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'proc_offering_country_id' => 1,
			'org_id' => 1,
			'proc_service_type' => 1,
			'proc_partner_id' => 1,
			'proc_service_offering_id' => 1,
			'proc_city' => 1,
			'proc_currency_id' => 1,
			'proc_price' => '',
			'proc_working_days' => 'Lorem ipsum dolor sit a',
			'proc_working_time_start' => '08:17:37',
			'proc_working_time_end' => '08:17:37',
			'created_by' => 1,
			'created_on' => '2018-06-28 08:17:37',
			'updated_by' => 1,
			'updated_on' => '2018-06-28 08:17:37'
		),
	);

}
