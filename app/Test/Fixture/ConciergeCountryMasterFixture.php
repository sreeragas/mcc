<?php
/**
 * ConciergeCountryMaster Fixture
 */
class ConciergeCountryMasterFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'cm_country_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'primary'),
		'cm_country_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cm_country_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 55, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cm_vat_percentage' => array('type' => 'decimal', 'null' => false, 'default' => '0.00', 'length' => '5,2', 'unsigned' => false),
		'cm_oth_tax_percent' => array('type' => 'decimal', 'null' => false, 'default' => '0.00', 'length' => '5,2', 'unsigned' => false),
		'cm_base_currency' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'cm_phonecode' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 6, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'cm_country_id', 'unique' => 1),
			'cm_base_currency' => array('column' => 'cm_base_currency', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'cm_country_id' => 1,
			'cm_country_code' => 'Lorem ipsum dolor ',
			'cm_country_name' => 'Lorem ipsum dolor sit amet',
			'cm_vat_percentage' => '',
			'cm_oth_tax_percent' => '',
			'cm_base_currency' => 1,
			'created_by' => 1,
			'created_on' => '2018-06-04 07:54:19',
			'updated_by' => 1,
			'updated_on' => '2018-06-04 07:54:19',
			'cm_phonecode' => 1
		),
	);

}
