<?php
/**
 * ConciergeServiceTypesServingLocation Fixture
 */
class ConciergeServiceTypesServingLocationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'stl_service_locations_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'comment' => 'for company identification'),
		'stl_service_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk from service_type table'),
		'stl_country_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'stl_city_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk from country_city table'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'stl_service_locations_id', 'unique' => 1),
			'stl_service_type_id' => array('column' => 'stl_service_type_id', 'unique' => 0),
			'stl_city_id' => array('column' => 'stl_city_id', 'unique' => 0),
			'stl_country_id' => array('column' => 'stl_country_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'stl_service_locations_id' => 1,
			'org_id' => 1,
			'stl_service_type_id' => 1,
			'stl_country_id' => 1,
			'stl_city_id' => 1,
			'created_by' => 1,
			'created_on' => '2018-06-07 14:51:24',
			'updated_by' => 1,
			'updated_on' => '2018-06-07 14:51:24'
		),
	);

}
