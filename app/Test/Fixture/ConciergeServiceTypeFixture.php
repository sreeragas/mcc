<?php
/**
 * ConciergeServiceType Fixture
 */
class ConciergeServiceTypeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'st_service_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'comment' => 'for company identification'),
		'st_service_catg_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'reference from service_category table'),
		'st_description' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'st_image' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'st_service_charges_if_us' => array('type' => 'decimal', 'null' => false, 'default' => '0.00', 'length' => '10,2', 'unsigned' => false),
		'st_service_charges_currency_if_us' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 9, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk -> from currency master'),
		'st_booking_terms' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'st_service_type_id', 'unique' => 1),
			'st_service_catg_id' => array('column' => 'st_service_catg_id', 'unique' => 0),
			'st_service_charges_currency_if_us' => array('column' => 'st_service_charges_currency_if_us', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB', 'comment' => 'used to save detail portion of service_booking_master')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'st_service_type_id' => 1,
			'org_id' => 1,
			'st_service_catg_id' => 1,
			'st_description' => 'Lorem ipsum dolor sit amet',
			'st_image' => 'Lorem ipsum dolor sit amet',
			'st_service_charges_if_us' => '',
			'st_service_charges_currency_if_us' => 1,
			'st_booking_terms' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created_by' => 1,
			'created_on' => '2018-06-07 14:52:31',
			'updated_by' => 1,
			'updated_on' => '2018-06-07 14:52:31'
		),
	);

}
