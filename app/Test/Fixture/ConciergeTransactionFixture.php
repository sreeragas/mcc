<?php
/**
 * ConciergeTransaction Fixture
 */
class ConciergeTransactionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'ts_order_id' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false),
		'ts_txn_line_no' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 6, 'unsigned' => false, 'comment' => '(Line number within an order)  *** system to display the items the way, user entered'),
		'ts_member_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'ts_order_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'ts_membership_category_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'ts_mr_mem_request_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'value if it is from a new membership purchase'),
		'ts_service_category_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'ts_service_type_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'ts_service_offering_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'ts_booked_for_country' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'ts_booked_for_city' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'ts_partner_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'partner_id if party is selected'),
		'ts_service_required_from_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'ts_service_required_to_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'ts_service_required_time' => array('type' => 'time', 'null' => true, 'default' => null),
		'ts_service_required_upto' => array('type' => 'time', 'null' => true, 'default' => null),
		'ts_service_hours' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '5,2', 'unsigned' => false),
		'ts_journey_type' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 4, 'unsigned' => false, 'comment' => '1->oneway 2->return'),
		'ts_country_travel_from' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'ts_city_travel_from' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'ts_country_travel_to' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'ts_city_travel_to' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'ts_adults' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 6, 'unsigned' => false),
		'ts_children' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 6, 'unsigned' => false),
		'ts_preferred_carrier_name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ts_airline_membership_no' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'comment' => 'Enter the Airline Club membership details', 'charset' => 'latin1'),
		'ts_detailed_requirements' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 5000, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ts_vehicle_type' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'ts_accomodation_details' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 2000, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ts_starting_point_city' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk->city_master.city_code'),
		'ts_starting_other_location' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ts_currency' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'ts_unit_rate' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'ts_vat_perecentage' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '5,2', 'unsigned' => false),
		'ts_vat_amount' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'ts_discount_coupon_code' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 30, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ts_discount_percentage' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'ts_qty' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 6, 'unsigned' => false),
		'ts_amount' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'ts_disc_amt' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'ts_net_amount' => array('type' => 'decimal', 'null' => true, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'ts_pay_reference' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'ts_paid_on_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'ts_order_id', 'unique' => 1),
			'ts_service_category_id' => array('column' => 'ts_service_category_id', 'unique' => 0),
			'ts_service_type_id' => array('column' => 'ts_service_type_id', 'unique' => 0),
			'ts_service_offering_id' => array('column' => 'ts_service_offering_id', 'unique' => 0),
			'ts_booked_from_country' => array('column' => 'ts_booked_for_country', 'unique' => 0),
			'ts_booked_from_city' => array('column' => 'ts_booked_for_city', 'unique' => 0),
			'ts_partner_id' => array('column' => 'ts_partner_id', 'unique' => 0),
			'ts_country_travel_from' => array('column' => 'ts_country_travel_from', 'unique' => 0),
			'ts_city_travel_from' => array('column' => 'ts_city_travel_from', 'unique' => 0),
			'ts_country_travel_to' => array('column' => 'ts_country_travel_to', 'unique' => 0),
			'ts_city_travel_to' => array('column' => 'ts_city_travel_to', 'unique' => 0),
			'ts_vehicle_type' => array('column' => 'ts_vehicle_type', 'unique' => 0),
			'ts_starting_point_city' => array('column' => 'ts_starting_point_city', 'unique' => 0),
			'ts_currency' => array('column' => 'ts_currency', 'unique' => 0),
			'ts_membership_category_id' => array('column' => 'ts_membership_category_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'ts_order_id' => 1,
			'org_id' => 1,
			'ts_txn_line_no' => 1,
			'ts_member_id' => 1,
			'ts_order_date' => '2018-07-07 13:45:47',
			'ts_membership_category_id' => 1,
			'ts_mr_mem_request_id' => 1,
			'ts_service_category_id' => 1,
			'ts_service_type_id' => 1,
			'ts_service_offering_id' => 1,
			'ts_booked_for_country' => 1,
			'ts_booked_for_city' => 1,
			'ts_partner_id' => 1,
			'ts_service_required_from_date' => '2018-07-07',
			'ts_service_required_to_date' => '2018-07-07',
			'ts_service_required_time' => '13:45:47',
			'ts_service_required_upto' => '13:45:47',
			'ts_service_hours' => '',
			'ts_journey_type' => 1,
			'ts_country_travel_from' => 1,
			'ts_city_travel_from' => 1,
			'ts_country_travel_to' => 1,
			'ts_city_travel_to' => 1,
			'ts_adults' => 1,
			'ts_children' => 1,
			'ts_preferred_carrier_name' => 'Lorem ipsum dolor sit amet',
			'ts_airline_membership_no' => 'Lorem ipsum dolor sit amet',
			'ts_detailed_requirements' => 'Lorem ipsum dolor sit amet',
			'ts_vehicle_type' => 1,
			'ts_accomodation_details' => 'Lorem ipsum dolor sit amet',
			'ts_starting_point_city' => 1,
			'ts_starting_other_location' => 'Lorem ipsum dolor sit amet',
			'ts_currency' => 1,
			'ts_unit_rate' => '',
			'ts_vat_perecentage' => '',
			'ts_vat_amount' => '',
			'ts_discount_coupon_code' => 'Lorem ipsum dolor sit amet',
			'ts_discount_percentage' => '',
			'ts_qty' => 1,
			'ts_amount' => '',
			'ts_disc_amt' => '',
			'ts_net_amount' => '',
			'ts_pay_reference' => 'Lorem ipsum dolor sit amet',
			'ts_paid_on_date' => '2018-07-07 13:45:47'
		),
	);

}
