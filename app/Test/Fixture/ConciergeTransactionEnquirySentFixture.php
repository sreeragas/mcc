<?php
/**
 * ConciergeTransactionEnquirySent Fixture
 */
class ConciergeTransactionEnquirySentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'tse_enq_sent_id' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false),
		'tse_ts_order_id' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk -> concierge_transaction'),
		'tse_pr_partner_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk-> partnership info'),
		'tse_price_quoted' => array('type' => 'decimal', 'null' => true, 'default' => '0.00', 'length' => '10,2', 'unsigned' => false),
		'tse_price_agreed' => array('type' => 'decimal', 'null' => true, 'default' => '0.00', 'length' => '10,2', 'unsigned' => false),
		'tse_remarks' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 550, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tse_offer_document' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tse_offer_supporting_document' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tse_offer_valid_upto date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'tse_enq_sent_id', 'unique' => 1),
			'tse_pr_partner_id' => array('column' => 'tse_pr_partner_id', 'unique' => 0),
			'tse_ts_order_id' => array('column' => 'tse_ts_order_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'tse_enq_sent_id' => 1,
			'org_id' => 1,
			'tse_ts_order_id' => 1,
			'tse_pr_partner_id' => 1,
			'tse_price_quoted' => '',
			'tse_price_agreed' => '',
			'tse_remarks' => 'Lorem ipsum dolor sit amet',
			'tse_offer_document' => 'Lorem ipsum dolor sit amet',
			'tse_offer_supporting_document' => 'Lorem ipsum dolor sit amet',
			'tse_offer_valid_upto date' => '2018-07-25 14:32:04'
		),
	);

}
