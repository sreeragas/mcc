<?php
/**
 * ConciergeCountryCity Fixture
 */
class ConciergeCountryCityFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'cs_city_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'cs_country_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index', 'comment' => 'reference from country_currency table'),
		'cs_city_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cs_city_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'cs_city_id', 'unique' => 1),
			'cs_country_id' => array('column' => 'cs_country_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'cs_city_id' => 1,
			'cs_country_id' => 1,
			'cs_city_code' => 'Lorem ipsum dolor ',
			'cs_city_name' => 'Lorem ipsum dolor sit amet',
			'created_by' => 1,
			'created_on' => '2018-06-06 12:04:57',
			'updated_by' => 1,
			'updated_on' => '2018-06-06 12:04:57'
		),
	);

}
