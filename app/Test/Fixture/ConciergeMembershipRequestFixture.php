<?php
/**
 * ConciergeMembershipRequest Fixture
 */
class ConciergeMembershipRequestFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'mr_mem_request_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'comment' => 'for company identification'),
		'mr_member_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk from member_datas table'),
		'mr_requested_on' => array('type' => 'date', 'null' => false, 'default' => null),
		'mr_old_membership_category_id' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false, 'key' => 'index', 'comment' => 'the value will be 0 , in cases other than membership upgrade process'),
		'mr_upgrade_reason' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'mr_membership_catg' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk from membership_category table'),
		'mr_membership_form_created_by' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'comment' => 'not to use this column now', 'charset' => 'latin1'),
		'mr_fee_payment_ref' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'mr_membership_start_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => 'if fee payment is done thru ONLINE payment, the value becomes the date on which payment is made'),
		'mr_membership_end_date date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => 'IF fee payment is done, start_date+365 days'),
		'mr_toh_order_id' => array('type' => 'float', 'null' => false, 'default' => '0', 'unsigned' => false, 'key' => 'index', 'comment' => 'this is the reference from transaction table in order to keep track of cash transaction'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'mr_mem_request_id', 'unique' => 1),
			'mr_membership_catg' => array('column' => 'mr_membership_catg', 'unique' => 0),
			'mr_member_id' => array('column' => 'mr_member_id', 'unique' => 0),
			'mr_toh_order_id' => array('column' => 'mr_toh_order_id', 'unique' => 0),
			'mr_old_membership_category_id' => array('column' => 'mr_old_membership_category_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'mr_mem_request_id' => 1,
			'org_id' => 1,
			'mr_member_id' => 1,
			'mr_requested_on' => '2018-07-04',
			'mr_old_membership_category_id' => 1,
			'mr_upgrade_reason' => 'Lorem ipsum dolor sit amet',
			'mr_membership_catg' => 1,
			'mr_membership_form_created_by' => 'Lorem ipsum dolor ',
			'mr_fee_payment_ref' => 'Lorem ipsum dolor sit amet',
			'mr_membership_start_date' => '2018-07-04',
			'mr_membership_end_date date' => '2018-07-04',
			'mr_toh_order_id' => 1,
			'created_by' => 1,
			'created_on' => '2018-07-04 13:09:04',
			'updated_by' => 1,
			'updated_on' => '2018-07-04 13:09:04'
		),
	);

}
