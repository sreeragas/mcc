<?php
/**
 * ConciergeServingCountry Fixture
 */
class ConciergeServingCountryFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'sct_serv_country_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false),
		'sct_service_category_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'sct_country_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'sct_city_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'sct_serv_country_id', 'unique' => 1),
			'sct_service_category_id' => array('column' => 'sct_service_category_id', 'unique' => 0),
			'sct_country_id' => array('column' => 'sct_country_id', 'unique' => 0),
			'sct_city_id' => array('column' => 'sct_city_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB', 'comment' => 'only these countries pirticular service categories are available')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'sct_serv_country_id' => 1,
			'org_id' => 1,
			'sct_service_category_id' => 1,
			'sct_country_id' => 1,
			'sct_city_id' => 1,
			'created_by' => 1,
			'created_on' => '2018-06-07 14:49:25',
			'updated_by' => 1,
			'updated_on' => '2018-06-07 14:49:25'
		),
	);

}
