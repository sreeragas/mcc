<?php
/**
 * ConciergeServiceTypesMembershipAccess Fixture
 */
class ConciergeServiceTypesMembershipAccessFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'stm_mem_access_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'key' => 'index', 'comment' => 'for company identification'),
		'stm_st_service_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk->service_types.st_service_type_id'),
		'stm_membership_catg' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk->membership_category.mc_catg_id'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'stm_mem_access_id', 'unique' => 1),
			'org_id' => array('column' => array('org_id', 'stm_st_service_type_id', 'stm_membership_catg'), 'unique' => 1),
			'stm_st_service_type_id' => array('column' => 'stm_st_service_type_id', 'unique' => 0),
			'stm_membership_catg' => array('column' => 'stm_membership_catg', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'stm_mem_access_id' => 1,
			'org_id' => 1,
			'stm_st_service_type_id' => 1,
			'stm_membership_catg' => 1,
			'created_by' => 1,
			'created_on' => '2018-06-13 14:46:25',
			'updated_by' => 1,
			'updated_on' => '2018-06-13 14:46:25'
		),
	);

}
