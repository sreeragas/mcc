<?php
/**
 * ConciergePartnershipRequest Fixture
 */
class ConciergePartnershipRequestFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'pr_partner_request_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'comment' => 'for company identification'),
		'pr_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_email_id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 155, 'key' => 'unique', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_pwd' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 8, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_country_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'pr_requested_on' => array('type' => 'date', 'null' => false, 'default' => null),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'pr_partner_request_id', 'unique' => 1),
			'pr_email_id' => array('column' => 'pr_email_id', 'unique' => 1),
			'pr_country_id' => array('column' => 'pr_country_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'pr_partner_request_id' => 1,
			'org_id' => 1,
			'pr_name' => 'Lorem ipsum dolor sit amet',
			'pr_email_id' => 'Lorem ipsum dolor sit amet',
			'pr_pwd' => 'Lorem ',
			'pr_country_id' => 1,
			'pr_requested_on' => '2018-06-14',
			'created_by' => 1,
			'created_on' => '2018-06-14 15:37:56',
			'updated_by' => 1,
			'updated_on' => '2018-06-14 15:37:56'
		),
	);

}
