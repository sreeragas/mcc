<?php
/**
 * VehicleType Fixture
 */
class VehicleTypeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 64, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'user_count' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'iwash_request_count' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'sort_order' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB', 'comment' => 'make necessary changes in concierge_atribute_list table against vehicle_type entry')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'created' => '2018-08-03 16:19:05',
			'modified' => '2018-08-03 16:19:05',
			'name' => 'Lorem ipsum dolor sit amet',
			'user_count' => '',
			'iwash_request_count' => '',
			'sort_order' => 1
		),
	);

}
