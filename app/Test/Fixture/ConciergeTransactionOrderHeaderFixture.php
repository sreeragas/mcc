<?php
/**
 * ConciergeTransactionOrderHeader Fixture
 */
class ConciergeTransactionOrderHeaderFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'toh_order_id' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false),
		'toh_agent_id' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false, 'key' => 'index', 'comment' => 'admin_user_id , if the booking is done by back office staff on behalf of a member'),
		'toh_member_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'toh_order_date' => array('type' => 'date', 'null' => false, 'default' => null),
		'toh_status' => array('type' => 'string', 'null' => false, 'default' => 'A', 'length' => 1, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'toh_order_id', 'unique' => 1),
			'toh_member_id' => array('column' => 'toh_member_id', 'unique' => 0),
			'toh_agent_id' => array('column' => 'toh_agent_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'toh_order_id' => 1,
			'org_id' => 1,
			'toh_agent_id' => 1,
			'toh_member_id' => 1,
			'toh_order_date' => '2018-07-04',
			'toh_status' => 'Lorem ipsum dolor sit ame',
			'created_by' => 1,
			'created_on' => '2018-07-04 13:15:17',
			'updated_by' => 1,
			'updated_on' => '2018-07-04 13:15:17'
		),
	);

}
