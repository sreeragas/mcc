<?php
/**
 * ConciergeAtributeMapping Fixture
 */
class ConciergeAtributeMappingFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'concierge_atribute_mapping';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'atm_mapping_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'atm_service_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk from service type '),
		'atm_at_list_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk from attribute_list table'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'atm_mapping_id', 'unique' => 1),
			'atm_service_type_id_2' => array('column' => array('atm_service_type_id', 'atm_at_list_id'), 'unique' => 1),
			'atm_service_type_id' => array('column' => 'atm_service_type_id', 'unique' => 0),
			'atm_at_list_id' => array('column' => 'atm_at_list_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'atm_mapping_id' => '',
			'atm_service_type_id' => 1,
			'atm_at_list_id' => 1
		),
	);

}
