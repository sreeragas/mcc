<?php
/**
 * ConciergeCurrencyMaster Fixture
 */
class ConciergeCurrencyMasterFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'cr_currency_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'primary'),
		'cr_symbol' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cr_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cr_short_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'cr_decimals' => array('type' => 'integer', 'null' => false, 'default' => '2', 'length' => 4, 'unsigned' => false),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'cr_currency_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'cr_currency_id' => 1,
			'cr_symbol' => 'Lorem ipsum dolor ',
			'cr_name' => 'Lorem ipsum dolor sit amet',
			'cr_short_name' => 'Lorem ipsum dolor sit amet',
			'cr_decimals' => 1,
			'created_by' => 1,
			'created_on' => '2018-06-06 08:03:46',
			'updated_by' => 1,
			'updated_on' => '2018-06-06 08:03:46'
		),
	);

}
