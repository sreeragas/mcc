<?php
/**
 * ConciergePartnershipInfo Fixture
 */
class ConciergePartnershipInfoFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'pr_partner_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'pr_partner_request_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'unique', 'comment' => 'FK -> partnership_request.partner_request_id'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'comment' => 'for company identification'),
		'pr_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_country' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk -> country_currency.country_id'),
		'pr_requested_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'pr_logo' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_registered_address' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_city' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk->city'),
		'pr_tel' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_web' => array('type' => 'string', 'null' => false, 'default' => 'NotAp.com', 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_contact_person' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_contact_person_email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_contact_person_mobile' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'key' => 'unique', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_trade_license' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_registered_at' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_valid_upto' => array('type' => 'date', 'null' => false, 'default' => null),
		'pr_trade_license_doc' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pr_other_doc image' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'pr_partner_id', 'unique' => 1),
			'pr_contact_person_mobile' => array('column' => 'pr_contact_person_mobile', 'unique' => 1),
			'pr_partner_request_id_2' => array('column' => 'pr_partner_request_id', 'unique' => 1),
			'pr_country' => array('column' => 'pr_country', 'unique' => 0),
			'pr_city' => array('column' => 'pr_city', 'unique' => 0),
			'pr_partner_request_id' => array('column' => 'pr_partner_request_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'pr_partner_id' => 1,
			'pr_partner_request_id' => 1,
			'org_id' => 1,
			'pr_name' => 'Lorem ipsum dolor sit amet',
			'pr_country' => 1,
			'pr_requested_on' => '2018-06-25 09:52:58',
			'pr_logo' => 'Lorem ipsum dolor sit amet',
			'pr_registered_address' => 'Lorem ipsum dolor sit amet',
			'pr_city' => 1,
			'pr_tel' => 'Lorem ipsum dolor sit amet',
			'pr_web' => 'Lorem ipsum dolor sit amet',
			'pr_contact_person' => 'Lorem ipsum dolor sit amet',
			'pr_contact_person_email' => 'Lorem ipsum dolor sit amet',
			'pr_contact_person_mobile' => 'Lorem ipsum dolor ',
			'pr_trade_license' => 'Lorem ipsum dolor sit amet',
			'pr_registered_at' => 'Lorem ipsum dolor sit amet',
			'pr_valid_upto' => '2018-06-25',
			'pr_trade_license_doc' => 'Lorem ipsum dolor sit amet',
			'pr_other_doc image' => 'Lorem ipsum dolor sit amet',
			'created_by' => 1,
			'created_on' => '2018-06-25 09:52:58',
			'updated_by' => 1,
			'updated_on' => '2018-06-25 09:52:58'
		),
	);

}
