<?php
/**
 * ConciergePartnerServiceNationalityPref Fixture
 */
class ConciergePartnerServiceNationalityPrefFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'concierge_partner_service_nationality_prefs';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'psnp_nation_pref_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false),
		'psnp_partner_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'psnp_service_offering_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'psnp_service_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'psnp_nationality_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'psnp_nation_pref_id', 'unique' => 1),
			'psnp_partner_id' => array('column' => 'psnp_partner_id', 'unique' => 0),
			'psnp_service_offering_id' => array('column' => 'psnp_service_offering_id', 'unique' => 0),
			'psnp_service_type_id' => array('column' => 'psnp_service_type_id', 'unique' => 0),
			'psnp_nationality_id' => array('column' => 'psnp_nationality_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'psnp_nation_pref_id' => 1,
			'org_id' => 1,
			'psnp_partner_id' => 1,
			'psnp_service_offering_id' => 1,
			'psnp_service_type_id' => 1,
			'psnp_nationality_id' => 1
		),
	);

}
