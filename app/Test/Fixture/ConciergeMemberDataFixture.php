<?php
/**
 * ConciergeMemberData Fixture
 */
class ConciergeMemberDataFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'concierge_member_datas';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'md_member_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'comment' => 'for company identification'),
		'md_member_login_user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk from user table'),
		'md_requested_on' => array('type' => 'date', 'null' => false, 'default' => null),
		'md_membership_catg' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk from membership_category table'),
		'md_membership_form_created_by' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'comment' => 'not to use this column now', 'charset' => 'latin1'),
		'md_fee_payment_ref' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'md_membership_start_date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => 'if fee payment is done thru ONLINE payment, the value becomes the date on which payment is made'),
		'md_membership_end_date date' => array('type' => 'date', 'null' => true, 'default' => null, 'comment' => 'IF fee payment is done, start_date+365 days'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'md_member_id', 'unique' => 1),
			'mr_member_login_user_id' => array('column' => 'md_member_login_user_id', 'unique' => 0),
			'md_membership_catg' => array('column' => 'md_membership_catg', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'md_member_id' => 1,
			'org_id' => 1,
			'md_member_login_user_id' => 1,
			'md_requested_on' => '2018-07-04',
			'md_membership_catg' => 1,
			'md_membership_form_created_by' => 'Lorem ipsum dolor ',
			'md_fee_payment_ref' => 'Lorem ipsum dolor sit amet',
			'md_membership_start_date' => '2018-07-04',
			'md_membership_end_date date' => '2018-07-04',
			'created_by' => 1,
			'created_on' => '2018-07-04 14:28:25',
			'updated_by' => 1,
			'updated_on' => '2018-07-04 14:28:25'
		),
	);

}
