<?php
/**
 * ConciergePartnerServiceOffering Fixture
 */
class ConciergePartnerServiceOfferingFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'pro_service_offering_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'org_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'length' => 6, 'unsigned' => false, 'comment' => 'for company identification'),
		'pro_partner_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'FK -> partner_info.partner_id'),
		'pro_service_catg_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'pro_service_type' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk->service_type.st_service_type_id'),
		'pro_product_code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pro_product_image' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 155, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'pro_service_details' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 500, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'pro_service_terms' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 1000, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'pro_price' => array('type' => 'decimal', 'null' => false, 'default' => '0.00', 'length' => '10,2', 'unsigned' => false),
		'pro_currency_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 9, 'unsigned' => false, 'key' => 'index', 'comment' => 'fk->currency_master.currency_id'),
		'created_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'updated_by' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'updated_on' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'pro_service_offering_id', 'unique' => 1),
			'pro_currency_id' => array('column' => 'pro_currency_id', 'unique' => 0),
			'pro_service_type' => array('column' => 'pro_service_type', 'unique' => 0),
			'pro_partner_id' => array('column' => 'pro_partner_id', 'unique' => 0),
			'pro_service_catg_id' => array('column' => 'pro_service_catg_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'pro_service_offering_id' => 1,
			'org_id' => 1,
			'pro_partner_id' => 1,
			'pro_service_catg_id' => 1,
			'pro_service_type' => 1,
			'pro_product_code' => 'Lorem ipsum dolor sit amet',
			'pro_product_image' => 'Lorem ipsum dolor sit amet',
			'pro_service_details' => 'Lorem ipsum dolor sit amet',
			'pro_service_terms' => 'Lorem ipsum dolor sit amet',
			'pro_price' => '',
			'pro_currency_id' => 1,
			'created_by' => 1,
			'created_on' => '2018-06-26 10:34:39',
			'updated_by' => 1,
			'updated_on' => '2018-06-26 10:34:39'
		),
	);

}
