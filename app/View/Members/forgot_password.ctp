<?php // echo $this->Flash->render(); ?>
<div class="mdl-card mdl-shadow--6dp">
    <?php echo $this->Session->flash('auth'); ?>
    <div class="mdl-card__supporting-text">
        <h5>Forgot Password - Members</h5>
    </div>

    <div class="mdl-card__title mdl-color--primary-custom mdl-color-text--white">
        
        <img src="<?php echo $this->base . '/admin_assets/'; ?>images/Apex_Logo.png" alt="apexServices" />
    </div>
    
     <?php echo $this->Form->create('User', array('url' => array('controller' => 'concierge_member_datas', 'action' => 'forgot_password'))); ?>
        <div class="mdl-card__supporting-text">
        
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <?php echo $this->Form->input('email',array('type'=>'text', 'class' => 'mdl-textfield__input','label' => false,'required'=>true)); ?>
<!--                <input class="mdl-textfield__input" type="text" name="username" id="username" maxlength="30" />-->
                <label class="mdl-textfield__label" for="email">Email Address</label>
            </div>
           

        </div>
        <div class="mdl-card__supporting-text mdl-typography--text-center">
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-color--yellow-A400">Reset Password</button>
            <p></p>
                    <a class="btn btn-danger btn-sm form-control" href="#" onClick="javascipt:window.location.href = '<?php echo $this->Html->url(array("controller" => "concierge_members", "action" => "login")); ?>'" >Go to Login</a>
                    <p></p>
        </div>

    <?php echo $this->Form->end(); ?>
</div>

<div class="">
