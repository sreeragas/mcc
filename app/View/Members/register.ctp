<style>
    .reg-form-section {
        margin-top: 85px;
    }
</style>


<body>
    <?php echo $this->Form->create('User', array('enctype' => 'multipart/form-data', 'url' => array('controller' => 'members', 'action' => 'register'))); ?>
    <div class="container">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4 panel panel-info reg-form-section">
                    <br>

                    <h4 class="text-center">Register Now</h4>
                    <div class="form-group">
                        <label for="usr">Full Name:</label>
                        <?php echo $this->Form->input('name', array('class' => 'form-control', 'id' => 'name', 'label' => FALSE, 'required' => TRUE)); ?>
                    </div>
                    <div class="form-group">
                        <label for="usr">Last Name:</label>
                        <?php echo $this->Form->input('lname', array('class' => 'form-control', 'id' => 'lname', 'label' => FALSE, 'required' => TRUE)); ?>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Email ID (user id):</label>
                        <?php echo $this->Form->input('email', array('type' => "text", 'class' => 'form-control', 'id' => 'email', 'label' => FALSE, 'required' => TRUE, 'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')); ?>
                    </div> 
                    <div class="form-group">
                        <label for="pwd">Create a password:</label>
                        <?php echo $this->Form->input('password', array('type' => "password", 'class' => 'form-control', 'id' => 'password', 'label' => FALSE, 'required' => TRUE)); ?>

                    </div> 
                    <div class="form-group">
                        <label for="pwd">Confirm Password:</label>
                        <?php echo $this->Form->input('password_retype', array('type' => "password", 'class' => 'form-control', 'id' => 'password_retype', 'label' => FALSE, 'required' => TRUE)); ?>

                    </div> 
                    <div class="form-group">
                        <label for="pwd">Mobile Number:</label>
                        <?php echo $this->Form->input('mobile_number', array('type' => "text", 'class' => 'form-control inputNumberOnly', 'id' => 'mobile_number', 'label' => FALSE, 'required' => TRUE)); ?>

                    </div> 
                    <div class="form-group">
                        <label for="pwd">Nationality:</label>
                        <?php echo $this->Form->input('nationality', array('class' => 'form-control', 'id' => 'nationality', 'label' => FALSE, 'required' => TRUE, 'options' => array('' => '---Select Nationality---', $country_list_opt))); ?>

                    </div> 

                    <div class="form-group">
                        <label>DOB:</label>
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control input-sm inputNoEnter" name="dob" placeholder="YYYY-MM-DD"  value="" required="TRUE" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Photo:</label>
                        <?php echo $this->Form->input("image", array("type" => "file", "size" => "45", 'error' => false, 'placeholder' => '', 'required' => TRUE, 'label' => array('class' => 'mdl-textfield__label', 'text' => ''))); ?>

                    </div> 

                    <button class="btn btn-info btn-sm form-control">Register</button>
                    <p></p>
                    <a class="btn btn-danger btn-sm form-control" href="#" onClick="javascipt:window.location.href = '<?php echo $this->Html->url(array("controller" => "admin_logins", "action" => "index")); ?>'" >Already a member ?  Sign In</a>
                    <p></p>
                    </ul>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>		
    </div>
    <?php echo $this->Form->end(); ?>
</body>
<script type="text/javascript">
    $(function () {

        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    });
</script>