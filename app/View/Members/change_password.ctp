<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <header class="demo-header mdl-layout__header mdl-color--white mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
        	<span class="mdl-layout-title">Change Password</span>
        	<div class="mdl-layout-spacer"></div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                    <i class="material-icons">search</i>
                </label>
                <div class="mdl-textfield__expandable-holder">
                    <input class="mdl-textfield__input" id="search" type="text">
                    <label class="mdl-textfield__label" for="search">Enter your query...</label>
                </div>
            </div>
        </div>
    </header>
	<?php include('../View/Layouts/sidebar.ctp'); ?>
    <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
        	<div id="message"><?php echo $this->Session->flash();?></div>
            <div class="mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col padding-15">
            	<?php echo $this->Form->create('AdminLogin',array('url' => '/Supervisiors/update_password/', 'autocomplete'=>'off')); ?>
                <div class="row">
                  <?php echo $this->Form->input('id',array('class' => 'form-control','type'=>'hidden','value'=>$user_detail['id'])); ?>
                  <div class="col-sm-6">
                      <div class="form-group">
                        <?php echo $this->Form->input('email',array('class' => 'form-control','type'=>'email','id' => 'email','label' => 'Email Address', 'readonly'=>true, 'value'=>$user_detail['email'])); ?>
                      </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="form-group">
                        <?php echo $this->Form->input('current_password',array('class' => 'form-control','type'=>'password', 'label' => 'Current Password', 'required'=>true)); ?>
                      </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="form-group">
                        <?php echo $this->Form->input('new_password',array('class' => 'form-control','type'=>'password','label' => 'New Password', 'required'=>true)); ?>
                      </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="form-group">
                        <?php echo $this->Form->input('confirm_password',array('class' => 'form-control','type'=>'password','label' => 'Confirm Password', 'required'=>true)); ?>
                      </div>
                  </div>


                <div class="text-right col-sm-12">
                	<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Update</button>
                </div>
            	<?php echo $this->Form->end(__('')); ?>
            </div>
        </div>
    </main>
</div>
