<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <header class="demo-header mdl-layout__header mdl-color--white mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
            <span class="mdl-layout-title">Manage Profile</span>
            <div class="mdl-layout-spacer"></div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                    <i class="material-icons">search</i>
                </label>
                <div class="mdl-textfield__expandable-holder">
                    <input class="mdl-textfield__input" id="search" type="text">
                    <label class="mdl-textfield__label" for="search">Enter your query...</label>
                </div>
            </div>
        </div>
    </header>
    <?php include('../View/Layouts/sidebar.ctp'); ?>
    <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
            <div id="message"><?php echo $this->Session->flash(); ?></div>
            <div class="mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col padding-15">
                <?php echo $this->Form->create('AdminLogin', array('url' => '/Supervisiors/update_profile/', 'autocomplete' => 'off')); ?>
                <div class="row">
                    <?php echo $this->Form->input('id', array('class' => 'form-control', 'type' => 'hidden', 'value' => $user_detail['id'])); ?>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => 'Name', 'required' => true, 'value' => $user_detail['name'])); ?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <?php echo $this->Form->input('email', array('class' => 'form-control', 'type' => 'email', 'id' => 'email', 'label' => 'Email Address', 'readonly' => true, 'value' => $user_detail['email'])); ?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <?php echo $this->Form->input('mobile_number', array('class' => 'form-control', 'label' => 'Mobile Number', 'required' => true, 'value' => $user_detail['mobile_number'])); ?>
                        </div>
                    </div>

                    <!--                  <div class="col-sm-6">
                                          <div class="form-group">
                    <?php echo $this->Form->input('team_size', array('class' => 'form-control', 'min' => '0', 'label' => 'Team Size', 'value' => $user_detail['team_size'])); ?>
                                          </div>
                                      </div>-->

                   


                    <div class="col-sm-12 text-right">
                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Update</button>
                    </div>
<?php echo $this->Form->end(__('')); ?>
                </div>
            </div>
        </div>
    </main>
</div>
