<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <header class="demo-header mdl-layout__header mdl-color--white mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">Dashboard</span>
        </div>
      </header>
      <?php include('../View/Layouts/sidebar.ctp'); ?>
     
      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
          <div class="demo-charts mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-grid">
          	<div class="demo-chart mdl-cell mdl-cell--4-col mdl-cell--3-col-desktop">
            	<div class="chart1 chart" data-percent="73"><span class="percentage">73%</span></div>
                <label>Consectetur </label>
            </div>
            <div class="demo-chart mdl-cell mdl-cell--4-col mdl-cell--3-col-desktop">
            	<div class="chart2 chart" data-percent="81"><span class="percentage">81%</span></div>
                <label>Reprehenderit </label>
            </div>
            <div class="demo-chart mdl-cell mdl-cell--4-col mdl-cell--3-col-desktop">
            	<div class="chart3 chart" data-percent="90"><span class="percentage">90%</span></div>
                <label>Non dolore</label>
            </div>
            <div class="demo-chart mdl-cell mdl-cell--4-col mdl-cell--3-col-desktop">
            	<div class="chart4 chart" data-percent="60"><span class="percentage">60%</span></div>
                <label>Adipisicing ea</label>
            </div>
          </div>
          <div class="demo-graphs mdl-shadow--2dp mdl-color--white mdl-cell mdl-cell--8-col">
            <canvas id="canvas"></canvas>
          </div>
          <div class="demo-cards mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-grid mdl-grid--no-spacing">
            <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop">
              <div class="mdl-card__title mdl-card--expand mdl-color--white">
              </div>
              <div class="mdl-card__supporting-text">
                <h4>Payments recieved</h4>
                <p>72,500 <span>AED</span></p>
              </div>
              <div class="mdl-card__actions mdl-typography--text-center">
                <a href="#" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">View</a>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>