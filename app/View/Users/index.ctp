<div  class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <header class="demo-header mdl-layout__header mdl-color--white mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
            <span class="mdl-layout-title">Users</span>
            <div class="mdl-layout-spacer"></div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                    <i class="material-icons">search</i>
                </label>
                <div class="mdl-textfield__expandable-holder">
                    <input class="mdl-textfield__input" id="search" type="text">
                    <label class="mdl-textfield__label" for="search">Enter your query...</label>
                </div>
            </div>
        </div>
    </header>
      <?php echo $this->element('sidebar'); ?>
    <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
            <div class="mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col">
                <div class="table-responsive">
                    <form>
                        <table class="mdl-data-table mdl-js-data-table" width="100%">
                            <thead>
                                <tr>
                                    <th width="65px">
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-all">
                                            <input type="checkbox" id="checkbox-all" class="mdl-checkbox__input">
                                        </label>
                                    </th>
                                    <th class="mdl-data-table__cell--non-numeric">First Name</th>
                                     <th class="mdl-data-table__cell--non-numeric">Last Name</th>
                                      <th class="mdl-data-table__cell--non-numeric">Username</th>
                                    <th class="mdl-data-table__cell--non-numeric">Email Address</th>
                                    <th class="mdl-data-table__cell--non-numeric">Role</th>
                                    <th class="mdl-data-table__cell--non-numeric">Status</th>
                                    <th width="50px">
                                        Actions
                                        <div class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="hdrbtn">
                                            <i class="material-icons">more_vert</i>
                                        </div>
                                        <ul class="mdl-menu mdl-js-menu mdl-js-ripple-effect mdl-menu--bottom-right" for="hdrbtn">
                                            <li class="mdl-menu__item">Active</li>
                                            <li class="mdl-menu__item">Inactive</li>
                                            <li class="mdl-menu__item mdl-color-text--red-600">Delete</li>
                                        </ul>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                             <?php foreach($users as $user): ?>   
                                <tr>
                                    <td>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                                            <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input">
                                        </label>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $user['User']['first_name']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $user['User']['last_name']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $user['User']['username']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $user['User']['email']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric">
 <?php
                                        $mode=array();
                                        $obj = ClassRegistry::init('Role');
                                        $RoleAssigned = $obj->find('first',array('conditions'=>array('Role.role_id'=>$user['User']['role']))); 
                        if($RoleAssigned){

echo $RoleAssigned['Role']['role_name'];
}
                                        ?>
</td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php if($user['User']['status']=="1"){ echo "Active"; } else { echo  "Inactive"; } ?></td>
                                    <td class="actions-td-cls"><a ng-click="UserEditForm('<?php echo $user['User']['id']; ?>');" href="#" title="Edit"><i class=" material-icons">create</i></a><a href="#" title="Delete"  ng-click="UserDelete('<?php echo $user['User']['id']; ?>');"><i class="material-icons">delete</i></a></td>
                                </tr>
                               <?php endforeach; ?>

                            </tbody>
                        </table>
                    </form>
                </div>
                 <?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'pagination'));?>
<?php echo $this->Paginator->numbers(array(   'class' => 'pagination'     ));?>
<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'pagination'));?>
                <ul class="pagination">

                    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                    <li class="active"><a href="#!">1</a></li>
                    <li><a href="#!">2</a></li>
                    <li><a href="#!">3</a></li>
                    <li><a href="#!">4</a></li>
                    <li><a href="#!">5</a></li>
                    <li><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                </ul>

            </div>
        </div>
    </main>
</div>
<button data-toggle="modal" data-target="#myModalAdd" class="add-button mdl-button mdl-js-button mdl-button--fab mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400" title="ADD">
    <i class="material-icons">add</i>
</button>
<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add User</h4>
            </div>
                 
            <div class="modal-body">

                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input ng-model="UsersModel.first_name" class="mdl-textfield__input" type="text" id="first_name" maxlength="30" >
                            <label class="mdl-textfield__label" for="uname">First name</label>
                            <span class="mdl-textfield__error">Letters and spaces only</span>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                             <input ng-model="UsersModel.last_name" class="mdl-textfield__input" type="text" id="last_name" maxlength="30" >
                            <label class="mdl-textfield__label" for="uemail">Last name</label>
                            <span class="mdl-textfield__error">Letters and spaces only</span>
                        </div>
                    </div>
                <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input ng-model="UsersModel.username" class="mdl-textfield__input" type="text" id="uname" maxlength="30" >
                            <label class="mdl-textfield__label" for="uname">User name</label>
                            <span class="mdl-textfield__error">Letters and spaces only</span>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input ng-model="UsersModel.email" class="mdl-textfield__input" type="text" name="email" id="uemail" maxlength="30" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                            <label class="mdl-textfield__label" for="uemail">Email Address</label>
                            <span class="mdl-textfield__error">Not valid address</span>
                        </div>
                    </div>

                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input ng-model="UsersModel.password" class="mdl-textfield__input" type="password" id="password" maxlength="30" >
                            <label class="mdl-textfield__label" for="password">Password</label>
                            <span class="mdl-textfield__error">Letters and spaces only</span>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                             <input ng-model="UsersModel.re_password" class="mdl-textfield__input" type="password" id="re_passwords" maxlength="30" >
                            <label class="mdl-textfield__label" for="re_password">Repeat Password</label>
                            <span class="mdl-textfield__error">Letters and spaces only</span>
                        </div>
                    </div>

                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <select ng-model="UsersModel.role" class="mdl-textfield__input" id="urole">
                                <option value="">Role</option>
                               <option ng-repeat="role in allrole" ng-value="role.role_id">{{role.role_name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <select ng-model="UsersModel.status" class="mdl-textfield__input" id="ustatus">
                                <option value="">Status</option>
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="mdl-dialog__actions">
                    <button type="button" ng-click="UserSave(UsersModel);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Submit</button>
                    <button type="button" class="mdl-button mdl-js-button mdl-color-text--grey-600" data-dismiss="modal">Cancel</button>
                </div>
            </div>
              
        </div>

    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update User</h4>
            </div>
                 
            <div class="modal-body">

                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input value="ajay" ng-model="UsersEditModel.first_name" class="mdl-textfield__input" type="text" id="first_name" maxlength="30" >
<!--                            <label class="mdl-textfield__label" for="uname">First name</label>-->
                            <span class="mdl-textfield__error">Letters and spaces only</span>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                             <input ng-model="UsersEditModel.last_name" class="mdl-textfield__input" type="text" id="last_name" maxlength="30" >
<!--                            <label class="mdl-textfield__label" for="uemail">Last name</label>-->
                            <span class="mdl-textfield__error">Letters and spaces only</span>
                        </div>
                    </div>
                    
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <select ng-model="UsersEditModel.role" class="mdl-textfield__input" id="urole">
                                <option value="">Role</option>
                                <option ng-repeat="role in allrole" ng-value="role.role_id">{{role.role_name}}</option>
                                </select>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">

                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <select ng-model="UsersEditModel.status" class="mdl-textfield__input" id="ustatus">
                                <option ng-selected="UsersEditModel.status==''" value="">Status</option>
                                <option  value="1" ng-selected="UsersEditModel.status=='1'">Active</option>
                                <option value="0" ng-selected="UsersEditModel.status=='0'">Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="mdl-dialog__actions">
                    <button type="button" ng-click="UserUpdate(UsersEditModel);" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Submit</button>
                    <button type="button" class="mdl-button mdl-js-button mdl-color-text--grey-600" data-dismiss="modal">Cancel</button>
                </div>
            </div>
              
        </div>

    </div>
</div>

<!-- Modal -->
<div id="myModalDelete" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete confirmation</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-danger" ng-click="DeleteConfirm();" data-dismiss="modal">Delete</button>
      </div>
    </div>

  </div>
</div>