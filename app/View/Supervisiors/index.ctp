<?php //include 'includes/header.php';     ?>
<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <header class="demo-header mdl-layout__header mdl-color--white mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
            <span class="mdl-layout-title">Manage Users</span>
            <div class="mdl-layout-spacer"></div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                    <i class="material-icons">search</i>
                </label>
                <div class="mdl-textfield__expandable-holder">
                    <input class="mdl-textfield__input" id="search" type="text">
                    <label class="mdl-textfield__label" for="search">Enter your query...</label>
                </div>
            </div>
        </div>
    </header>
    <?php include('../View/Layouts/sidebar.ctp'); ?>
    <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
            <div id="message"><?php echo $this->Session->flash(); ?></div>
            <div class="mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col">
                <div class="table-responsive">
                    <?php echo $this->Form->create('Supervisior', array('action' => 'update_leader_status')); ?>
                    <table class="mdl-data-table mdl-js-data-table" width="100%" data-toggle="table" data-pagination="false" data-pagination-h-align="left" data-pagination-detail-h-align="right">
                        <thead>
                            <tr>
                                <th data-field="checkbox">
                                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-all">
                                        <input type="checkbox" id="checkbox-all" class="mdl-checkbox__input">
                                    </label>
                                    <?php //echo $this->Form->input('', array('type'=>'checkbox', 'class'=>'mdl-checkbox__input', 'checked'=>'','id'=>'select_all', 'value'=>'')); ?>

                                </th>
                                <th class="mdl-data-table__cell--non-numeric" data-field="name"><?php echo $this->Paginator->sort('AdminLogin.name', $title = 'Name'); ?></th>
                                <th class="mdl-data-table__cell--non-numeric" data-field="mobile_number"><?php echo $this->Paginator->sort('AdminLogin.mobile_number', $title = 'Mobile'); ?></th>
                                <th class="mdl-data-table__cell--non-numeric" data-field="designation"><?php echo $this->Paginator->sort('AdminLogin.designation', $title = 'Designation'); ?></th>
                                <th class="mdl-data-table__cell--non-numeric" data-field="region_id"><?php echo $this->Paginator->sort('AdminLogin.StsRegionMaster.region', $title = 'Region'); ?></th>
                                <th class="mdl-data-table__cell--non-numeric" data-field="department_id"><?php echo $this->Paginator->sort('AdminLogin.StsDepartmentMaster.department', $title = 'Department'); ?></th>
                                <th class="mdl-data-table__cell--non-numeric" data-field="email" ><?php echo $this->Paginator->sort('AdminLogin.email', $title = 'Email'); ?></th>
                                <th class="mdl-data-table__cell--non-numeric" data-field="type" ><?php echo $this->Paginator->sort('AdminLogin.admin_type_id', $title = 'Type'); ?></th>
                                <th class="mdl-data-table__cell--non-numeric" data-field="status" data-sortable="false"><?php echo $this->Paginator->sort('AdminLogin.status', $title = 'Status'); ?></th>
                                <th width="50px">
                                    Actions
                                    <div class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="hdrbtn">
                                        <i class="material-icons">more_vert</i>
                                    </div>
                                    <ul class="mdl-menu mdl-js-menu mdl-js-ripple-effect mdl-menu--bottom-right" for="hdrbtn">
                                        <li class="mdl-menu__item"><?php
                                            $deleteoptions = array('label' => 'Active', 'class' => 'btn btn-link', 'div' => false, 'name' => 'active');
                                            echo $this->Form->end($deleteoptions);
                                            ?>
                                        </li>
                                        <li class="mdl-menu__item"><?php
                                            $deleteoptions = array('label' => 'Inactive', 'class' => 'btn btn-link', 'div' => false, 'name' => 'inactive');
                                            echo $this->Form->end($deleteoptions);
                                            ?></li>

                                        <!-- <li class="mdl-menu__item mdl-color-text--red-600"><?php
                                        $deleteoptions = array('label' => 'Delete', 'class' => 'btn btn-link', 'div' => false);
                                        echo $this->Form->end($deleteoptions);
                                        ?></li> -->
                                    </ul>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($supervisiors as $supervisior) : ?>
                                <?php $id = $supervisior['AdminLogin']['id']; ?>
                                <tr>
                                    <td>
                                        <?php //echo $this->Form->input('', array('type'=>'checkbox', 'class'=>'mdl-checkbox', 'checked'=>'','id'=>'chkbxchk'.$id, 'value'=>$id));   ?>
                                        <?php //echo $this->Form->checkbox('',array('value' => $id,'name' => $id[],));   ?>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox<?php echo $id; ?>">
                                            <input type="checkbox" name="id[]" value="<?php echo $id; ?>" id="checkbox<?php echo $id; ?>" class="mdl-checkbox__input" hiddenField="false">
                                        </label>
                                    </td>



                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $supervisior['AdminLogin']['name']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $supervisior['AdminLogin']['mobile_number']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $supervisior['AdminLogin']['designation']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $supervisior['StsRegionMaster']['region']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $supervisior['StsDepartmentMaster']['department']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $supervisior['AdminLogin']['email']; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php echo $admin_type_list[$supervisior['AdminLogin']['admin_type_id']]; ?></td>
                                    <td class="mdl-data-table__cell--non-numeric"><?php
                                        if ($supervisior['AdminLogin']['status'] == 1) {
                                            echo "Active";
                                        } else {
                                            echo "Inactive";
                                        }
                                        ?></td>
                                    <td class="actions-td-cls">
                                        <?php //echo $this->js->link("","/buildings/edit/id=".$id,array('htmlAttributes' => array('data-toggle' => 'modal','data-target' => '#myModalEdit')));   ?>
                                        <?php echo $this->form->PostLink('',array('controller'=>'Supervisiors','action'=>'delete', $id),array('confirm'=>'Are you sure to delete?','class'=>'deletefaicon'));   ?>
                                        <a href="#myModalEdit" onclick="" role="button" data-toggle="modal" data-id="<?php echo $id; ?>" class="edit-item"><i class="material-icons">edit</i></a>
                                    </td>

                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="">
                        <div class="col-sm-6">
                            <ul class="pagination">
                                <?php
                                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
                                echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                                ?>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <p class="text-muted text-right"><small>
                                    <?php
                                    echo $this->Paginator->counter(array(
                                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                    ));
                                    ?>  </small>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </main>
</div>
<button data-toggle="modal" data-target="#myModal" class="add-button mdl-button mdl-js-button mdl-button--fab mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400" title="ADD">
    <i class="material-icons">add</i>
</button>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <?php echo $this->Form->create('AdminLogin', array('url' => array('controller' => 'Supervisiors', 'action' => 'add'))); ?>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add User</h4>
            </div>
            <div class="modal-body">
                <div class="mdl-grid">

                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('name', array('autocomplete'=>'off', 'value'=>'','class' => 'mdl-textfield__input', 'maxlength' => '30', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Name'), 'div' => false, 'required' => true)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('email', array('class' => 'mdl-textfield__input', 'maxlength' => '50', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Email'), 'div' => false, 'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('password', array('autocomplete'=>'off', 'value'=>'', 'autocomplete'=>"off" ,'class' => 'mdl-textfield__input', 'maxlength' => '20', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Password'), 'div' => false, 'required' => true)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('repassword', array('class' => 'mdl-textfield__input', 'maxlength' => '20', 'type' => 'password', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Confirm Password',), 'div' => false, 'required' => true)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('mobile_number', array('class' => 'mdl-textfield__input inputNumberOnly', 'maxlength' => '20', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Mobile Number'), 'div' => false)); ?>
                        </div>
                    </div>
                    <!--                        <div class="mdl-cell mdl-cell--6-col">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <?php echo $this->Form->input('team_size', array('class' => 'mdl-textfield__input inputNumberOnly', 'maxlength' => '5', 'type' => 'text', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Team Size'), 'div' => false)); ?>
                                                </div>
                                            </div>-->
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label">
                            <?php echo $this->Form->input('region_id', array('class' => 'mdl-selectfield__select', 'options' => $region_list, 'label' => array('class' => 'mdl-selectfield__label', 'text' => 'Region'), 'div' => false)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label">
                            <?php echo $this->Form->input('department_id', array('class' => 'mdl-selectfield__select', 'options' => $department_list, 'label' => array('class' => 'mdl-selectfield__label', 'text' => 'Department'), 'div' => false)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('designation', array('class' => 'mdl-textfield__input', 'maxlength' => '20', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Designation'), 'div' => false)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label">
                            <?php echo $this->Form->input('status', array('class' => 'mdl-selectfield__select', 'options' => array('1' => 'Active', '2' => 'Inactive'), 'label' => array('class' => 'mdl-selectfield__label'), 'div' => false)); ?>
                        </div>
                    </div>

                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label">
                            <?php echo $this->Form->input('admin_type_id', array('class' => 'mdl-selectfield__select', 'options' => array('' => 'Choose Type', $admin_type_list), 'label' => array('Text' => 'Type', 'class' => 'mdl-selectfield__label'), 'div' => false, 'required' => true)); ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <div class="mdl-dialog__actions">
                    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Submit</button>
                    <button type="button" class="mdl-button mdl-js-button mdl-color-text--grey-600" data-dismiss="modal">Cancel</button>
                    <?php echo $this->Form->end(__('')); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Start For Edit window -->
<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEdit">
    <?php echo $this->Form->create('AdminLogin', array('url' => array('controller' => 'supervisiors', 'action' => 'edit'))); ?>

    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit User</h4>
            </div>
            <div class="modal-body">
                <div class="mdl-grid">


                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('name', array('autocomplete'=>'off','class' => 'mdl-textfield__input', 'maxlength' => '30', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Name'), 'div' => false, 'id' => 'name')); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('email', array('class' => 'mdl-textfield__input', 'maxlength' => '50', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Email'), 'div' => false, 'id' => 'email', 'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('mobile_number', array('class' => 'mdl-textfield__input inputNumberOnly', 'maxlength' => '20', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Mobile Number'), 'div' => false, 'id' => 'mobile_number')); ?>
                        </div>
                    </div>
                    <!--                        <div class="mdl-cell mdl-cell--6-col">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <?php echo $this->Form->input('team_size', array('class' => 'mdl-textfield__input inputNumberOnly', 'maxlength' => '5', 'type' => 'text', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Team Size'), 'div' => false, 'id' => 'team_size')); ?>
                                                </div>
                                            </div>-->
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('password', array('id'=>'password', 'value'=>'', 'autocomplete'=>'off','class' => 'mdl-textfield__input', 'maxlength' => '20', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Password'), 'div' => false)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label">
                            <?php echo $this->Form->input('region_id', array('id'=> 'region_id', 'class' => 'mdl-selectfield__select', 'options' => $region_list, 'label' => array('class' => 'mdl-selectfield__label', 'text' => 'Region'), 'div' => false)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label">
                            <?php echo $this->Form->input('department_id', array('id'=> 'department_id','class' => 'mdl-selectfield__select', 'options' => $department_list, 'label' => array('class' => 'mdl-selectfield__label', 'text' => 'Department'), 'div' => false)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('designation', array('id'=> 'designation','class' => 'mdl-textfield__input', 'maxlength' => '20', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Designation'), 'div' => false)); ?>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label">
                            <?php echo $this->Form->input('status', array('class' => 'mdl-selectfield__select', 'id' => 'status', 'options' => array('1' => 'Active', '2' => 'Inactive'), 'label' => array('class' => 'mdl-selectfield__label'), 'div' => false)); ?>
                        </div>
                    </div>

                    <div class="mdl-cell mdl-cell--6-col">
                        <div class="mdl-selectfield mdl-js-selectfield mdl-selectfield--floating-label">
                            <?php echo $this->Form->input('admin_type_id', array('class' => 'mdl-selectfield__select', 'id' => 'admin_type', 'options' => $admin_type_list, 'label' => array('Text' => 'Type', 'class' => 'mdl-selectfield__label'), 'div' => false, 'required' => true)); ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <div class="mdl-dialog__actions">
                    <?php echo $this->Form->hidden('id', array('id' => 'id')); ?>
                    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Submit</button>
                    <button type="button" class="mdl-button mdl-js-button mdl-color-text--grey-600" data-dismiss="modal">Cancel</button>
                    <?php echo $this->Form->end(__('')); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>



    jQuery(document).on('click', '.edit-item', function () {

        var row_id = jQuery(this).data('id');
        jQuery.ajax({
            url: baseUrl + 'Supervisiors/editform/' + row_id,
            method: 'GET',
            dataType: 'json',
            cache: false,
            success: function (data) {
                var id = data.AdminLogin.id;
                $("#id").val(id);
                var password = '';
                $("#password").val(password);
                var name = data.AdminLogin.name;
                $("#name").val(name);
                var email = data.AdminLogin.email;
                $("#email").val(email);
                if ($("#email").closest("div").hasClass("is-invalid") == true) {
                    $("#email").closest("div").removeClass("is-invalid");
                    $("#email").closest("div").addClass("is-dirty");
                }
                var mobile_number = data.AdminLogin.mobile_number;
                $("#mobile_number").val(mobile_number);
                var status = data.AdminLogin.status;
                $("#status").val(status);
                var designation = data.AdminLogin.designation;
                $("#designation").val(designation);
                var type = data.AdminLogin.admin_type_id;
                $("#admin_type").val(type).attr('selected', 'selected');
                var region = data.AdminLogin.region_id;
                $("#region_id").val(region).attr('selected', 'selected');
                var department = data.AdminLogin.department_id;
                $("#department_id").val(department).attr('selected', 'selected');
            }
        });
    });



</script>
