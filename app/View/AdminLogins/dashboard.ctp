<meta http-equiv="refresh" content="300">
<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">

    <header class="demo-header mdl-layout__header mdl-color--white mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
            <span class="mdl-layout-title">Dashboard</span>
        </div>
    </header>
    <?php include('../View/Layouts/sidebar.ctp'); ?>

    <main class="mdl-layout__content mdl-color--grey-100">

        <!--<p>&nbsp;</p>-->

        <div class="dashboard-content">
            <div id="message"><?php echo $this->Session->flash(); ?></div>


            <?php
            if (AuthComponent::user('admin_type_id') == Configure::read('Admin_Type_Staff')) {
                ?>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="widget-bg-color-icon card-box mdl-shadow--2dp">

                            <div class="row">


                                <div class="mdl-card__supporting-text">
                                    <div class="mdl-card__actions mdl-typography--text-center">
                                        <h4>MY DETAILS</h4>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="widget-bg-color-icon card-box mdl-shadow--2dp">

                                        <div class="text-center">
                                            <h3><b class="counter"><font color="#557ce8"><?php echo $member_single_detail['AdminLogin']['name']." ".$member_single_detail['AdminLogin']['lname']; ?></b> </font></h3>
                                            <p> <font color="#62c72e"><?php echo ($member_single_detail['AdminLogin']['admin_type_id'] == C_ADMIN_TYPE_ADMIN) ? 'Admin' : 'Member'; ?> </font></p>
                                            <hr>
                                            <p>Email : <font color="#62c72e"><?php echo $member_single_detail['AdminLogin']['email']; ?> </font></p>
                                            <p>Mobile : <font color="#62c72e"><?php echo $member_single_detail['AdminLogin']['mobile_number']; ?> </font></p>
                                            <p>Mobile : <font color="#62c72e"><?php echo $member_single_detail['AdminLogin']['mobile_number']; ?> </font></p>
                                            <p>Nationality : <font color="#62c72e"><?php echo $member_single_detail['StsCountryMaster']['cm_country_name']; ?> </font></p>
                                            <p>DOB : <font color="#62c72e"><?php echo $this->Concierge->myDateDMY($member_single_detail['AdminLogin']['dob']); ?> </font></p>
                                            <p>Status : <font color="#62c72e"><?php echo ($member_single_detail['AdminLogin']['status'] == 1) ? 'Active' : 'In Active'; ?> </font></p>


                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>


                            </div>



                            <div class="row">


                                <div class="mdl-card__supporting-text">


                                    <div class="mdl-card__actions mdl-typography--text-center">

                                        <a href="<?php echo $this->Html->url(array('controller' => 'Members', 'action' => 'manage')); ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Change My Details</a>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>

            <?php } else { ?>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="widget-bg-color-icon card-box mdl-shadow--2dp">

                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="mdl-card__supporting-text">
                                            <div class="mdl-card__actions mdl-typography--text-center">
                                                Total Members : <b><?php echo count($member_full_list); ?></b
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-md-6 col-lg-6">
                                    <div class="mdl-card__supporting-text">
                                        <div class="mdl-card__actions mdl-typography--text-center">

                                            <a href="<?php echo $this->Html->url(array('controller' => 'Members', 'action' => 'manage')); ?>" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Manage Members</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="clearfix"></div>-->

                        </div>
                    </div>
                </div>

            <?php } ?>
        </div>
</div>





<!--  end -->



<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(document).on("click", ".edit-item", function () {
            var row_id = jQuery(this).data('id');
            jQuery.ajax({
                url: baseUrl + 'Users/view_user/' + row_id,
                method: 'GET',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    var fullname = data.User.fullname;
                    var mobile_number = data.User.mobile_number;
                    var country_code = data.User.country_code;
                    var apartment_number = data.User.apartment_number;
                    var building_name = data.Building.name;
                    jQuery(".c_name_view").html(fullname);
                    jQuery(".mobile").html(country_code + '-' + mobile_number);
                    jQuery(".aprtment_no").html(apartment_number);
                    jQuery(".byilding_name_view").html(building_name);

                }
            });
        });

    });

    jQuery(document).on("click", ".picture", function () {
        var id = jQuery(this).data('id');
        jQuery.ajax({
            url: baseUrl + 'IwashRequests/view_log/' + id,
            method: 'GET',
            cache: false,
            dataType: 'json',
            success: function (data) {
                var request_id = data.IwashLog.iwash_requests_id;
                var id = data.IwashLog.iwash_log_id;
                jQuery(".pic_request_id").val(request_id);
                jQuery(".picture_log_id").val(id);
                //jQuery("#pic_customer").val(data.IwashRequest.id);
            }
        });
    });

    jQuery(document).on("change", "#payment_type", function () {
        if ($(this).val() ==<?php echo Configure::read('payment_type_card'); ?>)
            $('#receipt_number').attr('required', true);
        else
            $('#receipt_number').attr('required', false);
    });

    jQuery(document).ready(function () {
        jQuery(document).on("click", ".collect-receipt", function () {
            var row_id = jQuery(this).data('id');
            jQuery.ajax({
                url: baseUrl + 'ManualIwashRequests/updateReceiptNumberForm/' + row_id,
                method: 'GET',
                cache: false,
                dataType: 'json',
                success: function (data) {
                    var id = row_id;
                    jQuery('#request_id').val(data.IwashRequest.id);
                    jQuery('#transaction_id').val(data.Transaction.transaction_id);
                    jQuery('#price').val(data.Transaction.amount);
                    jQuery('#amount_collected').val(data.Transaction.amount);
                }
            });
        });

    });

    $('#payment_type').multiselect({
        columns: 1,
        placeholder: 'Select',
        search: true,
        showCheckbox: false,
        selectAll: false,
        onOptionClick: function (element, option) {
            $('.ms-options').css('display', 'none');
            $('.ms-search input[type="text"]').val('');
            $('.ms-search input[type="text"]').keyup();
            $('.ms-options ul li').removeClass('selected');
        },
    });
</script>
<script type="text/javascript">
    setTimeout(function () {
        location = ''
    }, 60 * 1000)
</script>