<?php echo $this->Flash->render('auth'); ?>
<div class="mdl-card mdl-shadow--6dp">
    <?php echo $this->Session->flash('auth'); ?>
    <div class="mdl-card__supporting-text">
        <h5>PROMPT Login</h5>
    </div>
    <div class="mdl-card__title mdl-color--primary-custom mdl-color-text--white">
        <img  width="60%" src="<?php echo $this->base . '/admin_assets/'; ?>images/Apex_Logo.png" alt='Apex Services' />
    </div>
    <?php echo $this->Form->create('AdminLogin'); ?>
    <div class="mdl-card__supporting-text">

        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <?php echo $this->Form->input('email', array('class' => 'mdl-textfield__input', 'label' => false)); ?>
            <label class="mdl-textfield__label" for="email">Email Address</label>
        </div>
        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            <?php echo $this->Form->input('password', array('class' => 'mdl-textfield__input', 'label' => false)); ?>
            <label class="mdl-textfield__label" for="userpass">Password</label>
        </div>

    </div>
    <div class="mdl-card__supporting-text mdl-typography--text-center">
        <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-color--yellow-A400">Log in</button>
    </div>


    <?php echo $this->Form->end(__('')); ?>
</div>
<div class="mdl-card__supporting-text mdl-typography--text-center">
    <a class="btn btn-danger btn-sm form-control" href="#" onClick="javascipt:window.location.href = '<?php echo $this->Html->url(array("controller" => "members", "action" => "register")); ?>'" >Not a member ?  Sign Up</a>

</div>

<?php
//echo $this->Html->link( "Add A New User",   array('action'=>'add') ); 
?>