<?php // echo $this->Flash->render(); ?>
<div class="mdl-card mdl-shadow--6dp">
    <?php echo $this->Session->flash('auth'); ?>
     <div class="mdl-card__supporting-text">
        <h5>Back Office Login</h5>
    </div>
    <div class="mdl-card__title mdl-color--primary-custom mdl-color-text--white">
        <img src="<?php echo $this->base . '/admin_assets/'; ?>images/logo.png" alt='Apex Services' />
    </div>
    <?php echo $this->Form->create('AdminLogin'); ?>
        <div class="mdl-card__supporting-text">
        
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <?php echo $this->Form->input('email',array('class' => 'mdl-textfield__input','label' => false)); ?>
<!--                <input class="mdl-textfield__input" type="text" name="username" id="username" maxlength="30" />-->
                <label class="mdl-textfield__label" for="email">Email Address</label>
            </div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <?php echo $this->Form->input('password',array('class' => 'mdl-textfield__input','label' => false)); ?>
<!--                <input class="mdl-textfield__input" type="password" name="password" id="userpass" maxlength="30" />-->
                <label class="mdl-textfield__label" for="userpass">Password</label>
            </div>

        </div>
        <div class="mdl-card__supporting-text mdl-typography--text-center">
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-color--yellow-A400">Log in</button>
        </div>

    <?php echo $this->Form->end(__('Login')); ?>
</div>

<div class="">
<?php //echo $this->Session->flash('auth'); ?>
<?php //echo $this->Form->create('User'); ?>
<!--    <fieldset>
        <legend><?php echo __('Please enter your Email and password'); ?></legend>
        <?php echo $this->Form->input('email');
        echo $this->Form->input('password');
    ?>
    </fieldset>-->
<?php //echo $this->Form->end(__('Login')); ?>
</div>
<?php
 //echo $this->Html->link( "Add A New User",   array('action'=>'add') ); 
?>