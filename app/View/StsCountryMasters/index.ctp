<?php //include 'includes/header.php';      ?>
<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <header class="demo-header mdl-layout__header mdl-color--white mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
            <span class="mdl-layout-title">Country Masters</span>
            <div class="mdl-layout-spacer"></div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                    <i class="material-icons">search</i>
                </label>
                <div class="mdl-textfield__expandable-holder">
                    <input class="mdl-textfield__input" id="search" type="text">
                    <label class="mdl-textfield__label" for="search">Enter your query...</label>
                </div>
            </div>
        </div>
    </header>
    <?php include('../View/Layouts/sidebar.ctp'); ?>
    <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
            <div id="message"><?php echo $this->Session->flash(); ?></div>
            <div class="mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col">
                <div class="padding-15">
                    <?php echo $this->Form->create('StsCountryMaster', array('url' => array('controller' => 'sts_country_masters', 'action' => 'index'))); ?>
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-3" id="divbuilding">
                            <div class="form-group">
                                <?php echo $this->Form->input('cm_country_id', array('class' => 'ms-options-wrap', 'id' => 'cm_country_id', 'label' => 'Country', 'options' => array('' => '---All Country---', $country_list_opt), 'default' => (!empty($cm_country_id)) ? $cm_country_id : '')); ?>
                                <label id="dept-error" class="error" for="country"></label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-8 col-lg-9">
                            <label class="hidden-xs">&nbsp;</label>
                            <div class="form-group">
                                <?php
                                echo $this->Form->end(array(
                                    'label' => 'Search',
                                    'class' => 'btn btn-primary btn-sm',
                                    'div' => false
                                ));
                                ?>
                                &nbsp;<input type="button" value="All Records" class="btn btn-default btn-sm" onClick="javascipt:window.location.href = '<?php echo $this->Html->url(array("controller" => "concierge_country_masters", "action" => "index")); ?>'"/>
                                &nbsp;<input type="button" value="Back" class="btn btn-default btn-sm" onClick="javascipt:window.location.href = '<?php echo $this->Html->url(array("controller" => "concierge_settings", "action" => "index")); ?>'"/>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <?php echo $this->Form->create('StsCountryMaster', array('action' => 'deleteall_contries')); ?>
                        <table class="mdl-data-table mdl-js-data-table" width="100%" data-toggle="table" data-pagination="false" data-pagination-h-align="left" data-pagination-detail-h-align="right" data-filter-control="true">
                            <thead>
                                <tr>
                                    <th width="50px">
                                        Actions
                                        <div class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon" id="hdrbtn">
                                            <i class="material-icons">more_vert</i>
                                        </div>
                                        <ul class="mdl-menu mdl-js-menu mdl-js-ripple-effect mdl-menu--bottom-right" for="hdrbtn">
                                            <li class="mdl-menu__item mdl-color-text--red-600"><?php
                                                $deleteoptions = array('label' => 'Delete', 'class' => 'btn btn-link', 'div' => false);
                                                echo $this->Form->end($deleteoptions);
                                                ?></li>
                                        </ul>
                                    </th>
                                    <th data-field="checkbox">
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-all">
                                            <input type="checkbox" id="checkbox-all" class="mdl-checkbox__input">
                                        </label>
                                    </th>
                                    <th class="mdl-data-table__cell--non-numeric" data-field="cm_country_code"><?php echo $this->Paginator->sort('StsCountryMaster.cm_country_code', $title = 'Country Code'); ?></th>
                                    <th class="mdl-data-table__cell--non-numeric" data-field="cm_country_name"><?php echo $this->Paginator->sort('StsCountryMaster.cm_country_name', $title = 'Country Name'); ?></th>
                                    <th class="mdl-data-table__cell--non-numeric" data-field="cm_phonecode"><?php echo $this->Paginator->sort('StsCountryMaster.cm_phonecode', $title = 'Phone Code'); ?></th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($country_master as $country) : ?>
                                    <?php $id = $country['StsCountryMaster']['cm_country_id']; ?>
                                    <tr>
                                        <td class="actions-td-cls">
                                            <?php echo $this->form->PostLink('', array('controller' => 'StsCountryMasters', 'action' => 'delete_country', $id), array('confirm' => 'Are you sure to delete?', 'class' => 'deletefaicon')); ?>
                                            <a href="#myModalEdit" onclick="" role="button" data-toggle="modal" data-id="<?php echo $id; ?>" class="edit-item"><i class="material-icons">edit</i></a>
                                        </td>
                                        <td>
                                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox<?php echo $id; ?>">
                                                <input type="checkbox" name="id[]" value="<?php echo $id; ?>" id="checkbox<?php echo $id; ?>" class="mdl-checkbox__input" hiddenField="false">
                                            </label>
                                        </td>
                                        <td class="mdl-data-table__cell--non-numeric"><?php echo $country['StsCountryMaster']['cm_country_code']; ?></td>
                                        <td class="mdl-data-table__cell--non-numeric"><?php echo $country['StsCountryMaster']['cm_country_name']; ?></td>
                                        <td class="mdl-data-table__cell--non-numeric"><?php echo $country['StsCountryMaster']['cm_phonecode']; ?></td>

                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        </form>
                        <div class="">
                            <div class="col-sm-6">
                                <ul class="pagination">
                                    <?php
                                    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                                    echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
                                    echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                                    ?>
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <p class="text-muted text-right"><small>
                                        <?php
                                        echo $this->Paginator->counter(array(
                                            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                        ));
                                        ?>  </small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<button data-toggle="modal" data-target="#myModal" class="add-button mdl-button mdl-js-button mdl-button--fab mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400" title="ADD">
    <i class="material-icons">add</i>
</button>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalADD">
    <?php echo $this->Form->create('StsCountryMaster', array('action' => 'add_country')); ?>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Country</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('cm_country_code', array('class' => 'mdl-textfield__input', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Country Code'), 'div' => false, 'required' => true)); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('cm_country_name', array('class' => 'mdl-textfield__input', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Country Name'), 'div' => false, 'required' => true)); ?>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    
                    <div class="col-sm-6">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('cm_phonecode', array('class' => 'mdl-textfield__input', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Phone Code'), 'div' => false, 'required' => true)); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php
                            $status_opt = array('A' => 'Active', 'I' => 'In Active');
                            echo $this->Form->input('cm_status', array('class' => 'mdl-textfield__input', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Status'), 'options' => array($status_opt)));
                            ?>
                            <label id="dept-error" class="error" for="currency"></label>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        &nbsp;
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="mdl-dialog__actions">
                    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Submit</button>
                    <button type="button" class="mdl-button mdl-js-button mdl-color-text--grey-600" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(__('')); ?>
</div>

<!-- Start For Edit window -->
<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEdit">
    <?php echo $this->Form->create('StsCountryMaster', array('action' => 'edit_country')); ?>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Country</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('cm_country_code', array('class' => 'mdl-textfield__input', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Country Code'), 'div' => false, 'id' => 'cm_country_code', 'required' => true)); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('cm_country_name', array('class' => 'mdl-textfield__input', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Country Name'), 'div' => false, 'id' => 'cm_country_name', 'required' => true)); ?>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    
                    <div class="col-sm-6">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php echo $this->Form->input('cm_phonecode', array('class' => 'mdl-textfield__input', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Phone Code'), 'div' => false, 'id' => 'cm_phonecode', 'required' => true)); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                       <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <?php
                            $status_opt = array('A' => 'Active', 'I' => 'In Active');
                            echo $this->Form->input('cm_status', array('id'=>'cm_status', 'class' => 'mdl-textfield__input', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Status'), 'options' => array($status_opt)));
                            ?>
                            <label id="dept-error" class="error" for="currency"></label>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        &nbsp;
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="mdl-dialog__actions">
                    <?php echo $this->Form->hidden('cm_country_id', array('id' => 'id'));
                    ?>
                    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Update</button>
                    <button type="button" class="mdl-button mdl-js-button mdl-color-text--grey-600" data-dismiss="modal">Cancel</button>
                    <?php echo $this->Form->end(__('')); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).on('click', '.edit-item', function () {
        var row_id = jQuery(this).data('id');
        jQuery.ajax({
            url: baseUrl + 'sts_country_masters/editform_country/' + row_id,
            method: 'GET',
            dataType: 'json',
            cache: false,
            success: function (data) {
                var id = data.StsCountryMaster.cm_country_id;
                $("#id").val(id);
                var cm_country_code = data.StsCountryMaster.cm_country_code;
                var cm_country_name = data.StsCountryMaster.cm_country_name;
               
                var cm_phonecode = data.StsCountryMaster.cm_phonecode;
                var cm_status = data.StsCountryMaster.cm_status;

                $("#cm_country_code").val(cm_country_code);
                $("#cm_country_name").val(cm_country_name);
               
                $("#cm_phonecode").val(cm_phonecode);


               
                if (cm_status == '' || cm_status == null) {
                    console.log(cm_status)
                    jQuery('#cm_status').val(0);
                } else {
                    $('#cm_status').val(cm_status);
                }
            }
        });
    });
</script>
<script>
    $('#cm_country_id').multiselect({
        columns: 1,
        placeholder: 'Select',
        search: true,
        showCheckbox: false,
        selectAll: false,
        onOptionClick: function (element, option) {
            $('.ms-options').css('display', 'none');
            $('.ms-search input[type="text"]').val('');
            $('.ms-search input[type="text"]').keyup();
            $('.ms-options ul li').removeClass('selected');
        },
    });
</script>
<!--new file -->
