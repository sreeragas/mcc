<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Apex Services');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		//echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="<?php echo $this->base . '/admin_assets/'; ?>images/android-desktop.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="IyeServices">
    <link rel="apple-touch-icon-precomposed" href="<?php echo $this->base . '/admin_assets/'; ?>images/ios-desktop.png">
    <meta name="msapplication-TileImage" content="<?php echo $this->base . '/admin_assets/'; ?>images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">
    <link rel="shortcut icon" href="<?php echo $this->base . '/admin_assets/'; ?>images/favicon.png">
    <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>css/bootstrap.css">
    
        
    
    <script src="<?php echo $this->base . '/admin_assets/'; ?>js/jquery-2.0.3.min.js"></script>
    <script src="<?php echo $this->base . '/admin_assets/js/bootstrap.min.js'; ?>"></script>
    <script src="<?php echo $this->base . '/admin_assets/js/jquery.mCustomScrollbar.concat.min.js'; ?>"></script>
    <script src="<?php echo $this->base . '/admin_assets/'; ?>js/jquery.cookie.js"></script>
     <script src="https://use.fontawesome.com/51d08972fb.js"></script>
     
</head>
 <?php $controller= $this->request->params['controller'].'Controller'; //echo $controller; die; ?>
<body>
    <style>
		.mdl-layout {
			align-items: center;
		  justify-content: center;
		}
		.mdl-card__supporting-text {
			border-top-width:0px;
		}
		.mdl-layout__content {
			padding: 24px;
			flex: none;
		}
		.mdl-color--primary-custom{
			background:#ffffff;
			box-shadow: 0 0 4px 2px #ccc;
			padding: 25px 0;
		}
		.mdl-color--primary-custom img{
			margin:0 auto;
		}
		#flashMessage.message{
			margin:15px 0;
		}
	</style>
    <div class="mdl-layout mdl-js-layout mdl-color--grey-100">
        <main class="mdl-layout__content">
            <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>
            
        </main>
    </div>

	
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/core.js"></script>
</body>
</html>
