<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header('Content-Type: text/html; charset=utf-8');
header("Pragma: no-cache");
 
$cakeDescription = __d('cake_dev',  Configure::read("PROJECT_NAME"));
$cakeVersion = __d('cake_dev',  Configure::read("PROJECT_NAME"))
?>
<!DOCTYPE html>
<html>
    <head>
	<?php echo $this->Html->charset(); ?>
        <title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
        </title>
	<?php
		//echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <meta name="format-detection" content="telephone=no">
        <?php /*?><meta name="mobile-web-app-capable" content="yes">
        <link rel="icon" sizes="192x192" href="<?php echo $this->base . '/admin_assets/'; ?>images/android-desktop.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="apple-mobile-web-app-title" content="IyeServices">
        <link rel="apple-touch-icon-precomposed" href="<?php echo $this->base . '/admin_assets/'; ?>images/ios-desktop.png">
        <meta name="msapplication-TileImage" content="<?php echo $this->base . '/admin_assets/'; ?>images/touch/ms-touch-icon-144x144-precomposed.png">
        <meta name="msapplication-TileColor" content="#3372DF"><?php */?>
        <link rel="shortcut icon" href="<?php echo $this->base . '/admin_assets/'; ?>images/favicon.png">
        <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>css/bootstrap.css?v=1.1">
        <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>css/material.cyan-light_blue.min.css">
        <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>css/styles.css?v=3.4">
        <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>css/bootstrap-table.css?v=1.2">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>css/mdl-selectfield.min.css">
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/material.min.js"></script>
    	<script src="<?php echo $this->base . '/admin_assets/'; ?>js/mdl-selectfield.min.js?v=1.0"></script>
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/jquery-2.0.3.min.js"></script>
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script src="<?php echo $this->base . '/admin_assets/'; ?>js/bootstrap-table.js?v=1.0"></script>
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/bootstrap-table-filter-control.js"></script>
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/bootstrap-table-flat-json.js"></script>
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/jquery.multiselect.js?v=1.0"></script>
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/jquery.cookie.js"></script>
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/jquery.validate.min.js"></script>
         <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>concierge/css/custom.css" />
        <script src="<?php echo $this->base . '/admin_assets/'; ?>concierge/js/custom.js"></script>
        
        <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>css/jquery.multiselect.css?v=1.1" />
        <link rel="stylesheet" href="<?php echo $this->base . '/admin_assets/'; ?>css/bootstrap-datetimepicker.css" />
        <script src="<?php echo $this->base . '/admin_assets/'; ?>js/moment.js"></script>
		<script src="<?php echo $this->base . '/admin_assets/'; ?>js/bootstrap-datetimepicker.js"></script>
                <script src="https://use.fontawesome.com/51d08972fb.js"></script>

        <script language="javascript">
        //var baseUrl='<?php echo Router::url('/api/', true) ?>'; // FOR FUTUERE POINT OF VIEW
		var baseUrl='<?php echo Router::url('/', true) ?>';
        </script>
    </head>
    <?php $controller= $this->request->params['controller'].'Controller'; //echo $controller; die; ?>
    <body>
		<?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>
		<script src="<?php echo $this->base . '/admin_assets/'; ?>js/core.js?v=1.7"></script>
	</body>
</html>
