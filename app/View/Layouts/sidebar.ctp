<div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
    <header class="demo-drawer-header">
        <img src="<?php echo $this->base . '/admin_assets/'; ?>images/logo.png" class="demo-avatar">
        <div class="demo-avatar-dropdown">
            <span><?php echo AuthComponent::user('name'); ?></span>
            <div class="mdl-layout-spacer"></div>
            <a href="<?php echo $this->Html->url(array('controller' => 'AdminLogins', 'action' => 'logout')); ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--icon mdl-color--white mdl-color-text--red-600" title="Logout">
                <i class="material-icons" role="presentation">power_settings_new</i>
                <span class="visuallyhidden"></span>
            </a>
        </div>
    </header>
    <nav class="mdl-navigation">
        <div id="sidebarmenulist">
            <ul class="sidebarmenulist">
                <?php
               
                    echo '<li class="dashboard"><a href="' . $this->Html->url(array('controller' => 'AdminLogins', 'action' => 'dashboard')) . '"><span></span>Dashboard</a></li>';
                    echo '<li class="teamleaders"><a href="' . $this->Html->url(array('controller' => 'Members', 'action' => 'manage')) . '"><span></span>Manage Member</a></li>';
                ?>


                 <li class="settings">
                        <a href="javascript:void(0);"><span></span>Settings</a>
                        <ul>
                            <?php
                            
                                echo '<li><a href="' . $this->Html->url(array('controller' => 'Supervisiors', 'action' => 'manage_profile')) . '">Manage Profile</a></li>';
                            
                                echo '<li><a href="' . $this->Html->url(array('controller' => 'Supervisiors', 'action' => 'change_password')) . '">Change Password</a></li>';
                            ?>
                        </ul>
                    </li>
               

            </ul>
        </div>
    </nav>
</div>
