<?php

/**
 * Name             :   Sreerag A S
 * Date Created     :   29-6-2018
 * Last Modified    :   29-6-2018
 *
 */
App::uses('AppHelper', 'View/Helper');

class ConciergeHelper extends AppHelper {

    public function getCompanyAddress() {
        return Configure::read('C_OFFICE_ADDRESS');
    }

    public function myDateDMY($date) {
        if ($date != "")
            return date('d-m-Y', strtotime($date));
        else
            return "";
    }

    public function myDateTime12Hrs($datetime) {
        if ($datetime != "")
            return date("d-m-Y g:i A", strtotime($datetime));
        else
            return "";
    }

    public function myDateTimeExpand12Hrs($datetime) {
        if ($datetime != "")
            return date("d-M-Y g:i A", strtotime($datetime));
        else
            return "";
    }

    public function myDateYMD($date) {
        if ($date != "")
            return date('Y-m-d', strtotime($date));
        else
            return "";
    }

    public function myTime12Hrs($time) {
        if ($time != "")
            return date("g:i A", strtotime($time));
        else
            return "";
    }

    
}
