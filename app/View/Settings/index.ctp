<?php //include 'includes/header.php';        ?>
<div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <header class="demo-header mdl-layout__header mdl-color--white mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
            <span class="mdl-layout-title">System Settings</span>
            <div class="mdl-layout-spacer"></div>
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                <label class="mdl-button mdl-js-button mdl-button--icon" for="search">
                    <i class="material-icons">search</i>
                </label>
                <div class="mdl-textfield__expandable-holder">
                    <input class="mdl-textfield__input" id="search" type="text">
                    <label class="mdl-textfield__label" for="search">Enter your query...</label>
                </div>
            </div>
        </div>
    </header>
    <?php include('../View/Layouts/sidebar.ctp'); ?>
    <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
            <div id="message"><?php echo $this->Session->flash(); ?></div>
            <div class="mdl-color--white mdl-shadow--2dp mdl-cell mdl-cell--12-col">
                <div class="padding-15">
                    <?php echo $this->Form->create('Setting', array( 'action' => 'edit')); ?>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                <?php echo $this->Form->input('concierge_video_url', array('value' => $settings['Setting']['concierge_video_url'], 'class' => 'mdl-textfield__input', 'label' => array('class' => 'mdl-textfield__label', 'text' => 'Concierge Video Tutorial URL'), 'div' => false, 'id' => 'concierge_video_url', 'required' => true)); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            &nbsp;
                        </div>
                    </div>
                    
                    

                    <div class="modal-footer">
                        <div class="mdl-dialog__actions">
                            <?php echo $this->Form->hidden('id', array('value' => $settings['Setting']['id'],'id' => 'id'));
                            ?>
                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--yellow-A400">Update</button>
                            <button type="button" class="mdl-button mdl-js-button mdl-color-text--grey-600" data-dismiss="modal">Cancel</button>
                            <?php echo $this->Form->end(__('')); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
</div>

