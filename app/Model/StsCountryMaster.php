<?php
App::uses('AppModel', 'Model');
/**
 * conciergeCountryMasters Model
 *
 * @property Org $Org
 * @property StServiceCatg $StServiceCatg
 */
class StsCountryMaster extends AppModel {
public $primaryKey = 'cm_country_id';

}
