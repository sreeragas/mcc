<?php
/* Create By Ajay Singh
 * 
 */
App::uses('AuthComponent', 'Controller/Component');
 
class Module extends AppModel {
     
    
     
    public $validate = array(
        'module_name' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A module_name is required',
                'allowEmpty' => false
            )
));
     
   
     
    public function alphaNumericDashUnderscore($check) {
        // $data array is passed using the form field name as the key
        // have to extract the value to make the function generic
        $value = array_values($check);
        $value = $value[0];
 
        return preg_match('/^[a-zA-Z0-9_ \-]*$/', $value);
    }
     
    
 
}