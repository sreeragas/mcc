<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
// for social media login - SREERAG - 21-7-2018
//Router::connect('/hybrid_login/*', array( 'controller' => 'users', 'action' => 'hybrid_login'));
//Router::connect('/hybrid_endpoint/*', array( 'controller' => 'users', 'action' => 'hybrid_endpoint'));

// ************* API Router *******************\\\\\

Router::connect('/admin/login', array('controller' => 'admin_logins', 'action' => 'index'));
Router::connect('/member', array('controller' => 'memebers', 'action' => 'register'));
//Router::connect('', array('controller' => 'admin_logins', 'action' => 'index'));
Router::connect('', array('controller' => 'members', 'action' => 'register'));

//Router::connect('/admin/web_login/', array('controller' => 'users', 'action' => 'index'));
// ********** Roles ************//

Router::connect('/api/register',array('controller' => 'MccApi','action' =>'register'));
Router::connect('/api/update',array('controller' => 'MccApi','action' =>'update_user'));
Router::connect('/api/login',array('controller' => 'MccApi','action' =>'login'));


Router::parseExtensions('json');
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
//Router::connect('/', array('controller' => 'users', 'action' => 'web_login'));
Router::connect('/', array('controller' => 'memebrs', 'action' => 'register'));
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
Router::connect('/api', array('controller' => 'AdminLogins', 'action' => 'index'));
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
