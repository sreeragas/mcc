<?php
class TestsController extends AppController {

 
 function index() {
  $this->loadModel('Transaction');
    $this->autoRender=false;
    ini_set('max_execution_time', 1600); //increase max_execution_time to 10 min if data set is very large
    
    $results = $this->Transaction->find('all', array('conditions'=>array('Transaction.payment_status_id'=>3), 'limit'=>5));// set the 
    $filename = "export_".date("Y.m.d").".xls";
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$filename.'"');
    $this->ExportFile($results);
 }

  function ExportFile($records) {
    echo 'Tran ID'."\t".'User ID'."\n";
          
    foreach($records as $record){
      echo $record['Transaction']['transaction_id']."\t".$record['Transaction']['user_id']."\n";
    }
    exit;
  }

}
