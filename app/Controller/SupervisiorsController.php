<?php

App::uses('AppController', 'Controller');

/**
 * Buildings Controller
 *
 * @property Building $Building
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class SupervisiorsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Flash','Common');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(
                'login', 'index', 'plan'
        );
    }

    /**
     * index method
     *
     * @return void
     */

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        // pr('sssssssssss');


        $this->layout = "admin";
        $this->loadModel('AdminLogin');
        $this->loadModel('AdminType');
        $this->loadModel('StsRegionMaster');
        $this->loadModel('StsDepartmentMaster');
        $this->AdminLogin->recursive = 1;
        $this->paginate = array(
            'limit' => 10,
            'order' => array('AdminLogin.name' => 'ASC')
        );
        $data = $this->paginate('AdminLogin');
        // echo '<pre>';
        //    print_r($data); exit;
        $this->set('supervisiors', $data);

        $admin_type_list = $this->AdminType->find('list', array('fields' => array('id', 'name')));
        $region_list = $this->StsRegionMaster->find('list', array('fields' => array('region_id', 'region'),'conditions'=>array('status'=>'A')));
        $department_list = $this->StsDepartmentMaster->find('list', array('fields' => array('department_id', 'department'),'conditions'=>array('status'=>'A')));
        $this->set('admin_type_list', $admin_type_list);
        $this->set('region_list', $region_list);
        $this->set('department_list', $department_list);
        $this->render('/Supervisiors/index');
    }

   
    public function activefitler() {
        $this->layout = "admin";
        $this->loadModel('AdminLogin');
        $this->AdminLogin->recursive = 0;
        $this->paginate = array(
            'limit' => 1000,
            'order' => array('id' => 'desc'));
        $this->set('supervisiors', $this->Paginator->paginate());
        $data = $this->AdminLogin->find('all', array('conditions' => array('AdminLogin.status' => '1')));
        //print_r($data);die;
        $this->set('supervisiors', $data);
        $this->render('/Supervisiors/index');
    }

    public function inactivefitler() {
        $this->layout = "admin";
        $this->loadModel('AdminLogin');
        $this->AdminLogin->recursive = 0;
        $this->paginate = array(
            'limit' => 1000,
            'order' => array('id' => 'desc'));
        $this->set('supervisiors', $this->Paginator->paginate());
        $data = $this->AdminLogin->find('all', array('conditions' => array('AdminLogin.status !=' => '1')));
        $this->set('supervisiors', $data);
        $this->render('/Supervisiors/index');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $this->loadModel('AdminLogin');
            if ($data['AdminLogin']['password'] == $data['AdminLogin']['repassword']) {
                $this->AdminLogin->create();
                $this->AdminLogin->set($this->request->data);
                if ($this->AdminLogin->save($this->request->data)) {
                    if ($this->request->data['AdminLogin']['admin_type_id'] == 1)
                        $this->Flash->success(__('The Admin has been saved.'));
                    else
                        $this->Flash->success(__('The User has been saved.'));

                    return $this->redirect(array('action' => 'index'));
                } else {
                    $this->Flash->error(__('Details could not be saved. Might be user already registered with the same mail id. Please, try again.'));
                }
            } else {
                $this->Flash->error(__('Password & Confirm Password should be same !'));
            }
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function editform($id = null) {
        $this->loadModel('AdminLogin');
        if ($this->request->is('ajax')) {
            $data = $this->AdminLogin->find('first', array(
                'conditions' => array(
                    //'AdminLogin.admin_type_id' => Configure::read('Admin_Type_Supervisor'),
                    'AdminLogin.id' => $id
                )
            ));
            $this->response->body(json_encode($data));
            //Return reponse object to prevent controller from trying to render a view
            return $this->response;
        }
    }

    public function edit() {
        $refer_url = $_SERVER['HTTP_REFERER'];

        $data = $this->request->data;

        $this->loadModel('AdminLogin');

        $id = $data['AdminLogin']['id'];  //exit;
        if ($this->request->is(array('post', 'put'))) {

            $this->AdminLogin->validate = $this->AdminLogin->validateWithoutPassword;

            $this->AdminLogin->set('name', $data['AdminLogin']['name']);
            $this->AdminLogin->set('email', $data['AdminLogin']['email']);
            $this->AdminLogin->set('mobile_number', $data['AdminLogin']['mobile_number']);
            $this->AdminLogin->set('status', $data['AdminLogin']['status']);
            $this->AdminLogin->set('region_id', $data['AdminLogin']['region_id']);
            $this->AdminLogin->set('department_id', $data['AdminLogin']['department_id']);
            $this->AdminLogin->set('designation', $data['AdminLogin']['designation']);
            $this->AdminLogin->set('admin_type_id', $data['AdminLogin']['admin_type_id']);
            $this->AdminLogin->set('id', $data['AdminLogin']['id']);

            $password = $data['AdminLogin']['password'];
            //pr($this->Auth->password($password)); exit;
            if ($password != "") // password changed
                $this->AdminLogin->set('password', $password);
            //echo $this->Auth->password($password); exit;

            if ($this->AdminLogin->save()) {
                $this->Flash->success(__('The User has been saved.'));
                return $this->redirect($refer_url);
            } else {
                $this->Flash->error(__('The User could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Supervisior.' . $this->Supervisior->primaryKey => $id));
            $this->request->data = $this->Supervisior->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    // to delete checked records
    public function deleteall() {
        $this->Session->setFlash(
                __('Not allow to delete !')
        );
        return $this->redirect(array('action' => 'index'));

        exit;
        $refer_url = $_SERVER['HTTP_REFERER'];

        if (!empty($this->data)) {
            //If at lesst 1 check box is selected
            if (isset($this->data) && !empty($this->data['build'])) {
                $selectedReferences = $this->data['build'];

                $flagReferenceAdded = false;
                foreach ($selectedReferences as $singleReference) {
                    //NEW DELETE
                    $this->Supervisior->id = $singleReference;
                    $this->Supervisior->delete();
                    //NEW DELETE
                }
                //After delete
                $this->Session->setFlash(
                        __('Your selected record Are deleted.')
                );
                return $this->redirect($refer_url);
            } else {
                echo "Select at least 1 Record.";
            }
        } else {
            $this->Session->setFlash(
                    __('Please Select at leats one record.')
            );
            return $this->redirect($refer_url);
        }
    }

    public function delete($id) {
        // echo 'ss'; exit;    

        $this->loadModel('AdminLogin');
        $this->AdminLogin->id = $id;

        if ($id == 1) {

            $this->Flash->error(__('Not allow to delete master admin account'));
            return $this->redirect(array('action' => 'index'));
        }
        if (!$this->AdminLogin->exists()) {
            throw new NotFoundException(__('Invalid User'));
        }
        $this->request->allowMethod('post', 'delete');
        try {
            if ($this->AdminLogin->delete()) {
                $this->Flash->success(__('The User has been deleted.'));
            } else {
                $this->Flash->error(__('The User could not be deleted. Please, try again.'));
            }
        } catch (Exception $ex) {
            $this->Flash->error(__("Canot be delete! This staff's logs are exists. So you cannot delete this staff. You can in activate this staff"));
        }



        return $this->redirect(array('action' => 'index'));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Building->recursive = 0;
        $this->set('buildings', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        if (!$this->Building->exists($id)) {
            throw new NotFoundException(__('Invalid building'));
        }
        $options = array('conditions' => array('Building.' . $this->Building->primaryKey => $id));
        $this->set('building', $this->Building->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Building->create();
            if ($this->Building->save($this->request->data)) {
                $this->Flash->success(__('The building has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The building could not be saved. Please, try again.'));
            }
        }
        $buildingTypes = $this->Building->BuildingType->find('list');
        $this->set(compact('buildingTypes'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        if (!$this->Building->exists($id)) {
            throw new NotFoundException(__('Invalid building'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Building->save($this->request->data)) {
                $this->Flash->success(__('The building has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The building could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Building.' . $this->Building->primaryKey => $id));
            $this->request->data = $this->Building->find('first', $options);
        }
        $buildingTypes = $this->Building->BuildingType->find('list');
        $this->set(compact('buildingTypes'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        $this->Building->id = $id;
        if (!$this->Building->exists()) {
            throw new NotFoundException(__('Invalid building'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Building->delete()) {
            $this->Flash->success(__('The building has been deleted.'));
        } else {
            $this->Flash->error(__('The building could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function change_password() {
        $this->layout = "admin";
        $user_detail = $this->Auth->User();
        //pr($user_detail);
        $this->set(compact('user_detail'));
        $this->render('change_password');
    }

    public function update_password() {
        App::uses('AuthComponent', 'Controller/Component');
        $this->loadModel('AdminLogin');
        $data = $this->request->data;
        $id = $data['AdminLogin']['id'];
        $user_details = $this->AdminLogin->find('first', array('conditions' => array('AdminLogin.id' => $id)));
        $old_password = $user_details['AdminLogin']['password'];
        $current_password = AuthComponent::password($data['AdminLogin']['current_password']);
        $new_password = $data['AdminLogin']['new_password'];
        $confirm_password = $data['AdminLogin']['confirm_password'];

        if ($old_password == $current_password) {
            if ($new_password == $confirm_password) {
                $data['AdminLogin']['password'] = $new_password;
                $this->AdminLogin->id = $data['AdminLogin']['id'];
                if ($this->AdminLogin->save($data)) {
                    $this->redirect('/AdminLogins/logout');
                } else {
                    $this->Flash->error(__('Something went wrong !'));
                    $this->redirect('change_password');
                }
            } else {
                $this->Flash->error(__('Password & Confirm Password did not match'));
                $this->redirect('change_password');
            }
        } else {
            $this->Flash->error(__('Your current password is wrong !'));
            $this->redirect('change_password');
        }
        echo '<pre>';
        print_r($user_details);
        print_r($data);
        exit;
    }

    public function manage_profile() {
        $this->layout = "admin";
        $this->loadModel('AdminLogin');
        //$this->loadModel('SupervisiorAssignment');
        $login_detail = $this->Auth->User();
        $id = $login_detail['id'];
        $user_details = $this->AdminLogin->find('first', array('conditions' => array('AdminLogin.id' => $id)));
        $user_detail = $user_details['AdminLogin'];
       // $buildings = $this->SupervisiorAssignment->find('all', array('fields' => array('SupervisiorAssignment.building_id', 'Building.name'), 'conditions' => array('SupervisiorAssignment.supervisior_id' => $id)));
        // echo '<pre>';
        // print_r($details); exit;
        $this->set(compact('user_detail', 'buildings'));
    }

    public function update_profile() {
        $this->loadModel('AdminLogin');
         $this->AdminLogin->validate = $this->AdminLogin->validateWithoutPassword;
        $data = $this->request->data;

        $this->AdminLogin->id = $data['AdminLogin']['id'];
        if ($this->AdminLogin->save($data)) {
            $this->Session->write('Auth.User.name', $data['AdminLogin']['name']);
            $this->Flash->success(__('Updated successfully.'));
            $this->redirect('manage_profile');
        } else {
            $this->Flash->error(__('Something went wrong !'.$this->Common->validationErrors($this->AdminLogin->validationErrors)));
            $this->redirect('manage_profile');
        }
    }

}
