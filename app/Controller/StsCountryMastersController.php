<?php

/**
 * Name             :   Sreerag A S
 * Date Created     :   4-6-2018
 * Last Modified    :   4-6-2018
 * 
 */
App::uses('AppController', 'Controller');

/**
 * StsCountryMasters Controller
 *
 * @property StsCountryMaster $StsCountryMaster
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class StsCountryMastersController extends AppController {

    public $components = array('Paginator', 'Session', 'Flash', 'Common');
    public $user_id;

    public function beforeFilter() {
        
        //Permission Checking 
        
        $super_admin_type = Configure::read('Admin_Type_Admin');
        $user = $this->Auth->user();
        $this->user_id = $user['id'];

        if ( $user['admin_type_id'] != $super_admin_type) {
            $this->Flash->error(__('No permission to access !'));
            $this->redirect('../concierge_settings/index');
        }
    }

    public function index() {

        // echo Configure::read('Concierge_Org_Id'); exit;
        $this->layout = "concierge_back_office";

        // for search option
        $this->StsCountryMaster->recursive = 0;
        $filter_conditions = array();
        if ($this->request->is('post')) {
            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            $filters = array();
            //Setting model to url
            if (isset($this->request->data['StsCountryMaster']['cm_country_id']) && !empty($this->request->data['StsCountryMaster']['cm_country_id'])) {
                $filters['cm_country_id'] = $this->request->data['StsCountryMaster']['cm_country_id'];
                $this->set('cm_country_id', $filters['cm_country_id']);
            }

            //setting redirection page for date search
            $this->redirect(array_merge($filter_url, $filters));
        }

        if (isset($this->passedArgs["cm_country_id"])) {
            array_push($filter_conditions, array('StsCountryMaster.cm_country_id' => $this->passedArgs["cm_country_id"]));
            $this->set('cm_country_id', $this->passedArgs["cm_country_id"]);
        }

        $this->paginate = array(
            'limit' => 10,
            'conditions' => $filter_conditions,
            'order' => array('StsCountryMaster.cm_country_name' => 'ASC')
        );

        $country_list = $this->StsCountryMaster->find('all', array(
            'fields' => array(
                'StsCountryMaster.cm_country_id',
                'StsCountryMaster.cm_country_name',
            ),
        ));
        $country_list_opt = array();
        foreach ($country_list as $country) {
            $country_list_opt[$country['StsCountryMaster']['cm_country_id']] = $country['StsCountryMaster']['cm_country_name'];
        }

       


        $country_master = $this->paginate('StsCountryMaster');
        $this->set(compact('country_master',  'country_list_opt'));
    }

    public function add_country() {

        //$data = array_map('trim', $this->request->data['AdminType']);
        //$data = array_map('trim', $this->request->data);
        //pr($this->request->data); exit;
        $today = date("Y-m-d H:s");
        $this->StsCountryMaster->create();
        
        //pr($this->request->data); exit;
        $this->StsCountryMaster->set($this->request->data);

        try {
            if ($this->StsCountryMaster->save($this->request->data)) {
                $this->Flash->success(__('Country Added.'));
            } else {

                $this->Flash->error(__('Something went wrong !' . $this->Common->validationErrors($this->StsCountryMaster->validationErrors)));
            }
        } catch (Exception $e) {
            $this->Flash->error(__('Already exists !'));
        }
        $this->redirect('index');
    }

    public function delete_country($id) {
        $this->StsCountryMaster->delete($id);

        //$this->StsCountryMaster->query("delete from admin_types where id='" . $id . "'");
        $this->Flash->success(__('The Country has been deleted.'));

        return $this->redirect(array('action' => 'index'));
    }

    public function deleteall_contries() {
        echo 'Not Allowed';
        exit;
        if (!empty($this->data)) {
            if (isset($this->data) && !empty($this->data['id'])) {
                $selectedReferences = $this->data['id'];
                foreach ($selectedReferences as $singleReference) {
                    $this->AdminType->query("delete from admin_types where id='" . $singleReference . "'");
                }
                $this->Session->setFlash(
                        __('Your selected records has been deleted')
                );
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(
                        __('Please select atleast one record !')
                );
                return $this->redirect(array('action' => 'index'));
            }
        } else {
            $this->Session->setFlash(
                    __('Please select atleast one record !')
            );
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function editform_country($id) {
        if ($this->request->is('ajax')) {
            $data = $this->StsCountryMaster->find('first', array('conditions' => array('cm_country_id' => $id)));
            $this->response->body(json_encode($data));
            //Return reponse object to prevent controller from trying to render a view
            return $this->response;
        }
    }

    public function edit_country() {

        // $data = array_map('trim', $this->request->data['StsCountryMaster']);
        if ($this->request->is(array('post', 'put'))) {

           

            //$name = $data['name'];
            try {
                // $this->StsCountryMaster->query("update admin_types set name='" . $name . "' where id='" . $id . "'");
                $this->StsCountryMaster->save($this->request->data);
                $this->Flash->success(__('Updated Successfully'));
            } catch (Exception $e) {
                $this->Flash->error(__('Already exists !'));
            }

            $this->redirect(array('action' => 'index'));
        }
    }

    public function listCountries() {
        $countries = $this->StsCountryMaster->find('all', array(
            'fields' => array(
                'StsCountryMaster.cm_country_id',
                'StsCountryMaster.cm_country_code',
                'StsCountryMaster.cm_country_name'
            ),
            'conditions' => array(
                'StsCountryMaster.cm_status' => 'A',
            ),
            'order' => array(
                'StsCountryMaster.cm_country_name' => 'ASC'
            ),
            'recursive' => -1
        ));
        $response = array();
        if (sizeof($countries)) {
            foreach ($countries as $key => $index) {
                $response[] = $index['StsCountryMaster'];
            }
        }
        return $response;
    }

}
