<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class LoginController extends Controller {

    public $components = array(
        // 'DebugKit.Toolbar',
        'Session',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'concierge_partners', 'action' => 'dashboard'),
            'loginAction' => array('controller' => 'concierge_partners', 'action' => 'login'),
            'logoutRedirect' => array('controller' => 'concierge_partners', 'action' => 'logout'),
            'authError' => 'You must be logged in to view this page.',
            'loginError' => 'Invalid Username or Password entered, please try again.',
            'authenticate' => array(
                AuthComponent::ALL => array('userModel' => 'ConciergePartnershipRequest'),
                'Form' => array(
                    'fields' => array('username' => 'pr_email_id', 'password' => 'pr_pwd')
                )
            )
        )
//        'Auth' => array(
//            'loginRedirect' => array('controller' => 'admin_logins', 'action' => 'dashboard'),
//            'loginAction' => array('controller' => 'admin_logins', 'action' => 'index'),
//            'logoutRedirect' => array('controller' => 'admin_logins', 'action' => 'index'),
//            'authError' => 'You must be logged in to view this page.',
//            'loginError' => 'Invalid Username or Password entered, please try again.',
//            'authenticate' => array('Form' => array('fields' => array('username' => 'email', 'password' => 'password'))
//            )
//        )
    );

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Cookie');
    }

// only allow the login controllers only
    public function beforeFilter() {
//        $this->Auth->authorize = 'Controller';
//        $this->Auth->allow('index', 'login');
    }

}
