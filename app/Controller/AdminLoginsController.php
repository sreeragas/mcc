<?php

/* Create By Ajay Singh
 *
 */
App::uses('AppController', 'Controller');

class AdminLoginsController extends AppController {

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $this->signup_requests = WWW_ROOT . Configure::read('Request_Log_Path') . 'User_signup/';
    }

    public $components = array('Paginator', 'Session', 'Flash', 'Export.Export');

    public function beforeFilter() {
        ob_start();
        parent::beforeFilter();
        //pr($this->Auth); exit;
        $this->Auth->allow(
                'login', 'index'
        );

        $this->loadModel('AdminLogins');
    }

    /**
     * signup method
     *
     * @return void
     */

    /**
     * login method
     *
     * @throws NotFoundException
     * @param string $username, $pasword,
     * @return void
     */
    public function login() {

        $this->redirect(array('controller' => 'adminlogins', 'action' => 'index'));
    }

    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    public function index() {
        $this->layout = "login";

        if (!empty($this->data)) {
            $result = $this->AdminLogin->find('first', array(
                'conditions' => array(
                    'AdminLogin.email' => $this->data['AdminLogin']['email'],
                    'AdminLogin.status' => 1
                )
            ));

            if (!empty($result)) {

                $hashedPassword = $this->Auth->password($this->data['AdminLogin']['password']);
                if ($hashedPassword == $result['AdminLogin']['password'] && !empty($result['AdminLogin']['email'])) {
                    //$this->Auth->login($result['AdminLogin']['email']);
                    $this->Auth->login($result['AdminLogin']);
                    $this->redirect(array('action' => 'dashboard'));
                } else {
                    $this->Session->setFlash(__('Invalid Email or password'));
                }
            } else {
                $this->Session->setFlash(__('The User account could not be found'));
            }
        }
    }

    public function dashboard() {
        /* This section is for Admin Dashboard */
        $this->Session->write('Page.refferideliver', 'dashboard');
        $this->Session->write('Page.refferidelivercon', 'AdminLogins');
        $this->loadModel('AdminLogin');
        $member_full_list = $this->AdminLogin->find('all', array(
            'conditions' => array('AdminLogin.id <>' => 1),
            //'order' => array('AdminLogin.log_id' => 'DESC'),
            'recursive' => 1));

        $this->set('member_full_list', $member_full_list);
        $login_id = $_SESSION['Auth']['User']['id'];
        /* Ends Here */
        $member_single_detail = [];
        if (AuthComponent::user('admin_type_id') == Configure::read('Admin_Type_Staff')) {

            $member_single_detail = $this->AdminLogin->find('all', array(
                'conditions' => array('AdminLogin.id' => $login_id),
                'recursive' => 1));
            $member_single_detail = $member_single_detail[0];
        }

        /* This section is for Supervisior Dashboard */


        $this->set('member_single_detail', $member_single_detail);
        $this->set('current_user_id', $login_id);

        /* Supervisior Dashboard Ends Here */
        $this->layout = "admin";
    }

   
}
