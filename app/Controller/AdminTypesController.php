<?php

App::uses('AppController', 'Controller');

class AdminTypesController extends AppController {

    public $components = array('Paginator', 'Session', 'Flash');

    public function beforeFilter() {
        $this->loadModel('AdminType');
        $this->loadModel('Screen');
        $this->loadModel('ScreenAssignment');
    }

    public function index() {
        $this->layout = "admin";
        $this->paginate = array(
            'limit' => 10,
            'order' => array('AdminType.name' => 'ASC')
        );
        $admin_types = $this->paginate('AdminType');
        $this->set(compact('admin_types'));
    }

    public function add_role() {
        $data = array_map('trim', $this->request->data['AdminType']);
        $this->AdminType->set($data);
        $this->AdminType->create();
        try {
            if ($this->AdminType->save($data)) {
                $this->Flash->success(__('Role Added.'));
            } else {
                $this->Flash->error(__('Something went wrong !'));
            }
        } catch (Exception $e) {
            $this->Flash->error(__('Already exists !'));
        }
        $this->redirect('index');
    }

    public function delete_role($id) {
        $this->AdminType->id = $id;
        if ($id == C_ADMIN_TYPE_ADMIN || $id == C_ADMIN_TYPE_STAFF) { // Admin or Staff User
            $this->Flash->error(__('You Cannot Delete Admin Role !'));
            $this->redirect('index');
        }
        $this->AdminType->query("delete from admin_types where id='" . $id . "'");
        $this->Flash->success(__('The role has been deleted.'));

        return $this->redirect(array('action' => 'index'));
    }

    public function deleteall_role() {
        
         $this->Session->setFlash(
                __('Not allow to delete !')
        );
        return $this->redirect(array('action' => 'index'));

        exit;
        if (!empty($this->data)) {
            if (isset($this->data) && !empty($this->data['id'])) {
                $selectedReferences = $this->data['id'];
                foreach ($selectedReferences as $singleReference) {
                    $this->AdminType->query("delete from admin_types where id='" . $singleReference . "'");
                }
                $this->Session->setFlash(
                        __('Your selected records has been deleted')
                );
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(
                        __('Please select atleast one record !')
                );
                return $this->redirect(array('action' => 'index'));
            }
        } else {
            $this->Session->setFlash(
                    __('Please select atleast one record !')
            );
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function editform_role($id) {
        if ($this->request->is('ajax')) {
            $data = $this->AdminType->find('first', array('conditions' => array('id' => $id)));
            $this->response->body(json_encode($data));
            //Return reponse object to prevent controller from trying to render a view
            return $this->response;
        }
    }

    public function edit_role() {
        $data = array_map('trim', $this->request->data['AdminType']);

        $id = $data['id'];
        if ($id == 1 ) { // Admin 
            $this->Flash->error(__('You Cannot Edit Admin Role !'));
            
            return $this->redirect(array('action' => 'index'));

            exit;
        }

        $id = $data['id'];
        if ($this->request->is(array('post', 'put'))) {
            $name = $data['name'];
            try {
                $this->AdminType->query("update admin_types set name='" . $name . "' where id='" . $id . "'");
                $this->Flash->success(__('Updated Successfully'));
            } catch (Exception $e) {
                $this->Flash->error(__('Already exists !'));
            }

            $this->redirect(array('action' => 'index'));
        }
    }

    public function screen() {
        $this->layout = 'admin';
        $conditions = array();

        if ($this->request->is('post')) {
            $filter_url['controller'] = $this->request->params['controller'];
            $filter_url['action'] = $this->request->params['action'];
            $filters = array();

            //Setting user to url
            if (isset($this->request->data['Screen']['group']) && !empty($this->request->data['Screen']['group'])) {
                $filters['group'] = $this->request->data['Screen']['group'];
                $this->set('group', $filters['group']);
            }
            //setting redirection page for date search
            $this->redirect(array_merge($filter_url, $filters));
        }

        //checking if start date and end date exists in url or not for setting pagination against them
        if (isset($this->passedArgs["group"])) {
            array_push($conditions, array('Screen.group' => $this->passedArgs["group"]));
            $this->set('group', $this->passedArgs["group"]);
        }
        $this->paginate = array(
            'conditions' => $conditions,
            'limit' => 10,
            'order' => 'Screen.group'
        );
        $screen_list = $this->paginate('Screen');
        $this->set(compact('screen_list'));
    }

    public function role_screen() {
        $this->layout = 'admin';
        $screens = $this->Screen->find('all', array('group_concat' => 'group'));
        //pr($screens);
        $usage = array();
        $i = 0;
        foreach ($screens as $dc) {
            $usage[$dc['Screen']['group']][] = $dc;
        }
        $screen_arr = $this->Screen->find('list');
        //pr($screen_arr);
        $role_arr = $this->AdminType->find('list', array('fields' => array('id', 'name')));
        $this->set(compact('usage', 'role_arr'));
    }

    public function add_role_screen() {
        $data = $this->request->data;
        //pr($data);
        //exit;
        $screen_id = implode(',', $data['screen_id']);
        $data_arr['ScreenAssignment']['admin_type_id'] = $data['AdminTypes']['admin_type'];
        $data_arr['ScreenAssignment']['screen_id'] = $screen_id;
        $this->ScreenAssignment->admin_type_id = $data['AdminTypes']['admin_type'];
        $this->ScreenAssignment->set($data_arr);
        if ($this->ScreenAssignment->save($data_arr)) {
            $this->Session->setFlash(
                    __('Updated Successfully.')
            );
        } else {
            $this->Session->setFlash(
                    __('Something went wrong')
            );
        }
        $this->redirect('role_screen');
    }

    public function permissions() {
        $this->layout = "admin";
        $list_arr = $this->ScreenAssignment->find('all');
        $screen_arr = $this->Screen->find('list');
        $this->set(compact('list_arr', 'screen_arr'));
    }

    public function edit_role_screen($id) {
        $this->layout = 'admin';
        $admin_type_details = $this->ScreenAssignment->find('first', array('conditions' => array('ScreenAssignment.admin_type_id' => $id)));
        $screens = $this->Screen->find('all', array('group_concat' => 'group'));
        $usage = array();
        $i = 0;
        foreach ($screens as $dc) {
            $usage[$dc['Screen']['group']][] = $dc;
        }
        $screen_arr = $this->Screen->find('list');

        $this->set(compact('admin_type_details', 'usage'));
        $this->render('edit_role_screen');
    }

    public function check_role($id) {
        if ($this->request->is('ajax')) {
            $data = $this->ScreenAssignment->find('first', array('conditions' => array('admin_type_id' => $id)));
            $this->response->body(json_encode($data));
            //Return reponse object to prevent controller from trying to render a view
            return $this->response;
        }
    }

}
