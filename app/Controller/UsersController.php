<?php

/* Create By Ajay Singh
 *
 */
App::uses('AppController', 'Controller');

class UsersController extends AppController {

    var $uses = array('User', 'SocialProfile');
    public $components = array('Common', 'Hybridauth');

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $this->signup_requests = WWW_ROOT . Configure::read('Request_Log_Path') . 'User_signup/';
    }

    public $paginate = array(
        'limit' => 25,
        'conditions' => array('status' => '1'),
        'order' => array('User.email' => 'asc')
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('web_login', 'login', 'add', 'signup', 'generateUserSessionKey', 'createSessionLog', 'getAtk', 'getProfile', 'updateProfile', 'changePassword', 'forgotPassword', 'social_login', 'hybrid_login', 'hybrid_endpoint');
    }

    //-----hybrid auth --------------------
    public function hybrid_login($provider) {

        if ($this->Hybridauth->connect($provider)) {

            $this->_successfulHybridauth($provider, $this->Hybridauth->user_profile);
        } else {
            // error
            $this->Session->setFlash($this->Hybridauth->error);
            $this->redirect($this->Auth->loginAction);
        }
    }

    public function hybrid_endpoint($provider) {
        $this->Hybridauth->processEndpoint();
    }

    private function _successfulHybridauth($provider, $incomingProfile) {

        // #1 - check if user already authenticated using this provider before
        $this->SocialProfile->recursive = -1;
        $existingProfile = $this->SocialProfile->find('first', array(
            'conditions' => array('social_network_id' => $incomingProfile['SocialProfile']['social_network_id'], 'social_network_name' => $provider)
        ));

        if ($existingProfile) {
            // #2 - if an existing profile is available, then we set the user as connected and log them in
            $user = $this->User->find('first', array(
                'conditions' => array('id' => $existingProfile['SocialProfile']['user_id'])
            ));

            $this->_doSocialLogin($user, true);
        } else {

            // New profile.
            if ($this->Auth->loggedIn()) {
                // user is already logged-in , attach profile to logged in user.
                // create social profile linked to current user
                $incomingProfile['SocialProfile']['user_id'] = $this->Auth->user('id');
                $this->SocialProfile->save($incomingProfile);

                $this->Session->setFlash('Your ' . $incomingProfile['SocialProfile']['social_network_name'] . ' account is now linked to your account.');
                $this->redirect($this->Auth->redirectUrl());
            } else {
                // no-one logged and no profile, must be a registration.
                $user = $this->User->createFromSocialProfile($incomingProfile);
                $incomingProfile['SocialProfile']['user_id'] = $user['User']['id'];
                $this->SocialProfile->save($incomingProfile);

                // log in with the newly created user
                $this->_doSocialLogin($user);
            }
        }
    }

    private function _doSocialLogin($user, $returning = false) {

        if ($this->Auth->login($user['User'])) {
            if ($returning) {
                $this->Session->setFlash(__('Welcome back, ' . $this->Auth->user('username')));
            } else {
                $this->Session->setFlash(__('Welcome to our community, ' . $this->Auth->user('username')));
            }
            $this->redirect($this->Auth->loginRedirect);
        } else {
            $this->Session->setFlash(__('Unknown Error could not verify the user: ' . $this->Auth->user('username')));
        }
    }

    public function createFromSocialProfile($incomingProfile) {

        // check to ensure that we are not using an email that already exists
        $existingUser = $this->find('first', array(
            'conditions' => array('email' => $incomingProfile['SocialProfile']['email'])));

        if ($existingUser) {
            // this email address is already associated to a member
            return $existingUser;
        }

        // brand new user
        $socialUser['User']['org_id'] = $this->Common->getOrganizationId();
        $socialUser['User']['email'] = $incomingProfile['SocialProfile']['email'];
        $socialUser['User']['fullname'] = $incomingProfile['SocialProfile']['display_name'];
        //$socialUser['User']['username'] = str_replace(' ', '_', $incomingProfile['SocialProfile']['display_name']);
        //$socialUser['User']['role'] = 'bishop'; // by default all social logins will have a role of bishop
        $socialUser['User']['password'] = date('Y-m-d h:i:s'); // although it technically means nothing, we still need a password for social. setting it to something random like the current time..
        $socialUser['User']['created'] = date('Y-m-d h:i:s');
        $socialUser['User']['modified'] = date('Y-m-d h:i:s');

        // save and store our ID
        $this->save($socialUser);
        $socialUser['User']['id'] = $this->id;

        return $socialUser;
    }

    //-----hybrid auth --------------------

    /**
     * signup method
     *
     * @return void
     */
    public function signup() {
        $response = array();
        $response['info'] = array(
            'status' => 0,
            'message' => ''
        );

        $HTTP_RAW_JSON_DATA = file_get_contents('php://input'); // RECEIVE INPUT JSON STRING

        $Decoded_Data = json_decode($HTTP_RAW_JSON_DATA, true);

        $data['User']['fullname'] = (isset($Decoded_Data['Register']['fullname'])) ? $Decoded_Data['Register']['fullname'] : '';
        $data['User']['email'] = (isset($Decoded_Data['Register']['email'])) ? $Decoded_Data['Register']['email'] : '';
        if (isset($Decoded_Data['Register']['app_version']))
            $data['User']['password'] = (isset($Decoded_Data['Register']['password'])) ? base64_decode($Decoded_Data['Register']['password']) : '';
        else
            $data['User']['password'] = (isset($Decoded_Data['Register']['password'])) ? $Decoded_Data['Register']['password'] : '';
        $data['User']['app_version'] = (isset($Decoded_Data['Register']['app_version'])) ? $Decoded_Data['Register']['app_version'] : '';

        $data['User']['country_code'] = (isset($Decoded_Data['Register']['country_code'])) ? $Decoded_Data['Register']['country_code'] : '';
        $data['User']['mobile_number'] = (isset($Decoded_Data['Register']['mobile_number'])) ? $Decoded_Data['Register']['mobile_number'] : '';
        $data['User']['building_id'] = (isset($Decoded_Data['Register']['building_id'])) ? $Decoded_Data['Register']['building_id'] : '';
        $data['User']['apartment_number'] = (isset($Decoded_Data['Register']['apartment_number'])) ? $Decoded_Data['Register']['apartment_number'] : '';
        $data['User']['community'] = (isset($Decoded_Data['Register']['community'])) ? $Decoded_Data['Register']['community'] : '';
        $data['User']['checksum'] = (isset($Decoded_Data['Register']['checksum'])) ? $Decoded_Data['Register']['checksum'] : '';

        if (is_array($data)) {
            if ($this->checksum($Decoded_Data)) {
                $this->User->set($data);
                $this->User->create();
                if ($this->User->save($data)) {
                    $user_email = $Decoded_Data['Register']['email'];
                    $user_name = $Decoded_Data['Register']['fullname'];
                    /*                     * * Sending Email to Customer** */

                    $Host = Configure::read('SMTP_HOST');
                    $Port = Configure::read('SMTP_PORT');
                    $SMTPAuth = true;
                    $SMTPSecure = Configure::read('SMTP_SECURE');
                    $Username = Configure::read('SMTP_USERNAME');
                    $Password = Configure::read('SMTP_PASSWORD');
                    $setFrom = Configure::read('SMTP_SET_FROM');
                    $setFromName = Configure::read('SMTP_SET_FROM_NAME');

                    $this->loadModel('WelcomeMailTemplate');
                    $content_details = $this->WelcomeMailTemplate->find('first', array('conditions' => array('WelcomeMailTemplate.id' => 1)));
                    $content = $content_details['WelcomeMailTemplate']['content'];
                    $content = str_replace('&amp;username&amp;', $user_email, $content);

                    App::import('Vendor', 'PHPMailer', array('file' => 'PHPMailer' . DS . 'class.phpmailer.php'));
                    /*                     * * Sending Email to Customer ** */
                    $mail = new PHPMailer();
                    $mail->isSMTP();
                    $mail->SMTPOptions = array(
                        'ssl' => array(
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                            'allow_self_signed' => true
                        )
                    );
                    $mail->SMTPDebug = 0;
                    $mail->Host = $Host;
                    $mail->Port = $Port;
                    $mail->SMTPAuth = true;
                    $mail->SMTPSecure = $SMTPSecure;
                    $mail->Username = $Username;
                    $mail->Password = $Password;
                    $mail->setFrom($setFrom, $setFromName);
                    $mail->addAddress($user_email, $user_name);
                    $mail->Subject = 'APEX-Services Confirmation';
                    $mail->MsgHTML($content);
                    $mail->Send();

                    $response['info'] = array(
                        'status' => 1,
                        'message' => 'The user has been saved.'
                    );
                } else {
                    $validationErrors = $this->User->validationErrors;

                    $errorMessage = (isset($validationErrors['email']) && sizeof($validationErrors['email']) && $validationErrors['email'][0] == 'Username already exists') ? $validationErrors['email'][0] : 'Invalid request';
                    $response['info'] = array(
                        'status' => -1,
                        'message' => $errorMessage
                    );
                }
            } else {
                $response['info'] = array(
                    'status' => -4,
                    'message' => 'Checksum validation failed'
                );
            }
        } else {
            $response['info'] = array(
                'status' => -1,
                'message' => 'Empty data received'
            );
        }

        echo json_encode($response);
        die;
    }

    /**
     * login method
     *
     * @throws NotFoundException
     * @param string $username, $pasword,
     * @return void
     */
    public function login() {

        $response = array();
        $response['info'] = array(
            'status' => 0,
            'message' => ''
        );

        $HTTP_RAW_JSON_DATA = file_get_contents('php://input'); // RECEIVE INPUT JSON STRING

        $Decoded_Data = json_decode($HTTP_RAW_JSON_DATA, true);

        App::uses('AuthComponent', 'Controller/Component');

        if ($this->checksum($Decoded_Data)) {
            $data['User']['email'] = (isset($Decoded_Data['Login']['email'])) ? $Decoded_Data['Login']['email'] : '';
            // $data['User']['password'] = (isset($Decoded_Data['Login']['password'])) ? AuthComponent::password(base64_decode($Decoded_Data['Login']['password'])) : '';
            if (isset($Decoded_Data['Login']['app_version']))
                $data['User']['password'] = (isset($Decoded_Data['Login']['password'])) ? AuthComponent::password(base64_decode($Decoded_Data['Login']['password'])) : '';
            else
                $data['User']['password'] = (isset($Decoded_Data['Login']['password'])) ? AuthComponent::password($Decoded_Data['Login']['password']) : '';

            $user = $this->User->find('first', array(
                'fields' => array(
                    'id',
                    'email',
                    'fullname',
                    'country_code',
                    'mobile_number',
                    'building_id',
                    'apartment_number',
                    'community',
                    'Building.name'
                ),
                'conditions' => array(
                    'email' => $data['User']['email'],
                    'password' => $data['User']['password'],
                ),
                    //'recursive' => -1
            ));

            if (sizeof($user)) {
                $hashKey = $this->generateUserSessionKey($user['User']);
                if ($this->createSessionLog($user['User'], $Decoded_Data['Login'], $hashKey)) {
                    $response['User'] = array(
                        'id' => $user['User']['id'],
                        'utk' => $hashKey,
                        'fullname' => $user['User']['fullname'],
                        'email' => $user['User']['email'],
                        'country_code' => $user['User']['country_code'],
                        'mobile_number' => $user['User']['mobile_number'],
                        'building_id' => $user['User']['building_id'],
                        'apartment_number' => $user['User']['apartment_number'],
                        'community' => $user['User']['community'],
                        'building_name' => $user['Building']['name'],
                    );
                    $response['info'] = array(
                        'status' => 1,
                        'message' => 'User logged in successfully'
                    );
                } else {
                    $response['info'] = array(
                        'status' => -1,
                        'message' => 'Kindly try again later'
                    );
                }
            } else {
                $response['info'] = array(
                    'status' => -1,
                    'message' => 'Username or password invalid'
                );
            }
        } else {
            $response['info'] = array(
                'status' => -4,
                'message' => 'Checksum validation failed'
            );
        }
        echo json_encode($response);
        die;
    }

    /**
     * Spcial login method
     *
     * @throws NotFoundException
     * @param string $username, $pasword,
     * @return void
     */
    public function social_login() {
        $response = array();
        $response['info'] = array(
            'status' => 0,
            'message' => ''
        );

        $HTTP_RAW_JSON_DATA = file_get_contents('php://input'); // RECEIVE INPUT JSON STRING

        $Decoded_Data = json_decode($HTTP_RAW_JSON_DATA, true);

        App::uses('AuthComponent', 'Controller/Component');

        $data['User']['email'] = (isset($Decoded_Data['SocialLogin']['email'])) ? $Decoded_Data['SocialLogin']['email'] : '';
        $data['User']['fullname'] = (isset($Decoded_Data['SocialLogin']['fullname'])) ? $Decoded_Data['SocialLogin']['fullname'] : '';
        $data['User']['device_token'] = (isset($Decoded_Data['SocialLogin']['device_token'])) ? $Decoded_Data['SocialLogin']['device_token'] : '';
        $data['User']['device_platform'] = (isset($Decoded_Data['SocialLogin']['device_platform'])) ? $Decoded_Data['SocialLogin']['device_platform'] : '';
        $data['User']['app_version'] = (isset($Decoded_Data['SocialLogin']['app_version'])) ? $Decoded_Data['SocialLogin']['app_version'] : '';
        $data['User']['social_type'] = (isset($Decoded_Data['SocialLogin']['social_type'])) ? $Decoded_Data['SocialLogin']['social_type'] : '';
        $data['User']['facebook_id'] = (isset($Decoded_Data['SocialLogin']['facebook_id'])) ? $Decoded_Data['SocialLogin']['facebook_id'] : '';
        $data['User']['google_id'] = (isset($Decoded_Data['SocialLogin']['google_id'])) ? $Decoded_Data['SocialLogin']['google_id'] : '';

        if (is_array($data)) {
            $user_details = $this->User->find('first', array('conditions' => array('User.email' => $data['User']['email'])));
            if (empty($user_details)) {
                if ($data['User']['social_type'] == 'f') {
                    $find_user = $this->User->find('first', array('conditions' => array('User.facebook_id' => $data['User']['facebook_id'])));
                }
                if ($data['User']['social_type'] == 'g') {
                    $find_user = $this->User->find('first', array('conditions' => array('User.google_id' => $data['User']['google_id'])));
                }
                if (empty($find_user)) {
                    $this->User->set($data);
                    $this->User->create();
                    if ($this->User->save($data)) {
                        $user_email = $Decoded_Data['SocialLogin']['email'];
                        $user_name = $Decoded_Data['SocialLogin']['fullname'];
                        /*                         * * Sending Email to Customer** */
                        $Host = Configure::read('SMTP_HOST');
                        $Port = Configure::read('SMTP_PORT');
                        $SMTPAuth = true;
                        $SMTPSecure = Configure::read('SMTP_SECURE');
                        $Username = Configure::read('SMTP_USERNAME');
                        $Password = Configure::read('SMTP_PASSWORD');
                        $setFrom = Configure::read('SMTP_SET_FROM');
                        $setFromName = Configure::read('SMTP_SET_FROM_NAME');

                        $this->loadModel('WelcomeMailTemplate');
                        $content_details = $this->WelcomeMailTemplate->find('first', array('conditions' => array('WelcomeMailTemplate.id' => 1)));
                        $content = $content_details['WelcomeMailTemplate']['content'];

                        App::import('Vendor', 'PHPMailer', array('file' => 'PHPMailer' . DS . 'class.phpmailer.php'));
                        /*                         * * Sending Email to Customer ** */
                        $mail = new PHPMailer();
                        $mail->isSMTP();
                        $mail->SMTPOptions = array(
                            'ssl' => array(
                                'verify_peer' => false,
                                'verify_peer_name' => false,
                                'allow_self_signed' => true
                            )
                        );
                        $mail->SMTPDebug = 0;
                        $mail->Host = $Host;
                        $mail->Port = $Port;
                        $mail->SMTPAuth = true;
                        $mail->SMTPSecure = $SMTPSecure;
                        $mail->Username = $Username;
                        $mail->Password = $Password;
                        $mail->setFrom($setFrom, $setFromName);
                        $mail->addAddress($user_email, $user_name);
                        $mail->Subject = 'APEX-Services Confirmation';
                        $mail->MsgHTML($content);
                        $mail->Send();


                        $user_id = $this->User->getLastInsertID();
                        $user = $this->User->find('first', array(
                            'fields' => array(
                                'id',
                                'email',
                                'fullname',
                                'country_code',
                                'mobile_number',
                                'building_id',
                                'apartment_number',
                                'community',
                                'facebook_id',
                                'google_id',
                                'Building.name'
                            ),
                            'conditions' => array(
                                'User.id' => $user_id,
                            ),
                        ));
                        $hashKey = $this->generateUserSessionKey($user['User']);
                        if ($this->createSessionLog($user['User'], $Decoded_Data['SocialLogin'], $hashKey)) {
                            $response['User'] = array(
                                'id' => $user['User']['id'],
                                'utk' => $hashKey,
                                'fullname' => $user['User']['fullname'],
                                'email' => $user['User']['email'],
                                'country_code' => $user['User']['country_code'],
                                'mobile_number' => $user['User']['mobile_number'],
                                'building_id' => $user['User']['building_id'],
                                'apartment_number' => $user['User']['apartment_number'],
                                'community' => $user['User']['community'],
                                'building_name' => $user['Building']['name'],
                            );
                            $response['User'] = array(
                                'utk' => $hashKey,
                                'fullname' => $user['User']['fullname'],
                            );
                            $response['info'] = array(
                                'status' => 1,
                                'message' => 'User logged in successfully'
                            );
                        } else {
                            $response['info'] = array(
                                'status' => -1,
                                'message' => 'Kindly try again later'
                            );
                        }
                    } else {
                        $errorMessage = 'Something went wrong !';
                        $response['info'] = array(
                            'status' => -1,
                            'message' => $errorMessage
                        );
                    }
                } else {
                    $user_id = $find_user['User']['id'];
                    $user = $this->User->find('first', array(
                        'fields' => array(
                            'id',
                            'email',
                            'fullname',
                            'country_code',
                            'mobile_number',
                            'building_id',
                            'apartment_number',
                            'community',
                            'facebook_id',
                            'google_id',
                            'Building.name'
                        ),
                        'conditions' => array(
                            'User.id' => $user_id,
                        ),
                    ));
                    $hashKey = $this->generateUserSessionKey($user['User']);
                    $response['User'] = array(
                        'id' => $user['User']['id'],
                        'utk' => $hashKey,
                        'fullname' => $user['User']['fullname'],
                        'email' => $user['User']['email'],
                        'country_code' => $user['User']['country_code'],
                        'mobile_number' => $user['User']['mobile_number'],
                        'building_id' => $user['User']['building_id'],
                        'apartment_number' => $user['User']['apartment_number'],
                        'community' => $user['User']['community'],
                        'building_name' => $user['Building']['name'],
                    );
                    $response['info'] = array(
                        'status' => 1,
                        'message' => 'User logged in successfully'
                    );
                }
            } else {
                $user_id = $user_details['User']['id'];
                $user = $this->User->find('first', array(
                    'fields' => array(
                        'id',
                        'email',
                        'fullname',
                        'country_code',
                        'mobile_number',
                        'building_id',
                        'apartment_number',
                        'community',
                        'facebook_id',
                        'google_id',
                        'Building.name'
                    ),
                    'conditions' => array(
                        'User.id' => $user_id,
                    ),
                ));
                $hashKey = $this->generateUserSessionKey($user['User']);
                $response['User'] = array(
                    'id' => $user['User']['id'],
                    'utk' => $hashKey,
                    'fullname' => $user['User']['fullname'],
                    'email' => $user['User']['email'],
                    'country_code' => $user['User']['country_code'],
                    'mobile_number' => $user['User']['mobile_number'],
                    'building_id' => $user['User']['building_id'],
                    'apartment_number' => $user['User']['apartment_number'],
                    'community' => $user['User']['community'],
                    'building_name' => $user['Building']['name'],
                );
                $response['info'] = array(
                    'status' => 1,
                    'message' => 'User logged in successfully'
                );
            }
        } else {
            $response['info'] = array(
                'status' => -1,
                'message' => 'Empty data received'
            );
        }

        echo json_encode($response);
        die;
    }

    /**
     * generate user session key
     *
     * @param array(),
     * @return void
     */
    private function generateUserSessionKey($sessionArray) {
        App::uses('Security', 'Utility');
        $string = $sessionArray['email'] . $sessionArray['id'];
        $hashKey = Security::hash($string);
        return $hashKey;
    }

    /**
     * create session log method
     *
     * @param array(),
     * @return void
     */
    private function createSessionLog($current_login_session = array(), $Decoded_Data = array(), $hashKey) {

        if (is_array($current_login_session)) {
            $sessionLogPath = WWW_ROOT . Configure::read('Session_Log_Path');


            $logFile = $hashKey . '.log';

            $sessionLogFile = $sessionLogPath . $logFile;
            if (!file_exists($sessionLogPath . $logFile)) {
                $fp = fopen($sessionLogPath . $logFile, "w");
            } else {
                $fp = fopen($sessionLogPath . $logFile, "r+");
            }

            $saved_sessions = trim(file_get_contents($sessionLogPath . $logFile));

            if (isset($saved_sessions) && !empty($saved_sessions)) {
                $saved_sessions_array = (array) json_decode($saved_sessions, true);
                $saved_app_devices = $saved_sessions_array['Active_Sessions']['app_devices'];

                $saved_sessions_array['Active_Sessions']['app_devices'][$Decoded_Data['atk']] = array(
                    'is_active' => 1,
                    'login_time' => date('d-m-Y h:i:s'),
                );
            } else {
                $current_login_session['Active_Sessions']['app_devices'][$Decoded_Data['atk']] = array(
                    'is_active' => 1,
                    'login_time' => date('d-m-Y h:i:s'),
                );
                $saved_sessions_array = $current_login_session;
            }

            $saved_sessions_json = json_encode($saved_sessions_array) . "\n";

            if (fwrite($fp, $saved_sessions_json)) {
                fclose($fp);
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * generate ATK key
     *
     * @param array(),
     * @return void
     */
    public function getAtk() {

        $response = array();
        $response['info'] = array(
            'status' => 0,
            'message' => ''
        );

        $HTTP_RAW_JSON_DATA = file_get_contents('php://input'); // RECEIVE INPUT JSON STRING

        $Decoded_Data = json_decode($HTTP_RAW_JSON_DATA, true);

        if ($this->checksum($Decoded_Data)) {
            $receivedString = $Decoded_Data['ATK']['appid'] . $Decoded_Data['ATK']['ts'] . $Decoded_Data['ATK']['checksum'];
            $shuffledString = str_shuffle($receivedString);

            $response['atk'] = array(
                'id' => md5($shuffledString),
            );
            $response['info'] = array(
                'status' => 1,
                'message' => ''
            );
        } else {
            $response['info'] = array(
                'status' => -1,
                'message' => 'Checksum validation failed'
            );
        }
        echo json_encode($response);
        die;
    }

    /**
     * get profile method
     *
     * @return void
     */
    public function getProfile() {

        $response = array();
        $response['info'] = array(
            'status' => 0,
            'message' => ''
        );

        $HTTP_RAW_JSON_DATA = file_get_contents('php://input'); // RECEIVE INPUT JSON STRING

        $Decoded_Data = json_decode($HTTP_RAW_JSON_DATA, true);

        if ($this->checksum($Decoded_Data)) {
            $utkId = $Decoded_Data['Profile']['utk'];
            $atkId = $Decoded_Data['Profile']['atk'];

            $session_validate = $this->validateUserSession($utkId, $atkId);

            if ($session_validate['is_verified']) {
                $user = $this->User->find('first', array(
                    'fields' => array(
                        'id',
                        'email',
                        'fullname',
                        'country_code',
                        'mobile_number',
                        'building_id',
                        'apartment_number',
                        'community',
                    ),
                    'conditions' => array(
                        'id' => $session_validate['id']
                    ),
                    'recursive' => -1
                ));
                if (is_array($user) && sizeof($user)) {
                    $response['User'] = $user['User'];
                    $response['info'] = array(
                        'status' => 1,
                        'message' => ''
                    );
                } else {
                    $response['info'] = array(
                        'status' => -1,
                        'message' => 'User not found'
                    );
                }
            } else {
                $response['info'] = array(
                    'status' => $session_validate['status'],
                    'message' => $session_validate['message']
                );
            }
        } else {
            $response['info'] = array(
                'status' => -4,
                'message' => 'Checksum validation failed'
            );
        }
        echo json_encode($response);
        die;
    }

    /**
     * update profile method
     *
     * @return void
     */
    public function updateProfile() {

        $response = array();
        $response['info'] = array(
            'status' => 0,
            'message' => ''
        );

        $HTTP_RAW_JSON_DATA = file_get_contents('php://input'); // RECEIVE INPUT JSON STRING

        $Decoded_Data = json_decode($HTTP_RAW_JSON_DATA, true);

        if ($this->checksum($Decoded_Data)) {
            $utkId = $Decoded_Data['UpdateProfile']['utk'];
            $atkId = $Decoded_Data['UpdateProfile']['atk'];

            $session_validate = $this->validateUserSession($utkId, $atkId);

            if ($session_validate['is_verified']) {
                $data['User']['id'] = $session_validate['id'];
                $data['User']['fullname'] = (isset($Decoded_Data['UpdateProfile']['fullname'])) ? $Decoded_Data['UpdateProfile']['fullname'] : '';
                $data['User']['country_code'] = (isset($Decoded_Data['UpdateProfile']['country_code'])) ? $Decoded_Data['UpdateProfile']['country_code'] : '';
                $data['User']['mobile_number'] = (isset($Decoded_Data['UpdateProfile']['mobile_number'])) ? $Decoded_Data['UpdateProfile']['mobile_number'] : '';
                $data['User']['building_id'] = (isset($Decoded_Data['UpdateProfile']['building_id'])) ? $Decoded_Data['UpdateProfile']['building_id'] : '';
                $data['User']['apartment_number'] = (isset($Decoded_Data['UpdateProfile']['apartment_number'])) ? $Decoded_Data['UpdateProfile']['apartment_number'] : '';
                $data['User']['community'] = (isset($Decoded_Data['UpdateProfile']['community'])) ? $Decoded_Data['UpdateProfile']['community'] : '';

                if (is_array($data)) {
                    $this->User->set($data);
                    $this->User->create();
                    if ($this->User->save($data)) {
                        $response['info'] = array(
                            'status' => 1,
                            'message' => 'The user has been updated successfully.'
                        );
                    } else {
                        $validationErrors = $this->User->validationErrors;

                        $errorMessage = (is_array($validationErrors['email']) && sizeof($validationErrors['email']) && $validationErrors['email'][0] == 'Username already exists') ? $validationErrors['email'][0] : 'Invalid request';
                        $response['info'] = array(
                            'status' => -1,
                            'message' => $errorMessage
                        );
                    }
                } else {
                    $response['info'] = array(
                        'status' => -1,
                        'message' => 'Empty data received'
                    );
                }
            } else {
                $response['info'] = array(
                    'status' => $session_validate['status'],
                    'message' => $session_validate['message']
                );
            }
        } else {
            $response['info'] = array(
                'status' => -4,
                'message' => 'Checksum validation failed'
            );
        }
        echo json_encode($response);
        die;
    }

    public function web_login() {

        $this->layout = "login";
        //print_r($this->request->data);
        //if already logged-in, redirect
        if ($this->Session->check('Auth.User')) {
            $this->redirect(array('action' => 'index'));
        }

        // if we get the post information, try to authenticate
        if ($this->request->is('post')) {

            if ($this->Auth->login()) {
                //$id = $this->Auth->user('id');

                $this->Session->setFlash(__('Welcome, ' . $this->Auth->user('email')));
                $this->redirect(array('action' => 'dashboard'));
            } else {
                $this->Session->setFlash(__('Invalid Email or password'));
            }
        }
    }

    /**
     * change password method
     *
     * @return void
     */
    public function changePassword() {

        $response = array();
        $response['info'] = array(
            'status' => 0,
            'message' => ''
        );

        $HTTP_RAW_JSON_DATA = file_get_contents('php://input'); // RECEIVE INPUT JSON STRING

        $Decoded_Data = json_decode($HTTP_RAW_JSON_DATA, true);

        if ($this->checksum($Decoded_Data)) {
            $utkId = $Decoded_Data['ChangePassword']['utk'];
            $atkId = $Decoded_Data['ChangePassword']['atk'];

            $session_validate = $this->validateUserSession($utkId, $atkId);

            if ($session_validate['is_verified']) {
                if (isset($Decoded_Data['ChangePassword']['app_version'])) {
                    $current_password = (isset($Decoded_Data['ChangePassword']['current_password'])) ? $this->decrypt(base64_decode($Decoded_Data['ChangePassword']['current_password'])) : '';
                    $new_password = (isset($Decoded_Data['ChangePassword']['new_password'])) ? $this->decrypt(base64_decode($Decoded_Data['ChangePassword']['new_password'])) : '';
                    $reconfirm_password = (isset($Decoded_Data['ChangePassword']['reconfirm_password'])) ? $this->decrypt(base64_decode($Decoded_Data['ChangePassword']['reconfirm_password'])) : '';
                } else {
                    $current_password = (isset($Decoded_Data['ChangePassword']['current_password'])) ? $this->decrypt($Decoded_Data['ChangePassword']['current_password']) : '';
                    $new_password = (isset($Decoded_Data['ChangePassword']['new_password'])) ? $this->decrypt($Decoded_Data['ChangePassword']['new_password']) : '';
                    $reconfirm_password = (isset($Decoded_Data['ChangePassword']['reconfirm_password'])) ? $this->decrypt($Decoded_Data['ChangePassword']['reconfirm_password']) : '';
                }

                if (isset($current_password) && isset($new_password) && isset($reconfirm_password)) {

                    App::uses('AuthComponent', 'Controller/Component');

                    $data['User']['password'] = AuthComponent::password($current_password);
                    $data['User']['id'] = $session_validate['id'];

                    $existinguser = $this->User->find('first', array(
                        'fields' => array(
                            'User.id'
                        ),
                        'conditions' => array(
                            'User.id' => $data['User']['id'],
                            'User.password' => $data['User']['password'],
                        )
                    ));

                    if (sizeof($existinguser)) {
                        if ($new_password == $reconfirm_password) {
                            $data['User']['password'] = $new_password;

                            $this->User->set($data);
                            $this->User->create();
                            if ($this->User->save($data)) {
                                $this->updateLogForInactive($utkId, $atkId);
                                $response['info'] = array(
                                    'status' => 1,
                                    'message' => 'Password updated successfully.'
                                );
                            } else {
                                $validationErrors = $this->User->validationErrors;

                                $errorMessage = (is_array($validationErrors['email']) && sizeof($validationErrors['email']) && $validationErrors['email'][0] == 'Username already exists') ? $validationErrors['email'][0] : 'Invalid request';
                                $response['info'] = array(
                                    'status' => -1,
                                    'message' => $errorMessage
                                );
                            }
                        } else {
                            $response['info'] = array(
                                'status' => -1,
                                'message' => 'Password and reconfirm password mismatch'
                            );
                        }
                    } else {
                        $response['info'] = array(
                            'status' => -1,
                            'message' => 'Invalid current password'
                        );
                    }
                }
            } else {
                $response['info'] = array(
                    'status' => $session_validate['status'],
                    'message' => $session_validate['message']
                );
            }
        } else {
            $response['info'] = array(
                'status' => -4,
                'message' => 'Checksum validation failed'
            );
        }
        echo json_encode($response);
        die;
    }

    /**
     * change password method
     *
     * @return void
     */
    public function forgotPassword() {

        $response = array();
        $response['info'] = array(
            'status' => 0,
            'message' => ''
        );

        $HTTP_RAW_JSON_DATA = file_get_contents('php://input'); // RECEIVE INPUT JSON STRING

        $Decoded_Data = json_decode($HTTP_RAW_JSON_DATA, true);

        if ($this->checksum($Decoded_Data)) {
            if ($Decoded_Data['ForgotPassword']['email']) {
                $existing_user = $this->User->find('first', array(
                    'fields' => array(
                        'User.fullname',
                        'User.id',
                    ),
                    'conditions' => array(
                        'User.email' => $Decoded_Data['ForgotPassword']['email'],
                        'User.status' => 1,
                    )
                ));
                if (sizeof($existing_user)) {

                    $chars = "abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789!@#$%";
                    $password = substr(str_shuffle($chars), 0, 8);
                    $send_pass = $password;
                    App::uses('AuthComponent', 'Controller/Component');
                    $data['User']['password'] = $password;
                    $data['User']['id'] = $existing_user['User']['id'];
                    $this->User->set($data);
                    $this->User->create();
                    if ($this->User->save($data)) {
                        $Host = Configure::read('SMTP_HOST');
                        $Port = Configure::read('SMTP_PORT');
                        $SMTPAuth = true;
                        $SMTPSecure = Configure::read('SMTP_SECURE');
                        $Username = Configure::read('SMTP_USERNAME');
                        $Password = Configure::read('SMTP_PASSWORD');
                        $setFrom = Configure::read('SMTP_SET_FROM');
                        $setFromName = Configure::read('SMTP_SET_FROM_NAME');
                        //$message = $password;
                        $message = "<table cellpadding='0' cellspacing='0' border='0' width='100%' style='background:#eeeeee;'>
                             <tr>
                                 <td style='padding:50px 0;'>
                                     <table cellpadding='0' cellspacing='0' border='0' width='600px' align='center' style='background:#ffffff;' class='table-container'>
                                       <tr>
                                              <td style='padding-top:25px;'>
                                             <table cellpadding='0' cellspacing='0' border='0' width='90%' align='center'>
                                                     <tr>
                                                         <td style='font-family:Segoe UI; font-weight:700; color:#222; font-size:16px; padding-bottom:15px;'>Hi,</td>
                                                     </tr>
                                                     <tr>
                                                         <td style='font-family:Segoe UI; font-weight:500; color:#555; font-size:15px; padding-bottom:5px;'>
                                                         We have reset your password for your APEX-Services account.<br/>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td style='font-family:Segoe UI; font-weight:500; color:#555; font-size:15px;'>
                                                         Kindly login with new password <font style='color:#00B0FF; font-weight:bold;'>" . $password . "</font>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td style='font-family:Segoe UI; font-weight:500; color:#555; font-size:13px; padding-bottom:50px; text-align:center;'>

                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td style='font-family:Segoe UI; font-weight:500; color:#777; font-size:15px; padding-bottom:25px;'>
                                                         Regards,<br/>
                                                         APEX-Services Team
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </td>
                                         </tr>
                                     </table>
                                 </td>
                             </tr>
                         </table>";

//                        App::import('Vendor', 'PHPMailer', array('file' => 'PHPMailer' . DS . 'class.phpmailer.php'));
//                        $mail = new PHPMailer();
//
//                        $mail->isSMTP();
//                        $mail->SMTPOptions = array(
//                            'ssl' => array(
//                                'verify_peer' => false,
//                                'verify_peer_name' => false,
//                                'allow_self_signed' => true
//                            )
//                        );
//                        $mail->SMTPDebug = 0;
//                        $mail->Host = $Host;
//                        $mail->Port = $Port;
//                        $mail->SMTPAuth = true;
//                        $mail->SMTPSecure = $SMTPSecure;
//                        $mail->Username = $Username;
//                        $mail->Password = $Password;
//                        $mail->setFrom($setFrom, $setFromName);
//                        $mail->addAddress($Decoded_Data['ForgotPassword']['email'], $existing_user['User']['fullname']);
//                        $mail->Subject = 'IYE-Services New Password';
//                        $mail->MsgHTML($message);
                        //$message = $this->Common->getEmailTemplate($content);

                        $subject = "Apex Services - New Password ";

                        // if ($mail->Send()) {
                        if ($this->Common->sendEmail($Decoded_Data['ForgotPassword']['email'], $existing_user['User']['fullname'], $subject, $message)) {
                            $response['info'] = array(
                                'status' => 1,
                                'message' => 'Password has been sent to your registered email account'
                            );
                        } else {
                            //echo 'Mailer Error: ' . $mail->ErrorInfo; die;
                            $response['info'] = array(
                                'status' => -1,
                                'message' => 'Problem in sending email. Try again later',
                                'password' => $send_pass
                            );
                        }
                    } else {
                        $response['info'] = array(
                            'status' => -1,
                            'message' => 'Problem in resetting password. Try again later'
                        );
                    }
                } else {
                    $response['info'] = array(
                        'status' => -1,
                        'message' => 'Email not registered with us'
                    );
                }
            } else {
                $response['info'] = array(
                    'status' => -1,
                    'message' => 'Invalid email address'
                );
            }
        } else {
            $response['info'] = array(
                'status' => -4,
                'message' => 'Checksum validation failed'
            );
        }
        echo json_encode($response);
        die;
    }

    public function logout() {
        $this->Hybridauth->logout();
        $this->redirect($this->Auth->logout());
    }

    public function index() {
        $this->layout = "admin";
        $this->paginate = array(
            'limit' => 6,
            'order' => array('User.fullname' => 'asc')
        );
        $users = $this->paginate('User');
        $this->set(compact('users'));
    }

    public function dashboard() {
        $this->layout = "admin";
        $this->paginate = array(
            'limit' => 6,
            'order' => array('User.fullname' => 'asc')
        );
        $users = $this->paginate('User');
        $this->set(compact('users'));
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been created'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be created. Please, try again.'));
            }
        }
    }

    public function edit($id = null) {

        if (!$id) {
            $this->Session->setFlash('Please provide a user id');
            $this->redirect(array('action' => 'index'));
        }

        $user = $this->User->findById($id);
        if (!$user) {
            $this->Session->setFlash('Invalid User ID Provided');
            $this->redirect(array('action' => 'index'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been updated'));
                $this->redirect(array('action' => 'edit', $id));
            } else {
                $this->Session->setFlash(__('Unable to update your user.'));
            }
        }

        if (!$this->request->data) {
            $this->request->data = $user;
        }
    }

    public function delete($id = null) {

        if (!$id) {
            $this->Session->setFlash('Please provide a user id');
            $this->redirect(array('action' => 'index'));
        }

        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Session->setFlash('Invalid user id provided');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->User->saveField('status', 0)) {
            $this->Session->setFlash(__('User deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    public function activate($id = null) {

        if (!$id) {
            $this->Session->setFlash('Please provide a user id');
            $this->redirect(array('action' => 'index'));
        }

        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Session->setFlash('Invalid user id provided');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->User->saveField('status', 1)) {
            $this->Session->setFlash(__('User re-activated'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not re-activated'));
        $this->redirect(array('action' => 'index'));
    }

    public function get_name($id) {
        $this->User->recursive = 0;
        $data = $this->User->find('all', array(
            'conditions' => array('User.id' => 1)
        ));
        //$name = $this->User->find('all', array('id' =>$id ));
        //echo "<pre>";print_r();echo "</pre>";
        return $data[0]['User']['fullname'];
    }

    public function view_user($id) {
        $this->User->recursive = 0;
        $data = $this->User->findById($id);
        $this->response->body(json_encode($data));
        //Return reponse object to prevent controller from trying to render a view
        return $this->response;
        exit();
    }

}
