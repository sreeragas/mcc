<?php

/**
 * Name             :   Sreerag A S
 * Date Created     :   19-7-2018
 * Last Modified    :   19-7-2018
 *
 */
App::uses('AppController', 'Controller');

/**
 * Settings Controller
 */
class SettingsController extends AppController {

    public $components = array('Paginator', 'Session', 'Flash', 'Common');

    public function beforeFilter() {
        $this->loadModel('Setting');

        //Permission Checking 
        $concierge_admin_type = Configure::read('Admin_Type_ConciergeAdmin');
        $super_admin_type = Configure::read('Admin_Type_Admin');
        $user = $this->Auth->user();
        $this->user_id = $user['id'];
        $this->org_id = $this->Common->getOrganizationId();

        if ($user['admin_type_id'] != $concierge_admin_type && $user['admin_type_id'] != $super_admin_type) {
            $this->Flash->error(__('No permission to access !'));
            $this->redirect('../AdminLogins/dashboard');
        }
    }

    public function index() {
        $this->layout = "admin";
        $this->loadModel('Setting');

        $settings = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1)));
        //pr($settings); exit;
        $this->set(compact('settings'));
    }

    public function edit() {
        $this->loadModel('Setting');

        // $data = array_map('trim', $this->request->data['ConciergeCurrencyMaster']);
        if ($this->request->is(array('post', 'put'))) {

            $settings_data['Setting']['id'] = 1;
            $settings_data['Setting']['concierge_video_url'] = $this->request->data['Setting']['concierge_video_url'];

            //$name = $data['name'];
            try {
                // $this->ConciergeCurrencyMaster->query("update admin_types set name='" . $name . "' where id='" . $id . "'");
                $this->Setting->save($settings_data);
                $this->Flash->success(__('Updated Successfully'));
            } catch (Exception $e) {
                $this->Flash->error(__('Already exists !'));
            }

            $this->redirect(array('action' => 'index'));
        }
    }

}
