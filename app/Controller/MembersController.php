<?php

App::uses('AppController', 'Controller');

/**

 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class MembersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'Flash', 'Common');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(
                'login', 'index', 'register'
        );
    }

    public function register() {

//        $this->layout = "concierge_back_office";
        $this->layout = "common";
        $this->loadModel('StsCountryMaster');
        $this->loadModel('AdminLogins');
        //pr($this->Auth); exit;
        $country_list_opt = $this->StsCountryMaster->find('list', array('fields' => array('cm_country_id', 'cm_country_name'), 'order' => array('StsCountryMaster.cm_country_name' => 'ASC'), 'conditions' => array('StsCountryMaster.cm_status' => 'A')));
        if ($this->request->is('post')) {
            //pr($this->request->data);
            $today = date("Y-m-d H:s");
            $fullname = $this->request->data['User']['name'];
            $email = $this->request->data['User']['email'];
            $password = $this->request->data['User']['password'];
            $pr_pwd_retype = $this->request->data['User']['password_retype'];
            $mobile_number = $this->request->data['User']['mobile_number'];
            $nationality = $this->request->data['User']['nationality'];
            $dob = $this->request->data['dob'];
            $exists = $this->AdminLogins->find('first', array(
                'conditions' => array('AdminLogins.email' => $email)
            ));
            if ($exists) {
                $this->Flash->error(__('Email Already Used.'));
                return $this->redirect(array('members' => 'resgister'));
            }

            $data_save = $this->request->data['User'];

            if ($password != $pr_pwd_retype) {
                $this->Flash->error(__('Password is not matching !'));
                $this->redirect(array('controller' => 'members', 'action' => 'register'));
            }

            $file = $this->request->data['User']['image'];


            if (isset($file['name']) && $file['name'] != "") {
                $rslt = $this->Common->fileFormatCommonImagesSizeCheck($file['size']);
                if (!$rslt['status']) {
                    $this->Flash->error(__('Image, ' . $rslt['msg']));
                    return $this->redirect(array('members' => 'resgister'));
                }
                $img_rslt = $this->Common->fileFormatCommonImages($file['type']);
                if ($img_rslt['status']) {

                    // echo WWW_ROOT. 'upload/' . $file['name'];
                    // echo '<br>';
                    $t = time();
                    $file_name = $t . '_image_' . $file['name'];
                    $path = WWW_ROOT . 'media/photo/' . $file_name;

                    move_uploaded_file($file['tmp_name'], $path);
                    //prepare the filename for database entry
                    $data_save ['image'] = $file_name;
                } else {
                    $this->Flash->error(__('Invalid file format for Logo !' . $img_rslt['msg']));
                    return $this->redirect(array('action' => 'register'));
                }
            } else {

                $this->Flash->error(__('Please select Photo !'));
                return $this->redirect(array('action' => 'register'));
            }

            //$password_enc = $this->Auth->password($password);
            // $password_retype_enc = $this->Auth->password($pr_pwd_retype);


            $data_save ['created'] = $today;
            $data_save ['password'] = $this->Auth->password($password);
            $data_save ['modified'] = $today;

            $data_save ['dob'] = $dob;
            $data_save ['admin_type_id'] = 2;
            $data_save ['status'] = 2;



//            pr($data_save);
//            exit;


            $this->AdminLogins->create();
            try {
                if ($this->AdminLogins->save($data_save)) {

                    $this->Flash->success(__('Your Registration Completed Successfully. You can login after approval.'));

                    $this->redirect(array('controller' => 'members', 'action' => 'index'));
                } else {

                    $this->Flash->error(__('Something went wrong !' . $this->Common->validationErrors($this->AdminLogins->validationErrors)));
                    $this->redirect(array('controller' => 'members', 'action' => 'register'));
                }
            } catch (Exception $e) {

                $this->Flash->error(__('Already exists !' . $e->getMessage()));
                $this->redirect(array('controller' => 'members', 'action' => 'register'));
            }

            // $this->redirect(array('controller' => 'concierge_members', 'action' => 'login'));
        }

        $this->set(compact('country_list_opt'));
    }

    public function index() {

        $this->redirect(array('controller' => 'admin_logins', 'action' => 'index'));
    }

    public function login() {

        $this->redirect(array('controller' => 'admin_logins', 'action' => 'index'));
    }

    public function update_member_status() {
        $this->loadModel('AdminLogin');
        $refer_url = $_SERVER['HTTP_REFERER'];

        if (!empty($this->data)) {
            if (isset($this->data) && !empty($this->data['active'])) {
                $status = 1;
            }
            if (isset($this->data) && !empty($this->data['inactive'])) {
                $status = 2;
            }


            //If at lesst 1 check box is selected
            if (isset($this->data) && !empty($this->data['id'])) {
                $selectedReferences = $this->data['id'];

                $flagReferenceAdded = false;
                foreach ($selectedReferences as $singleReference) {

                    $data['status'] = $status;
                    $this->AdminLogin->id = $singleReference;
                    $this->AdminLogin->set($data);
                    $this->AdminLogin->save($data);
                }
                //After delete
                $this->Session->setFlash(
                        __('Your records has been updated.')
                );
                return $this->redirect($refer_url);
            } else {
                $this->Session->setFlash(
                        __('Please select atleast one record !')
                );
                return $this->redirect($refer_url);
            }
        } else {
            $this->Session->setFlash(
                    __('Please select atleast one record !')
            );
            return $this->redirect($refer_url);
        }
    }

    public function manage() {
        // pr('sssssssssss');


        $this->layout = "admin";
        $this->loadModel('AdminLogin');
        $this->loadModel('AdminType');
        $this->loadModel('StsCountryMaster');
        $conditions = array();

        $user = $this->Auth->user();
        if (AuthComponent::user('admin_type_id') == Configure::read('Admin_Type_Admin')) {
            $user_category = 'Admin';
        } else {
            $user_category = 'Member';
            array_push($conditions, array('AdminLogin.id' => $user['id']));
        }



        $this->AdminLogin->recursive = 1;
        $this->paginate = array(
            'conditions' => $conditions,
            'limit' => 10,
            'order' => array('AdminLogin.name' => 'ASC')
        );
        $data = $this->paginate('AdminLogin');
        // echo '<pre>';
        //    print_r($data); exit;
        $this->set('members', $data);

        $admin_type_list = $this->AdminType->find('list', array('fields' => array('id', 'name')));
        $country_list = $this->StsCountryMaster->find('list', array('fields' => array('cm_country_id', 'cm_country_name'), 'conditions' => array('cm_status' => 'A')));
        $this->set('admin_type_list', $admin_type_list);
        $this->set('country_list', $country_list);



        $this->set('user_category', $user_category);

        $this->render('/Members/index');
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {

            if (AuthComponent::user('admin_type_id') == Configure::read('Admin_Type_Staff')) {
                $this->Flash->error(__('No permission to add !'));
                $this->redirect(array('controller' => 'members', 'action' => 'manage'));
            }
            $this->loadModel('AdminLogins');
            $data = $this->request->data;
            //pr($data); exit;
            $today = date("Y-m-d H:s");
            $fullname = $this->request->data['User']['name'];
            $email = $this->request->data['User']['email'];
            $password = $this->request->data['User']['password'];
            $pr_pwd_retype = $this->request->data['User']['repassword'];
            $mobile_number = $this->request->data['User']['mobile_number'];
            $nationality = $this->request->data['User']['nationality'];
            $dob = $this->request->data['dob'];

            $data_save = $this->request->data['User'];

            if ($password != $pr_pwd_retype) {
                $this->Flash->error(__('Password is not matching !'));
                $this->redirect(array('controller' => 'members', 'action' => 'manage'));
            }

            $file = $this->request->data['User']['image'];


            if (isset($file['name']) && $file['name'] != "") {
                $rslt = $this->Common->fileFormatCommonImagesSizeCheck($file['size']);
                if (!$rslt['status']) {
                    $this->Flash->error(__('Image, ' . $rslt['msg']));
                    return $this->redirect(array('members' => 'manage'));
                }
                $img_rslt = $this->Common->fileFormatCommonImages($file['type']);
                if ($img_rslt['status']) {

                    // echo WWW_ROOT. 'upload/' . $file['name'];
                    // echo '<br>';
                    $t = time();
                    $file_name = $t . '_image_' . $file['name'];
                    $path = WWW_ROOT . 'media/photo/' . $file_name;

                    move_uploaded_file($file['tmp_name'], $path);
                    //prepare the filename for database entry
                    $data_save ['image'] = $file_name;
                } else {
                    $this->Flash->error(__('Invalid file format for Logo !' . $img_rslt['msg']));
                    return $this->redirect(array('action' => 'manage'));
                }
            } else {

                $this->Flash->error(__('Please select Photo !'));
                return $this->redirect(array('action' => 'manage'));
            }

            //$password_enc = $this->Auth->password($password);
            // $password_retype_enc = $this->Auth->password($pr_pwd_retype);


            $data_save ['created'] = $today;
            $data_save ['password'] = $this->Auth->password($password);
            $data_save ['modified'] = $today;

            $data_save ['dob'] = $dob;

            $this->AdminLogins->create();
            try {
                if ($this->AdminLogins->save($data_save)) {

                    $this->Flash->success(__('Member Registration Completed Successfully.'));

                    $this->redirect(array('controller' => 'members', 'action' => 'manage'));
                } else {

                    $this->Flash->error(__('Something went wrong !' . $this->Common->validationErrors($this->AdminLogins->validationErrors)));
                    $this->redirect(array('controller' => 'members', 'action' => 'manage'));
                }
            } catch (Exception $e) {

                $this->Flash->error(__('Already exists !' . $e->getMessage()));
                $this->redirect(array('controller' => 'members', 'action' => 'manage'));
            }
            return $this->redirect(array('action' => 'manage'));
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function editform($id = null) {
        $this->loadModel('AdminLogin');
        if ($this->request->is('ajax')) {
            $data = $this->AdminLogin->find('first', array(
                'conditions' => array(
                    'AdminLogin.id' => $id
                )
            ));
            //pr($data);
            $this->response->body(json_encode($data));
            //Return reponse object to prevent controller from trying to render a view
            return $this->response;
        }
    }

    public function edit() {
        $refer_url = $_SERVER['HTTP_REFERER'];

        $data = $this->request->data;

        $this->loadModel('AdminLogin');

        $id = $data['AdminLogin']['id'];  //exit;
        if ($this->request->is(array('post', 'put'))) {

            $message = "";
            if ($data['AdminLogin']['image']['name'] != '') {

                $img_rslt = $this->Common->fileFormatCommonImages($data['AdminLogin']['image']['type']);
                if (!$img_rslt['status']) {
                    $this->Flash->error(__('Invalid image format !' . $img_rslt['msg']));
                    return $this->redirect(array('action' => 'manage'));
                }

                $file = $data['AdminLogin']['image'];
                $t = time();
                $file_name = $t . '_' . $file['name'];
                $path = WWW_ROOT . 'media/photo/' . $file_name;

                $old_path = WWW_ROOT . 'media/photo/' . $data['AdminLogin']['image_old'];

                if (move_uploaded_file($file['tmp_name'], $path)) {

                    if ($data['AdminLogin']['image_old'] != "") {
                        if (file_exists($old_path))
                            unlink($old_path);
                    }
                    $data['AdminLogin']['image'] = $file_name;
                    $message = 'image uploaded';
                    //echo "<script>alert('image uploaded')</script>";
                } else {
                    // echo "<script>alert('There is some error in file uploading.')</script>";
                    $message = 'There is some error in file uploading.';
                }
            } else {

                $data['AdminLogin']['image'] = $data['AdminLogin']['image_old'];
            }

            $this->AdminLogin->validate = $this->AdminLogin->validateWithoutPassword;

            $exists = $this->AdminLogin->find('first', array(
                'conditions' => array('AdminLogin.email' => $data['AdminLogin']['email'], 'AdminLogin.id <>' => $data['AdminLogin']['id'])
            ));
            if ($exists) {
                
                $this->Flash->success(__('Email is already in use.' . $message));
                return $this->redirect($refer_url);
            }
            
            if ($data['AdminLogin']['change_password'] != "")
            {
                //echo 'ent';
                $this->AdminLogin->set('password', $data['AdminLogin']['change_password']);
            }

//pr($this->AdminLogin->password); exit;
            $this->AdminLogin->set('name', $data['AdminLogin']['name']);
            $this->AdminLogin->set('image', $data['AdminLogin']['image']);
            $this->AdminLogin->set('lname', $data['AdminLogin']['lname']);
            $this->AdminLogin->set('email', $data['AdminLogin']['email']);
            $this->AdminLogin->set('mobile_number', $data['AdminLogin']['mobile_number']);
            $this->AdminLogin->set('status', $data['AdminLogin']['status']);
            $this->AdminLogin->set('nationality', $data['AdminLogin']['nationality']);
            $this->AdminLogin->set('dob', $data['dob']);
            if ($id == 1) {
                $this->AdminLogin->set('admin_type_id', C_ADMIN_TYPE_ADMIN);
                $message .= ", You canot change the User Type. This is Master Admin. All other contents are modified";
            } else {
                if (AuthComponent::user('admin_type_id') == Configure::read('Admin_Type_Admin')) {
                    $this->AdminLogin->set('admin_type_id', $data['AdminLogin']['admin_type_id']);
                } else {
                    $this->AdminLogin->set('admin_type_id', C_ADMIN_TYPE_STAFF);
                    $message .= ", You canot change the User Type. All other contents are modified";
                }
            }
            $this->AdminLogin->set('id', $data['AdminLogin']['id']);


            if ($this->AdminLogin->save()) {
                $this->Flash->success(__('The User has been saved.' . $message));
                return $this->redirect($refer_url);
            } else {
                $this->Flash->error(__('The User could not be saved. Please, try again.'));
            }
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id) {
        if (AuthComponent::user('admin_type_id') == Configure::read('Admin_Type_Staff')) {
            $this->Flash->error(__('No permission to delete !'));
            $this->redirect(array('controller' => 'members', 'action' => 'manage'));
        }
        // echo 'ss'; exit;    

        $this->loadModel('AdminLogin');
        $this->AdminLogin->id = $id;

        if ($id == 1) {

            $this->Flash->error(__('Not allow to delete master admin account'));
            return $this->redirect(array('controller' => 'members', 'action' => 'manage'));
        }
        if (!$this->AdminLogin->exists()) {
            throw new NotFoundException(__('Invalid User'));
        }
        $this->request->allowMethod('post', 'delete');
        try {
            if ($this->AdminLogin->delete()) {
                $this->Flash->success(__('The User has been deleted.'));
            } else {
                $this->Flash->error(__('The User could not be deleted. Please, try again.'));
            }
        } catch (Exception $ex) {
            $this->Flash->error(__("Canot be delete! This staff's logs are exists. So you cannot delete this staff. You can in activate this staff"));
        }



        return $this->redirect(array('controller' => 'members', 'action' => 'manage'));
    }

    public function change_password() {
        $this->layout = "admin";
        $user_detail = $this->Auth->User();
        //pr($user_detail);
        $this->set(compact('user_detail'));
        $this->render('change_password');
    }

    public function update_password() {
        App::uses('AuthComponent', 'Controller/Component');
        $this->loadModel('AdminLogin');
        $data = $this->request->data;
        $id = $data['AdminLogin']['id'];
        $user_details = $this->AdminLogin->find('first', array('conditions' => array('AdminLogin.id' => $id)));
        $old_password = $user_details['AdminLogin']['password'];
        $current_password = AuthComponent::password($data['AdminLogin']['current_password']);
        $new_password = $data['AdminLogin']['new_password'];
        $confirm_password = $data['AdminLogin']['confirm_password'];

        if ($old_password == $current_password) {
            if ($new_password == $confirm_password) {
                $data['AdminLogin']['password'] = $new_password;
                $this->AdminLogin->id = $data['AdminLogin']['id'];
                if ($this->AdminLogin->save($data)) {
                    $this->redirect('/AdminLogins/logout');
                } else {
                    $this->Flash->error(__('Something went wrong !'));
                    $this->redirect('change_password');
                }
            } else {
                $this->Flash->error(__('Password & Confirm Password did not match'));
                $this->redirect('change_password');
            }
        } else {
            $this->Flash->error(__('Your current password is wrong !'));
            $this->redirect('change_password');
        }
        echo '<pre>';
        print_r($user_details);
        print_r($data);
        exit;
    }

    public function manage_profile() {
        $this->layout = "admin";
        $this->loadModel('AdminLogin');
        $login_detail = $this->Auth->User();
        $id = $login_detail['id'];
        $user_details = $this->AdminLogin->find('first', array('conditions' => array('AdminLogin.id' => $id)));
        $user_detail = $user_details['AdminLogin'];
        $this->set(compact('user_detail'));
    }

    public function update_profile() {
        $this->loadModel('AdminLogin');
        $this->AdminLogin->validate = $this->AdminLogin->validateWithoutPassword;
        $data = $this->request->data;

        $this->AdminLogin->id = $data['AdminLogin']['id'];
        if ($this->AdminLogin->save($data)) {
            $this->Session->write('Auth.User.name', $data['AdminLogin']['name']);
            $this->Flash->success(__('Updated successfully.'));
            $this->redirect('manage_profile');
        } else {
            $this->Flash->error(__('Something went wrong !' . $this->Common->validationErrors($this->AdminLogin->validationErrors)));
            $this->redirect('manage_profile');
        }
    }

}
