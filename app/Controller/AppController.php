<?php
//session_start(); // Fir HybridAuth (Social Login)
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        // 'DebugKit.Toolbar',
        'Session',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'admin_logins', 'action' => 'dashboard'),
            'loginAction' => array('controller' => 'admin_logins', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'admin', 'action' => 'login'),
            'authError' => 'You must be logged in to view this page.',
            'loginError' => 'Invalid Username or Password entered, please try again.',
            'authenticate' => array('Form' => array('fields' => array('username' => 'email', 'password' => 'password'))
            )
        )
    );

    public function initialize() {
        parent::initialize();

        $this->loadComponent('Flash');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Cookie');
    }

// only allow the login controllers only
    public function beforeFilter() {
        $this->Auth->authorize = 'Controller';
        $this->Auth->allow('index');
    }

    public function isAuthorized($user) {
        // Here is where we should verify the role and give access based on role
        //echo "kdks";die;
        return true;
    }

    function checksum($reques = array()) {
        return true;
    }

    function decrypt($encrypted_string = null) {
        if ($encrypted_string) {
            return $encrypted_string;
        }
    }

    function updateLogForInactive($utkId = null, $atkId = null) {
        $sessionLogPath = WWW_ROOT . Configure::read('Session_Log_Path');
        $logFile = $utkId . '.log';
        $sessionLogFile = $sessionLogPath . $logFile;
        $saved_sessions = trim(file_get_contents($sessionLogPath . $logFile));
        $saved_sessions_string = json_decode($saved_sessions, true);
        $fp = fopen($sessionLogFile, "r+");

        foreach ($saved_sessions_string['Active_Sessions']['app_devices'] as $key => $index) {
            if ($key != $atkId)
                $index['is_active'] = 0;
            $updated_status[$key] = $index;
        }

        $saved_sessions_string['Active_Sessions']['app_devices'] = $updated_status;
        $saved_sessions_json = json_encode($saved_sessions_string);
        if (fwrite($fp, $saved_sessions_json)) {
            fclose($fp);
        }
    }

    function validateUserSession($utkId = null, $atkId = null) {

        $sessionLogPath = WWW_ROOT . Configure::read('Session_Log_Path');
        $logFile = $utkId . '.log';
        $sessionLogFile = $sessionLogPath . $logFile;
        $response['is_verified'] = 0;
        $response['message'] = '';
        $response['status'] = '';
        if (!empty($utkId) && !empty($atkId)) {
            if (file_exists($sessionLogFile)) {
                $saved_sessions = trim(file_get_contents($sessionLogPath . $logFile));
                $saved_sessions_string = json_decode($saved_sessions, true);
                $application_session = (isset($saved_sessions_string['Active_Sessions']['app_devices'][$atkId]) && is_array($saved_sessions_string['Active_Sessions']['app_devices'][$atkId]) && sizeof($saved_sessions_string['Active_Sessions']['app_devices'][$atkId])) ? $saved_sessions_string['Active_Sessions']['app_devices'][$atkId] : '';
                if ($application_session) {
                    if ($application_session['is_active']) {
                        $response['is_verified'] = 1;
                        $response['id'] = $saved_sessions_string['id'];
                    } else {
                        $response['is_verified'] = 0;
                        $response['message'] = 'Session expired';
                        $response['status'] = '-1';
                    }
                } else {
                    $response['is_verified'] = 0;
                    $response['message'] = 'ATK invalid';
                    $response['status'] = '-2';
                }
            } else {
                $response['is_verified'] = 0;
                $response['message'] = 'UTK invalid';
                $response['status'] = '-3';
            }
        } else {
            $response['is_verified'] = 0;
            $response['message'] = 'Either UTK or ATK missing';
            $response['status'] = '-1';
        }
        return $response;
    }

    function sms_notification($recipient = null, $messageBody = null) {
        if ($recipient && $messageBody) {

            $active = Configure::read('sms_api_is_active');
            $username = Configure::read('sms_api_username');
            $password = Configure::read('sms_api_password');
            $source = Configure::read('sms_api_source');

            $this->loadModel('SmsErrorCode');
            $this->loadModel('SmsReport');

            if ($active) {
                if (function_exists("curl_init")) {
                    $ch = curl_init();
                    $baseURL = "http://sms4.rivet.solutions:8080/bulksms/bulksms";
                    $recipient = urlencode($recipient);
                    $messageBody = urlencode($messageBody);
                    $URI = $baseURL;
                    $URI .= "?username=" . $username;
                    $URI .= "&password=" . $password;
                    $URI .= "&type=0";
                    $URI .= "&dlr=1";
                    $URI .= "&destination=" . $recipient;
                    $URI .= "&message=" . $messageBody;
                    $URI .= "&source=" . urlencode($source);

                    curl_setopt($ch, CURLOPT_URL, $URI);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $content = curl_exec($ch);

                    $api_responses = explode(',', $content);

                    if (is_array($api_responses)) {
                        foreach ($api_responses as $api_response) {
                            $response_string = explode('|', $content);
                            $error_code = $response_string[0];
                            $recipient = $response_string[1];
                            $success_code = Configure::read('sms_success_code');

                            if ($success_code != $error_code) {
                                $error = $this->SmsErrorCode->findByErrorCode($error_code);
                                $this->smsFailureNotification($error['SmsErrorCode']['error_description'], urldecode($messageBody), $recipient);
                            } else {
                                $message_id = $response_string[2];
                                $data['recipient_number'] = $recipient;
                                $data['message'] = urldecode($messageBody);
                                $data['message_id'] = $message_id;

                                $this->SmsReport->create();
                                $this->SmsReport->save($data);
                            }
                        }
                    }
                } else {
                    $this->smsFailureNotification('cUrl not initialized');
                }
            }
        } else {
            $this->smsFailureNotification('Invalid request to send SMS', $messageBody, $recipient);
        }
        die;
    }

    function smsFailureNotification($error_message = null, $sms_message = null, $receipient_id = null) {
        $notify_email = Configure::read('sms_api_failure_notify_email');
        if ($notify_email) {
            
        }
    }

    function write_log($log_file, $content) {
        if (!file_exists($log_file)) {
            $fp = fopen($log_file, "w");
        } else {
            $fp = fopen($log_file, "r+");
        }
        $content = trim(file_get_contents($log_file)) . "\n" . $content;

        if (fwrite($fp, $content)) {
            fclose($fp);
        }
    }

    public function clear_cache() {
        Cache::clear();
        clearCache();

        $files = array();
        $files = array_merge($files, glob(CACHE . '*')); // remove cached css
        $files = array_merge($files, glob(CACHE . 'css' . DS . '*')); // remove cached css
        $files = array_merge($files, glob(CACHE . 'js' . DS . '*'));  // remove cached js           
        $files = array_merge($files, glob(CACHE . 'models' . DS . '*'));  // remove cached models           
        $files = array_merge($files, glob(CACHE . 'persistent' . DS . '*'));  // remove cached persistent           

        foreach ($files as $f) {
            if (is_file($f)) {
                unlink($f);
            }
        }

        if (function_exists('apc_clear_cache')):
            apc_clear_cache();
            apc_clear_cache('user');
        endif;

        $this->set(compact('files'));
        $this->layout = 'admin';
    }  
}
