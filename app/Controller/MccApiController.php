<?php

/* Create By Ajay Singh
 * 
 */
App::uses('AppController', 'Controller');

class MccApiController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Session', 'Flash', 'Common');

    public function beforeFilter() {

        $this->autoRender = false;

        $this->Auth->allow('update_user', 'register', 'login');

        $this->loadModel('AdminLogin');
    }

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    //23-6-2019 SREERAG
    //API to update product details
    public function register() {

        try {
            $request = $this->request;
            $data = $this->request->input('json_decode', true);
            //pr($data); exit;
            if (!$data) {
                throw new Exception("request not found", 501);
            }
            $this->AdminLogin->create();
            $errors = array();
            if ($data['key'] != C_API_KEY) {
                $errors [] = "Invalid API Key .";
            }
            if (empty($data['name'])) {
                $errors [] = "First Name not found .";
            }

            if (empty($data['lname'])) {
                $errors [] = "Last name not found.";
            }
            if (empty($data['password'])) {
                $errors [] = "Password not found.";
            }
            if (empty($data['re_password'])) {
                $errors [] = "Repeat password not found.";
            }
            if ($data['re_password'] != $data['password']) {
                $errors [] = "Repeat password and password couldn't  match.";
            }
            if (empty($data['email'])) {
                $errors [] = "Email not found.";
            }
            if (empty($data['admin_type_id'])) {
                $errors [] = "User Type not found.";
            }
            if ($data['admin_type_id'] != C_ADMIN_TYPE_ADMIN && $data['admin_type_id'] != C_ADMIN_TYPE_STAFF) {
                $errors [] = "Invalid User Type.";
            }
            if (empty($data['status'])) {
                $errors [] = "Status not found.";
            }
            if (empty($data['mobile_number']) || !is_int($data['mobile_number'])) {
                $errors [] = "Invalid Mobile Number.";
            }
            if (empty($data['dob'])) {
                $errors [] = "DOB not found.";
            }
            if (!$this->Common->isRealDate($data['dob'])) {

                $errors [] = "Invalid Date.(Format : YYYY-MM-DD)";
            }
            if ($data['nationality'] < 0 && $data['nationality'] >= 246) {
                $errors [] = "Invalid Nationality.";
            }
            if ($data['status'] != 1 && $data['status'] != 2) {
                $errors [] = "Invalid Status.(1 - Active; 2- In Active)";
            }
            if ($data['email']) {
                $exists = $this->AdminLogin->find('first', array(
                    'conditions' => array('AdminLogin.email' => $data['email'])
                ));
                if ($exists) {
                    $errors [] = "Email is already in used.";
                }
            }
            if (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $data['email'])) {
                $errors [] = "Invalid Email.";
            }

            if (count($errors) == 0) {
                if ($data) {

                    $save = $this->AdminLogin->save($data);

                    if ($save) {
                        $object = array(
                        );
                        $code = "200";
                        $message = "User has been added successfully.";
                        $title = "SUCCESS";
                    } else {
                        $object = "Error: While Adding User.";
                        $code = "401";
                        $message = "Error: While Adding User.";
                        $title = "Error";
                    }
                }
            } else {
                $errorString = implode("<br/>", $errors);
                $object = $errorString;
                $code = "501";
                $message = "Invalid Data.";
                $title = "Error";
            }

            $meta = array(
                "status" => $code,
                "message" => $message,
                "title" => $title
            );

            $arr = array(
                "info" => $meta,
                "data" => $object
            );
        } catch (Exception $e) {
            $meta = array(
                "status" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "info" => $meta
            );
        }

        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function update_user() {

        try {
            $request = $this->request;
            $data_save = array();
            $data = $this->request->input('json_decode', true);
            //pr($data); exit;
            if (!$data) {
                throw new Exception("request not found", 501);
            }
            $this->AdminLogin->validate = $this->AdminLogin->validateWithoutPassword;
            $errors = array();
            if ($data['key'] != C_API_KEY) {
                $errors [] = "Invalid API Key .";
            }
            if (empty($data['id']) || $data['id'] <= 0) {
                $id = 0;
                $errors [] = "Invalid Member ID .";
            } else {
                $this->AdminLogin->set('id', $data['id']);
                $id = $data['id'];
            }
            if (!empty($data['name'])) {
                $this->AdminLogin->set('name', $data['name']);
            }

            if (!empty($data['lname'])) {
                $this->AdminLogin->set('lname', $data['lname']);
            }
            if (!empty($data['password']) && $data['password'] != "") {
                $this->AdminLogin->set('password', $data['password']);
            }

            if (!empty($data['email'])) {

                $exists = $this->AdminLogin->find('first', array(
                    'conditions' => array('AdminLogin.email' => $data['email'], 'AdminLogin.id <>' => $id)
                ));
                if ($exists) {
                    $errors [] = "Email is already in use.";
                }

                if (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $data['email'])) {
                    $errors [] = "Invalid Email.";
                }
                $this->AdminLogin->set('email', $data['email']);
            }
            if (!empty($data['admin_type_id']) && $data['admin_type_id'] != "") {
                if ($data['admin_type_id'] != C_ADMIN_TYPE_ADMIN && $data['admin_type_id'] != C_ADMIN_TYPE_STAFF) {
                    $errors [] = "Invalid User Type.";
                } else {
                    $this->AdminLogin->set('admin_type_id', $data['admin_type_id']);
                }
                if (!empty($data['id']) && $data['id'] == 1) {
                    $this->AdminLogin->set('admin_type_id', C_ADMIN_TYPE_ADMIN);
                    $errors [] = "You canot change the User Type. This is Master Admin. All other contents are modified";
                }
            }

            if (!empty($data['status'])) {
                if ($data['status'] != 1 && $data['status'] != 2)
                    $errors [] = "Invalid Status.(1 - Active; 2- In Active)";
                else
                    $this->AdminLogin->set('status', $data['status']);
            }
            if (!empty($data['mobile_number']) && !is_int($data['mobile_number'])) {
                $errors [] = "Invalid Mobile Number.";
            } else {
                $this->AdminLogin->set('mobile_number', $data['mobile_number']);
            }
            if (!empty($data['dob'])) {
                if (!$this->Common->isRealDate($data['dob'])) {

                    $errors [] = "Invalid Date.(Format : YYYY-MM-DD)";
                } else {
                    $this->AdminLogin->set('dob', $data['dob']);
                }
            }

            if (!empty($data['nationality'])) {
                if ($data['nationality'] < 0 && $data['nationality'] >= 246)
                    $errors [] = "Invalid Nationality.";
                else
                    $this->AdminLogin->set('nationality', $data['nationality']);
            }


            if (count($errors) == 0) {

                $save = $this->AdminLogin->save();

                if ($save) {
                    $object = array(
                    );
                    $code = "200";
                    $message = "User has been added successfully.";
                    $title = "SUCCESS";
                } else {
                    $object = "Error: While Adding User.";
                    $code = "401";
                    $message = "Error: While Adding User.";
                    $title = "Error";
                }
            } else {
                $errorString = implode("<br/>", $errors);
                $object = $errorString;
                $code = "501";
                $message = "Invalid Data.";
                $title = "Error";
            }

            $meta = array(
                "status" => $code,
                "message" => $message,
                "title" => $title
            );

            $arr = array(
                "info" => $meta,
                "data" => $object
            );
        } catch (Exception $e) {
            $meta = array(
                "status" => $e->getCode(),
                "message" => $e->getMessage()
            );

            $arr = array(
                "info" => $meta
            );
        }

        $json = json_encode($arr, JSON_PRETTY_PRINT);
        echo $json;
    }

    public function login() {

        try {
            $request = $this->request;
            $data_save = array();
            $data = $this->request->input('json_decode', true);
            //pr($data); exit;
            if (!$data) {
                throw new Exception("request not found", 501);
            }
            $this->AdminLogin->validate = $this->AdminLogin->validateWithoutPassword;
            $errors = array();
            if ($data['key'] != C_API_KEY) {
                $errors [] = "Invalid API Key .";
            }

            if (empty($data['password']) || $data['password'] == "") {
                $errors [] = "Password Not Entered .";
            }
            if (empty($data['email'])) {
                $errors [] = "Email Not Entered .";
            }

            if (count($errors) == 0) {
                $exists = $this->AdminLogin->find('first', array(
                    'conditions' => array('AdminLogin.email' => $data['email'], 'AdminLogin.password' => $this->Auth->password($data['password']))
                ));
                if (!$exists) {
                    $code = FALSE;
                    $message = "Invalid User.";
                    $title = "Error";
                } else {
                    $code = TRUE;
                    $message = "Valid User";
                    $title = "Success";
                }
            } else {
                $errorString = implode("<br/>", $errors);

                $code = FALSE;
                $message = "Invalid Data." . $errorString;
                $title = "Error";
            }



            $meta = array(
                "status" => $code,
                "message" => $message,
                "title" => $title
            );
        } catch (Exception $e) {
            $meta = array(
                "status" => FALSE,
                "message" => $e->getMessage(),
                "title" => $e->getCode()
            );
        }

        $json = json_encode($meta, JSON_PRETTY_PRINT);
        echo $json;
    }

}
