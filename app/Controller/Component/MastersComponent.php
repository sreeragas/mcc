<?php

/**
 * Name             :   Sreerag A S
 * Date Created     :   4-6-2018
 * Last Modified    :   4-6-2018
 *
 */
App::uses('Component', 'Controller');

class MastersComponent extends Component {

    public $components = array('Common');

    public function getCities($country_id) {
        App::import('Controller', 'ConciergeCountryCities');
        $ConciergeCityObj = new ConciergeCountryCitiesController;
        //echo  $country_id; exit;
        $cities = $ConciergeCityObj->listCities($country_id);
        return $cities;
    }

    //Sreerag 
    // pass $nationality_pref  as 0, if there is no nationality preference
    public function getServiceOfferings($city_id = 0, $service_type_id = 0, $nationality_pref = 0) {
        //$mem_cat_mdl = ClassRegistry::init('ConciergeMembershipCategory');
        $mem_cat_mdl = ClassRegistry::init('ConciergePartnerServiceOffering');  // Initialize Model in components
        if ($nationality_pref > 0)
        {
             $sql = "SELECT SO.pro_service_offering_id,SO.pro_product_code,SO.pro_product_description,SO.pro_product_image,SO.pro_service_details,
            SO.pro_service_terms,ST.st_service_level,ST.st_show_booking_terms_while_booking,SO.pro_product_image_second,SO.pro_product_image_third,
            SOC.proc_price,SOC.proc_working_days,SOC.proc_working_time_start,SOC.proc_working_time_end,CURRENCY.cr_symbol,
            PARTNER.pr_name,PARTNER.pr_registered_address,PARTNER.pr_contact_person_mobile,
            PARTNER.pr_contact_person_mobile,PARTNER.pr_contact_person_email,
            COUNTRY.cm_vat_percentage,COUNTRY.cm_oth_tax_percent
            
            FROM concierge_partner_service_offerings AS SO
            LEFT JOIN concierge_service_types AS ST on ST.st_service_type_id = SO.pro_service_type
            LEFT JOIN concierge_partner_service_offering_countries AS SOC on SOC.proc_service_offering_id = SO.pro_service_offering_id
            LEFT JOIN concierge_currency_masters AS CURRENCY on CURRENCY.cr_currency_id = SOC.proc_currency_id
            LEFT JOIN concierge_country_masters AS COUNTRY on COUNTRY.cm_country_id = SOC.proc_country_id
            LEFT JOIN concierge_partnership_infos AS PARTNER on PARTNER.pr_partner_id = SO.pro_partner_id
            LEFT JOIN concierge_partner_service_nationality_prefs AS SONP on SONP.psnp_service_offering_id = SO.pro_service_offering_id
            WHERE SONP.psnp_nationality_id = '" . $nationality_pref."' AND SONP.psnp_status = 'A'"
                     . " AND SO.pr_product_approved_yn = 'Y' AND SO.pro_status = 'A' AND SOC.proc_service_type = '".$service_type_id."' AND SOC.proc_city = '".$city_id."' "
                . "AND  SOC.proc_status ='A' AND SO.org_id ='" . $this->Common->getOrganizationId() . "' ";
        }
        else 
        {
            $sql = "SELECT SO.pro_service_offering_id,SO.pro_product_code,SO.pro_product_description,SO.pro_product_image,SO.pro_service_details,
            SO.pro_service_terms,ST.st_service_level,ST.st_show_booking_terms_while_booking,SO.pro_product_image_second,SO.pro_product_image_third,
            SOC.proc_price,SOC.proc_working_days,SOC.proc_working_time_start,SOC.proc_working_time_end,CURRENCY.cr_symbol,
            PARTNER.pr_name,PARTNER.pr_registered_address,PARTNER.pr_contact_person_mobile,
            PARTNER.pr_contact_person_mobile,PARTNER.pr_contact_person_email,
            COUNTRY.cm_vat_percentage,COUNTRY.cm_oth_tax_percent
            
            FROM concierge_partner_service_offerings AS SO
            LEFT JOIN concierge_service_types AS ST on ST.st_service_type_id = SO.pro_service_type
            LEFT JOIN concierge_partner_service_offering_countries AS SOC on SOC.proc_service_offering_id = SO.pro_service_offering_id
            LEFT JOIN concierge_currency_masters AS CURRENCY on CURRENCY.cr_currency_id = SOC.proc_currency_id
            LEFT JOIN concierge_country_masters AS COUNTRY on COUNTRY.cm_country_id = SOC.proc_country_id
            LEFT JOIN concierge_partnership_infos AS PARTNER on PARTNER.pr_partner_id = SO.pro_partner_id
            WHERE SO.pr_product_approved_yn = 'Y' AND SO.pro_status = 'A' AND SOC.proc_service_type = '".$service_type_id."' AND SOC.proc_city = '".$city_id."' "
                . "AND  SOC.proc_status ='A' AND SO.org_id ='" . $this->Common->getOrganizationId() . "' ";
        }
           
        
                
        $mem_cat = $mem_cat_mdl->query($sql);
     //  pr($sql); exit;
        return $mem_cat;
    }
    
    //Sreerag 
    //will return the service offering details with price, tax etc for a selected city
     public function getServiceOfferingsDetailsById($city_id = 0, $service_offering_id = 0) {
        //$mem_cat_mdl = ClassRegistry::init('ConciergeMembershipCategory');
        $mem_cat_mdl = ClassRegistry::init('ConciergePartnerServiceOffering');  // Initialize Model in components
       
            $sql = "SELECT SO.pro_service_offering_id,SO.pro_product_code,SO.pro_product_description,SO.pro_product_image,SO.pro_service_details,
            SO.pro_service_terms,ST.st_service_level,ST.st_show_booking_terms_while_booking,SO.pro_product_image_second,SO.pro_product_image_third,
            SOC.proc_price,SOC.proc_working_days,SOC.proc_working_time_start,SOC.proc_working_time_end,CURRENCY.cr_symbol,
            PARTNER.pr_name,PARTNER.pr_registered_address,PARTNER.pr_contact_person_mobile,
            PARTNER.pr_contact_person_mobile,PARTNER.pr_contact_person_email,
            COUNTRY.cm_vat_percentage,COUNTRY.cm_oth_tax_percent
            
            FROM concierge_partner_service_offerings AS SO
            LEFT JOIN concierge_service_types AS ST on ST.st_service_type_id = SO.pro_service_type
            LEFT JOIN concierge_partner_service_offering_countries AS SOC on SOC.proc_service_offering_id = SO.pro_service_offering_id
            LEFT JOIN concierge_currency_masters AS CURRENCY on CURRENCY.cr_currency_id = SOC.proc_currency_id
            LEFT JOIN concierge_country_masters AS COUNTRY on COUNTRY.cm_country_id = SOC.proc_country_id
            LEFT JOIN concierge_partnership_infos AS PARTNER on PARTNER.pr_partner_id = SO.pro_partner_id
            WHERE  SO.pro_service_offering_id = '".$service_offering_id."' AND SOC.proc_city = '".$city_id."' ";
                
        
        
                
        $mem_cat = $mem_cat_mdl->query($sql);
     //  pr($sql); exit;
        return $mem_cat;
    }

    //Sreerag 
    public function getMembershipCategory($country_id = 0, $category_id = 0) {
        //$mem_cat_mdl = ClassRegistry::init('ConciergeMembershipCategory');
        $mem_cat_mdl = ClassRegistry::init('ConciergeMembershipCategory');  // Initialize Model in components
//        $options['joins'] = array(
//            array('table' => 'concierge_membersip_prices',
//                'alias' => 'ConciergeMembersipPrice',
//                'type' => 'LEFT',
//                'conditions' => array(
//                    'ConciergeMembersipPrice.mp_catg_id = ConciergeMembershipCategory.mc_catg_id',
//                )
//            )
//        );
//        $options['conditions'] = array(
//            'ConciergeMembersipPrice.mp_country_id' => $country_id,
//            'ConciergeMembershipCategory.org_id' => Configure::read('Concierge_Org_Id'),
//        );
//        $options['fileds'] = array(
//            'mc_catg_id','mc_catg_code', 'mc_catg_description', 'mc_status', 'mc_image',
//            'mc_benefits', 'mp_price_year', 'mp_price_month', 'mp_vat_applicable_yn',
//            'mp_tax1_applicable_yn', 'mp_status'
//            );
//
//        $mem_cat = $mem_cat_mdl->find('all', $options);
//         $dbo = $mem_cat_mdl->getDatasource();
//          $logs = $dbo->getLog();
//          $lastLog = end($logs['log']);
//          debug( $lastLog['query'] );
        // pr($countries); exit;

        $catgry_str = "";
        if ($category_id > 0)
            $catgry_str = " AND concierge_membership_categories.mc_catg_id = " . $category_id;
        $country_str = "";
        if ($country_id > 0)
            $country_str = " AND concierge_membership_prices.mp_country_id = " . $country_id;


        $sql = "SELECT 	mc_catg_id,mc_catg_code, mc_catg_description, mc_status, mc_image,mp_country_id,
            mc_benefits, mp_price_year, mp_price_month, mp_vat_applicable_yn,
            mp_tax1_applicable_yn, mp_status FROM concierge_membership_categories
            LEFT JOIN concierge_membership_prices on concierge_membership_categories.mc_catg_id = concierge_membership_prices.mp_catg_id
            WHERE concierge_membership_categories.org_id = " . $this->Common->getOrganizationId() . " "
                . " AND concierge_membership_categories.mc_status = 'A' " . $country_str . $catgry_str;

        $mem_cat = $mem_cat_mdl->query($sql);
       //  pr($sql); exit;
        return $mem_cat;
    }

    public function getServiceCategory($city_id) {
        $mem_cat_mdl = ClassRegistry::init('ConciergeServiceCategory');  // Initialize Model in components

        if ($city_id > 0)
            $sql = "SELECT ServiceCategories.sct_catg_id,ServiceCategories.sct_catg_code,
             ServiceCategories.sct_catg_description,ServiceCategories.sct_image,ServiceCategories.sct_details
                FROM concierge_service_categories AS ServiceCategories
                LEFT JOIN concierge_serving_countries AS ServingCountries ON ServingCountries.sct_service_category_id = ServiceCategories.sct_catg_id 
                WHERE ServiceCategories.org_id = " . $this->Common->getOrganizationId() . " AND ServingCountries.sct_city_id = " . $city_id . ""
                    . " AND ServiceCategories.sct_status = 'A' AND ServingCountries.sct_status = 'A'";
        else
            $sql = "SELECT ServiceCategories.sct_catg_id,ServiceCategories.sct_catg_code,
             ServiceCategories.sct_catg_description,ServiceCategories.sct_image,ServiceCategories.sct_details
                FROM concierge_service_categories AS ServiceCategories
                LEFT JOIN concierge_serving_countries AS ServingCountries ON ServingCountries.sct_service_category_id = ServiceCategories.sct_catg_id 
                WHERE ServiceCategories.org_id = " . $this->Common->getOrganizationId() . " AND ServiceCategories.sct_status = 'A' AND ServingCountries.sct_status = 'A'";

        $mem_cat = $mem_cat_mdl->query($sql);
         //pr($mem_cat); exit;
        return $mem_cat;
    }

    public function get_membership_category_details_by_id($mem_id) {
        $mem_cat_mdl = ClassRegistry::init('ConciergeMembershipCategory');  // Initialize Model in components
        $options['conditions'] = array(
            'ConciergeMembershipCategory.mc_catg_id' => $mem_id,
        );
        $mem_cat = $mem_cat_mdl->find('all', $options);
        return $mem_cat;
    }

}
