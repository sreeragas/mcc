<?php

/**
 * Name             :   Sreerag A S
 * Date Created     :   4-6-2018
 * Last Modified    :   4-6-2018
 *
 */
App::uses('Component', 'Controller');

class CommonComponent extends Component {

    public function myDateDMY($date) {
        if ($date != "")
            return date('d-m-Y', strtotime($date));
        else
            return "";
    }

    public function myDateYMD($date) {
        if ($date != "")
            return date('Y-m-d', strtotime($date));
        else
            return "";
    }

    public function myTime12Hrs($time) {
        if ($time != "")
            return date("g:i A", strtotime($time));
        else
            return "";
    }

    public function isRealDate($date) {
        if (false === strtotime($date)) {
            return false;
        }
        list($year, $month, $day) = explode('-', $date);

        if (is_numeric($month)&& is_numeric($day) && is_numeric($year))
        {
            return checkdate($month, $day, $year);
        }
        else 
        {
            return FALSE;
        }
        
        
    }

    //24-7-2018 SREERAG
    //Function to encode all special charactors in an array. This is required to get json_encode working properly if there is
    // special characters like this . "Monégasque"
    public function utf8_string_array_encode(&$array) {
        $func = function(&$value, &$key) {
            if (is_string($value)) {
                $value = utf8_encode($value);
            }
            if (is_string($key)) {
                $key = utf8_encode($key);
            }
            if (is_array($value)) {
                $this->utf8_string_array_encode($value);
            }
        };
        array_walk($array, $func);
        return $array;
    }

    public function fileFormatCommonDocuments($type) {
        if (($type == 'image/png') || ($type == 'image/jpg') || ($type == 'image/jpeg') || ($type == 'application/pdf') || ($type == 'application/msword') || ($type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')) {
            $rslt['status'] = TRUE;
            $rslt['msg'] = "";
            return $rslt;
        } else {
            $rslt['status'] = FALSE;
            $rslt['msg'] = "(Only allowed PNG,JPG,JPEG,PDF,DOC,DOCX formats)";
            return $rslt;
        }
    }

    public function fileFormatCommonImages($type) {
        if (($type == 'image/png') || ($type == 'image/jpg') || ($type == 'image/jpeg')) {
            $rslt['status'] = TRUE;
            $rslt['msg'] = "";
            return $rslt;
        } else {
            $rslt['status'] = FALSE;
            $rslt['msg'] = "(Only allowed PNG,JPG,JPEG formats)";
            return $rslt;
        }
    }

    public function fileFormatCommonImagesSizeCheck($size) {

        if (($size > (1048576 * 1))) { // 1 MB
            $rslt['status'] = FALSE;
            $rslt['msg'] = "Maximum size allowed is 1 MB ";
        } else {
            $rslt['status'] = TRUE;
            $rslt['msg'] = "";
        }
        return $rslt;
    }

    public function fileFormatCommonDocumentsSizeCheck($size) {

        if (($size > (1048576 * 5))) { // 5 MB
            $rslt['status'] = FALSE;
            $rslt['msg'] = "Maximum size allowed is 5 MB ";
        } else {
            $rslt['status'] = TRUE;
            $rslt['msg'] = "";
        }
        return $rslt;
    }

    public function getLastExecutedQuery($model) {

        $dbo = $model->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        debug($lastLog['query']);
    }

    public function validationErrors($error_array) {
        $error_str = "";

        if (!empty($error_array)) {
            foreach ($error_array as $err_sub) {
                if (!empty($err_sub)) {
                    foreach ($err_sub as $err) {
                        if ($error_str != "")
                            $error_str .= ',' . $err;
                        else
                            $error_str .= $err;
                    }
                }
            }
        }
        return $error_str;
    }

    public function getOrganizationId() {
        return Configure::read('Concierge_Org_Id');
    }

    //permits both staff type and admin type users
    public function checkAuthPermission() {

        $user = AuthComponent::user();
        $staff_type = Configure::read('Admin_Type_Staff');
        $super_admin_type = Configure::read('Admin_Type_Admin');
        $user_id = $user['id'];

        if ($user['admin_type_id'] != $staff_type && $user['admin_type_id'] != $super_admin_type) {
            return false;
        }

        return $user_id;
    }

}
