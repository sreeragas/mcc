-----------------URLS------------------------
http://localhost/mcc    =>  Landing page 

Master Admin User
------------------
User : admin@mcc.com
Password : 1234

Normal Admin Sample
------------------------
User : sruthy@gmail.com
Password : 1234

Member Login Sample
------------------------
User : sreerag@nexabion.com
Password : 1234

-------------------------------------------------
Database Settings
-------------------------------------------------
app/config/database.php 


----------------------------------------------
---------------------API DOC------------------
----------------------------------------------
Register User
--------------------------
Method : POST
URL : http://localhost/mcc/api/register


key is defined in app/config/bootstrap.php 
JSON DATA 
{
"key" : "2E3872CC312CFA9ECAA24FF128389",
"name" : "API Name",
"lname" : "Test API",
"password" : "123",
"re_password" : "123",
"email" : "sree@nex.com",
"admin_type_id" : 2,
"status":1,
"dob":"2010-05-28",
"nationality":5,
"mobile_number":875412552
}
--------------------------
Update User
--------------------------
Method : POST
URL : http://localhost/mcc/api/update

JSON DATA 
{
"key" : "2E3872CC312CFA9ECAA24FF128389",
"id" : 2,
"name" : "API Name",
"lname" : "Test API",
"password" : "123",
"re_password" : "123",
"email" : "sree@nex.com",
"admin_type_id" : 2,
"status":1,
"dob":"2010-05-28",
"nationality":5,
"mobile_number":875412552
}
NOTE : leave the fields blank if not update

-------------------------------------------
LOGIN API
------------------------------------------

Method : POST
URL : http://localhost/mcc/api/login

JSON DATA 
{
"key" : "2E3872CC312CFA9ECAA24FF128389",
 
"password" : "12323",

"email" : "admin@mcc.com"

}

--------------------------------------------------------------