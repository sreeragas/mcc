-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 11, 2019 at 06:11 PM
-- Server version: 5.6.37
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mc_collins`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_logins`
--

CREATE TABLE IF NOT EXISTS `admin_logins` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `admin_type_id` int(11) NOT NULL DEFAULT '2',
  `name` varchar(64) NOT NULL,
  `lname` varchar(75) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` enum('1','2') NOT NULL DEFAULT '1',
  `dob` date DEFAULT NULL,
  `nationality` mediumint(9) NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_logins`
--

INSERT INTO `admin_logins` (`id`, `created`, `modified`, `admin_type_id`, `name`, `lname`, `email`, `mobile_number`, `password`, `status`, `dob`, `nationality`, `image`) VALUES
(1, '2017-04-17 00:00:00', '2019-09-11 19:18:32', 1, 'Admin', 'Ad', 'admin@mcc.com', '1223344545454', '3572c2076bab9ef39c44a6c5da1a86efd4c02a5a', '1', '2019-09-17', 4, ''),
(2, '2017-04-19 00:00:00', '2019-09-11 22:09:37', 2, 'Sreerag', 'AS', 'sreerag@nexabion.com', '9555533333', '3572c2076bab9ef39c44a6c5da1a86efd4c02a5a', '1', '2019-09-03', 17, '1568055093_WhatsApp Image 2019-08-03 at 7.29.00 PM.jpeg'),
(28, '2019-09-08 22:27:00', '2019-09-11 20:41:13', 2, 'Mr Raed Hassan', 'Hassan', 'admin@sds.com', '585193663', '3728d59f8cd45e6710878bb924d25cf6c55e325b', '1', '2019-09-20', 16, '1568055093_WhatsApp Image 2019-08-03 at 7.29.00 PM.jpeg'),
(30, '2019-09-09 18:03:00', '2019-09-09 22:55:28', 1, 'Mr Raed Hassan', 'Hassan', 'admin@dsds.dsds', '585193663', '50238e5604ed8201bbd9d5d59e0fdafcefe9380c', '1', '2019-09-03', 16, '1568037603_image_WhatsApp Image 2019-08-03 at 7.29.00 PM.jpeg'),
(32, '2019-09-10 20:52:00', '2019-09-10 20:52:00', 1, 'Sruthy', 'Sreerag', 'sruthy@gmail.com', '875445', '3572c2076bab9ef39c44a6c5da1a86efd4c02a5a', '1', '1995-07-20', 101, '1568131312_image_WhatsApp Image 2019-08-03 at 7.29.00 PM (1).jpeg'),
(34, '2019-09-11 19:51:14', '2019-09-11 22:10:55', 2, 'API Name', 'Test API', 'sree@nexabion.com', '875412552', '3572c2076bab9ef39c44a6c5da1a86efd4c02a5a', '1', '2010-05-28', 5, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_logins`
--
ALTER TABLE `admin_logins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `admin_nationality` (`nationality`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_logins`
--
ALTER TABLE `admin_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
